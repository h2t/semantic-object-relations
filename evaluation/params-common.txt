# common parameters for all strategies

# Shape Extraction

maxIterations = 100
distanceThresholdBox = 5.0
distanceThresholdPCL = 5.0

minInlierRate = 0.7
outlierRate = 0.00125
sizePenaltyFactor = 0.0
sizePenaltyExponent = 2.0
concurrencyEnabled = true


# Support Analysis

gravityX = 0.0
gravityY = 0.0
gravityZ = -1.0

contactMargin = 10.0
supportAreaRatioMin = 0.7

