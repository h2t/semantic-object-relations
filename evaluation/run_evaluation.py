#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Run experiments and evaluation.

@author: Rainer Kartmann
"""

#%%
import os
import subprocess

import evaluation_config as cfg
import evaluation_utils as utils
from confusion_matrix import ConfusionMatrix


#%%
def run_evaluation():
    
    #%% Prepare experiments
    
    utils.resetDirectory(cfg.experiments_dir)
    
    #%% Run experiments
    
    print("=== Running experiments ===")
    
    for scene_name in cfg.scene_names:
        
        print("== Experiment for scene {} ==".format(scene_name))
        
        pointcloud_file = utils.pointcloudFilePath(scene_name)
        
        output_dir = utils.sceneResultsDir(scene_name)
        utils.resetDirectory(scene_name, cfg.experiments_dir)
        
        output_graph_name = os.path.join(output_dir, cfg.output_graph_name)
        
        
        # call the experiment executable
        print("Running experiment ...")


        args = [ cfg.experiment_binary ]
        args += [ pointcloud_file, output_graph_name, 
                cfg.params_common, cfg.params_strategy ]        
        logfile = os.path.join(output_dir, "experiment-log.txt")
        
        try:
            with open(logfile, "w") as logfile:
                ret = subprocess.call(args, stdout=logfile, stderr=logfile)
                
        except FileNotFoundError as e:
            print("[error] Experiment executable not found.")
            print("[error] Please build the experiment executable '{}'".format(
                    cfg.experiment_binary))
            return
        
        if ret == 0:
            print("Experiment finished.")
        else:
            print("Experiment finished with error: {}".format(ret))
    
        print("Results stored in: '{}'".format(output_dir))
        print("")
    
    
    #%% Relabel result graphs
    
    for scene_name in cfg.scene_names:
        print("Relabeling result graphs for scene {}".format(scene_name))
        for strategy_name in cfg.strategy_names:
            utils.relabelResultGraph(scene_name, strategy_name)
        
        
    #%% Run evaluation
    
    print("Allocating evaluation data frame ...")
    eval_data = utils.createEvalDataFrame()
    
    for scene_name in cfg.scene_names:
        
        print("Evaluating results for scene {} ...".format(scene_name))
        
        for strategy_name in cfg.strategy_names:
            
            cm = ConfusionMatrix(scene_name, strategy_name)
            
            row = eval_data.loc[scene_name, strategy_name]
            
            row.tp = cm.tp()
            row.fp = cm.fp()
            row.fn = cm.fn()
            row.tn = cm.tn()
            row.prec = cm.precision()
            row.rec = cm.recall()
            
    
    paper_data = utils.toPaperFormat(eval_data)
    
    #%% Store results
    
    print("Storing results in {} and {}".format(
            cfg.evaluation_data_file,
            cfg.evaluation_data_file.replace(".", "_paper.")))
    
    eval_data.to_csv(cfg.evaluation_data_file)
    paper_data.to_csv(cfg.evaluation_data_file.replace(".", "_paper."))

#%%
    
    
if __name__ == "__main__":
    run_evaluation()

