#include "SemantiXVariantsFactories.h"

namespace armarx
{
    namespace ObjectFactories
    {
        const FactoryCollectionBaseCleanUp ShapePrimitivesFactories::ShapePrimitivesFactoriesVar =
            FactoryCollectionBase::addToPreregistration(new ObjectFactories::ShapePrimitivesFactories());
    }
}
