#pragma once

#include <functional>

#include <ArmarXCore/observers/AbstractObjectSerializer.h>

namespace armarx
{

    template <class VariantT>
    using ItemSerializer = std::function<void(AbstractObjectSerializerPtr, const VariantT&)>;

    template <class VariantT>
    using ItemDeserializer = std::function<VariantT(AbstractObjectSerializerPtr)>;


    template <class VariantPtrT>
    void VariantPtrSerializer(AbstractObjectSerializerPtr obj, const VariantPtrT& v)
    {
        v->serialize(obj);
    }

    template <class VariantT>
    auto VariantPtrDeserializer(AbstractObjectSerializerPtr obj) -> IceInternal::Handle<VariantT>
    {
        IceInternal::Handle<VariantT> var(new VariantT());
        var->deserialize(obj);
        return var;
    }

    template<class VariantT>
    void serializeVariantList(
        const std::vector<VariantT>& list,
        const std::string& key,
        const ObjectSerializerBasePtr& serializer,
        ItemSerializer<VariantT> itemSerializer)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        armarx::AbstractObjectSerializerPtr array = obj->createElement();
        array->setElementType(armarx::ElementTypes::eArray);
        for (const VariantT& item : list)
        {
            armarx::AbstractObjectSerializerPtr obj = array->createElement();
            obj->setElementType(armarx::ElementTypes::eObject);
            itemSerializer(obj, item); // item->serialize(o);
            array->append(obj);
        }
        obj->setElement(key, array);
    }

    template<class VariantT>
    void deserializeVariantList(
        std::vector<VariantT>& targetList,
        const std::string& key,
        const armarx::ObjectSerializerBasePtr& serializer,
        ItemDeserializer<VariantT> itemDeserializer)
    {
        armarx::AbstractObjectSerializerPtr obj = armarx::AbstractObjectSerializerPtr::dynamicCast(serializer);

        armarx::AbstractObjectSerializerPtr array = obj->getElement(key);
        targetList.clear();
        targetList.resize(array->size());
        for (std::size_t i = 0; i < array->size(); ++i)
        {
            targetList[i] = itemDeserializer(array->getElement(i));
        }
    }

}
