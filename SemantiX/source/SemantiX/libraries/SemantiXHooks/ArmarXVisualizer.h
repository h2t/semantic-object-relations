#pragma once

#include <Eigen/Core>

#include <SemanticObjectRelations/Hooks/VisualizerInterface.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <RobotAPI/components/ArViz/Client/Client.h>


namespace armarx
{

    /**
     * @brief Implementation of semrel::VisualizerInterface for ArmarX
     * (using the DebugDrawer).
     */
    class ArmarXVisualizer : public semrel::VisualizerInterface
    {
    public:
        static void setAsImplementation(DebugDrawerInterfacePrx debugDrawer);


        ArmarXVisualizer(DebugDrawerInterfacePrx debugDrawer);

        virtual void clearAll() override;
        virtual void clearLayer(const std::string& layer) override;

        virtual void drawLine(semrel::VisuMetaInfo id, const Eigen::Vector3f& start, const Eigen::Vector3f& end, float lineWidth, semrel::DrawColor color) override;
        virtual void drawArrow(semrel::VisuMetaInfo id, const Eigen::Vector3f& origin, const Eigen::Vector3f& direction, float length, float width, semrel::DrawColor color) override;
        virtual void drawBox(semrel::VisuMetaInfo id, const semrel::Box& box, semrel::DrawColor color) override;
        virtual void drawCylinder(semrel::VisuMetaInfo id, const semrel::Cylinder& cylinder, semrel::DrawColor color) override;
        virtual void drawSphere(semrel::VisuMetaInfo id, const semrel::Sphere& sphere, semrel::DrawColor color) override;
        virtual void drawPolygon(semrel::VisuMetaInfo id, const std::vector<Eigen::Vector3f>& polygonPoints, float lineWidth, semrel::DrawColor colorInner, semrel::DrawColor colorBorder) override;
        virtual void drawTriMesh(semrel::VisuMetaInfo id, const semrel::TriMesh& mesh, semrel::DrawColor color) override;
        virtual void drawText(semrel::VisuMetaInfo id, const std::string& text, const Eigen::Vector3f& position, float size, semrel::DrawColor color) override;
        virtual void drawPointCloud(semrel::VisuMetaInfo id, const std::vector<Eigen::Vector3f>& cloud, float pointSize, semrel::DrawColor color) override;

    private:
        DebugDrawerInterfacePrx drawer;
    };

    /**
     * @brief Implementation of semrel::VisualizerInterface for ArmarX
     * (using the DebugDrawer).
     */
    class ArVizVisualizer : public semrel::VisualizerInterface
    {
    public:

        static void setAsImplementation(const viz::Client& arviz);


        ArVizVisualizer(armarx::viz::Client const& arviz);

        virtual void clearAll() override;
        virtual void clearLayer(const std::string& layer) override;

        virtual void drawLine(semrel::VisuMetaInfo id, const Eigen::Vector3f& start, const Eigen::Vector3f& end, float lineWidth, semrel::DrawColor color) override;
        virtual void drawArrow(semrel::VisuMetaInfo id, const Eigen::Vector3f& origin, const Eigen::Vector3f& direction, float length, float width, semrel::DrawColor color) override;
        virtual void drawBox(semrel::VisuMetaInfo id, const semrel::Box& box, semrel::DrawColor color) override;
        virtual void drawCylinder(semrel::VisuMetaInfo id, const semrel::Cylinder& cylinder, semrel::DrawColor color) override;
        virtual void drawSphere(semrel::VisuMetaInfo id, const semrel::Sphere& sphere, semrel::DrawColor color) override;
        virtual void drawPolygon(semrel::VisuMetaInfo id, const std::vector<Eigen::Vector3f>& polygonPoints, float lineWidth, semrel::DrawColor colorInner, semrel::DrawColor colorBorder) override;
        virtual void drawTriMesh(semrel::VisuMetaInfo id, const semrel::TriMesh& mesh, semrel::DrawColor color) override;
        virtual void drawText(semrel::VisuMetaInfo id, const std::string& text, const Eigen::Vector3f& position, float size, semrel::DrawColor color) override;
        virtual void drawPointCloud(semrel::VisuMetaInfo id, const std::vector<Eigen::Vector3f>& cloud, float pointSize, semrel::DrawColor color) override;


    private:
        armarx::viz::Layer& getLayer(std::string const& name);

        armarx::viz::Client arviz;
        std::map<std::string, armarx::viz::Layer> layers;
    };


}
