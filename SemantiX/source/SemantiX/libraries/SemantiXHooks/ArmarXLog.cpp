#include "ArmarXLog.h"


using namespace armarx;


void armarx::ArmarXLog::setAsImplementation()
{
    semrel::LogInterface::setImplementation(std::make_shared<ArmarXLog>());
}

ArmarXLog::ArmarXLog(const std::string& tag)
{
    setTag(tag);
}


void ArmarXLog::log(semrel::LogMetaInfo info, const std::string& message)
{
    (*loghelper(info.file.c_str(), info.line, info.func.c_str()))
            << logLevelToMessageType(info.level)
            << message;
}

MessageTypeT ArmarXLog::logLevelToMessageType(semrel::LogLevel level)
{
    switch (level)
    {
        case semrel::LogLevel::DEBUG:
            return armarx::MessageTypeT::DEBUG;
        case semrel::LogLevel::VERBOSE:
            return armarx::MessageTypeT::VERBOSE;
        case semrel::LogLevel::INFO:
            return armarx::MessageTypeT::INFO;
        case semrel::LogLevel::IMPORTANT:
            return armarx::MessageTypeT::IMPORTANT;
        case semrel::LogLevel::WARNING:
            return armarx::MessageTypeT::WARN;
        case semrel::LogLevel::ERROR:
            return armarx::MessageTypeT::ERROR;

        default:
            std::stringstream msg;
            msg << "Unexpected enum value of enum semrel::LogLevel (" << int(level) << ")";
            throw std::domain_error(msg.str());
    }
}

