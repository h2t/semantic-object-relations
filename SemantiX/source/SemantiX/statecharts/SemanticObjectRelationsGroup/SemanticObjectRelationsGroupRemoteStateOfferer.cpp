/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::SemanticObjectRelationsGroup::SemanticObjectRelationsGroupRemoteStateOfferer
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SemanticObjectRelationsGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace SemanticObjectRelationsGroup;

// DO NOT EDIT NEXT LINE
SemanticObjectRelationsGroupRemoteStateOfferer::SubClassRegistry SemanticObjectRelationsGroupRemoteStateOfferer::Registry(SemanticObjectRelationsGroupRemoteStateOfferer::GetName(), &SemanticObjectRelationsGroupRemoteStateOfferer::CreateInstance);



SemanticObjectRelationsGroupRemoteStateOfferer::SemanticObjectRelationsGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < SemanticObjectRelationsGroupStatechartContext > (reader)
{
}

void SemanticObjectRelationsGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void SemanticObjectRelationsGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void SemanticObjectRelationsGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string SemanticObjectRelationsGroupRemoteStateOfferer::GetName()
{
    return "SemanticObjectRelationsGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr SemanticObjectRelationsGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new SemanticObjectRelationsGroupRemoteStateOfferer(reader));
}



