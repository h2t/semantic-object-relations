#include "shapes_from_aabbs.h"

#include <SemanticObjectRelations/Shapes.h>
#include <SemanticObjectRelations/ShapeExtraction/util/SoftMinMax.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <VisionX/libraries/PointCloudTools/segments.h>


namespace semrel
{

    template <class PointT>
    ShapeList getShapesFromAABBs(const pcl::PointCloud<PointT>& pointCloud)
    {
        const std::map<uint32_t, pcl::PointIndices> segmentIndices = visionx::tools::getSegmentIndices(pointCloud, false);
        const std::map<uint32_t, simox::AxisAlignedBoundingBox> aabbs = visionx::tools::getSegmentAABBs(pointCloud, segmentIndices);
        std::map<std::uint32_t, Eigen::Matrix32f> aabbsEigen;
        for (auto& pair : aabbs)
        {
            auto label = pair.first;
            auto& aabb = pair.second;

            aabbsEigen[label] = aabb.limits();
        }
        return getShapesFromAABBs(aabbsEigen);
    }

    ShapeList getShapesFromAABBs(const pcl::PointCloud<pcl::PointXYZL>& pointCloud)
    {
        return getShapesFromAABBs<pcl::PointXYZL>(pointCloud);
    }

    ShapeList getShapesFromAABBs(const pcl::PointCloud<pcl::PointXYZRGBL>& pointCloud)
    {
        return getShapesFromAABBs<pcl::PointXYZRGBL>(pointCloud);
    }

    ShapeList getShapesFromAABBs(const std::map<uint32_t, Eigen::Matrix32f>& segmentAABBs)
    {
        semrel::ShapeList shapes;
        shapes.reserve(segmentAABBs.size());
        for (const auto& [label, aabb] : segmentAABBs)
        {
            shapes.push_back(std::make_unique<semrel::Box>(aabb, semrel::ShapeID(label)));
        }
        return shapes;
    }

    ShapeList getShapesFromSoftAABBs(
        const pcl::PointCloud<pcl::PointXYZRGBL>& pointCloud, float outlierRatio)
    {
        const std::map<uint32_t, pcl::PointIndices> segmentIndices = visionx::tools::getSegmentIndices(pointCloud, false);

        std::map<uint32_t, Eigen::Matrix32f> segmentAABBs;
        for (const auto& [label, indices] : segmentIndices)
        {
            semrel::SoftMinMax minMaxX(outlierRatio, indices.indices.size());
            semrel::SoftMinMax minMaxY(outlierRatio, indices.indices.size());
            semrel::SoftMinMax minMaxZ(outlierRatio, indices.indices.size());

            for (int index : indices.indices)
            {
                const auto& point = pointCloud[static_cast<std::size_t>(index)];

                minMaxX.add(point.x);
                minMaxY.add(point.y);
                minMaxZ.add(point.z);
            }

            Eigen::Matrix32f aabb;
            aabb(0, 0) = minMaxX.getSoftMin();
            aabb(0, 1) = minMaxX.getSoftMax();
            aabb(1, 0) = minMaxY.getSoftMin();
            aabb(1, 1) = minMaxY.getSoftMax();
            aabb(2, 0) = minMaxZ.getSoftMin();
            aabb(2, 1) = minMaxZ.getSoftMax();
            segmentAABBs[label] = aabb;
        }

        return getShapesFromAABBs(segmentAABBs);
    }

    Eigen::Matrix<float, 3, 2>
    getSoftAABB(const pcl::PointCloud<pcl::PointXYZRGBL>& pointCloud,
                const pcl::PointIndices& indices,
                float outlierRatio)
    {
        ARMARX_CHECK_LESS_EQUAL(0, outlierRatio);
        ARMARX_CHECK_LESS_EQUAL(outlierRatio, 0.5f);

        semrel::SoftMinMax minMaxX(outlierRatio, indices.indices.size());
        semrel::SoftMinMax minMaxY(outlierRatio, indices.indices.size());
        semrel::SoftMinMax minMaxZ(outlierRatio, indices.indices.size());

        for (int index : indices.indices)
        {
            const auto& point = pointCloud[static_cast<std::size_t>(index)];

            minMaxX.add(point.x);
            minMaxY.add(point.y);
            minMaxZ.add(point.z);
        }

        Eigen::Matrix32f aabb;
        aabb(0, 0) = minMaxX.getSoftMin();
        aabb(0, 1) = minMaxX.getSoftMax();
        aabb(1, 0) = minMaxY.getSoftMin();
        aabb(1, 1) = minMaxY.getSoftMax();
        aabb(2, 0) = minMaxZ.getSoftMin();
        aabb(2, 1) = minMaxZ.getSoftMax();

        return aabb;
    }

    std::map<uint32_t, Eigen::Matrix<float, 3, 2>>
            getSoftAABBs(const pcl::PointCloud<pcl::PointXYZRGBL>& pointCloud,
                         const std::map<uint32_t, pcl::PointIndices>& segmentIndices,
                         float outlierRatio)
    {
        std::map<uint32_t, Eigen::Matrix32f> segmentAABBs;
        for (const auto& [label, indices] : segmentIndices)
        {
            segmentAABBs[label] = getSoftAABB(pointCloud, indices, outlierRatio);;
        }
        return segmentAABBs;
    }

}
