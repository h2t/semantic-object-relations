/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::SupportAnalyzer
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <SemanticObjectRelations/SupportAnalysis/SupportAnalysis.h>

#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>

#include <VisionX/libraries/VisionXComponentPlugins/SemanticGraphStorageComponentPlugin.h>


#include <SemantiX/interface/SupportAnalyzer.h>


namespace armarx
{

    /// @class SupportAnalyzerPropertyDefinitions
    class SupportAnalyzerPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        SupportAnalyzerPropertyDefinitions(std::string prefix);
    };

    /**
     * @defgroup Component-SupportAnalyzer SupportAnalyzer
     * @ingroup SemantiX-Components
     * A description of the component SupportAnalyzer.
     *
     * @class SupportAnalyzer
     * @ingroup Component-SupportAnalyzer
     * @brief Brief description of class SupportAnalyzer.
     *
     * Detailed description of class SupportAnalyzer.
     */
    class SupportAnalyzer
        : virtual public armarx::Component
        , virtual public SupportAnalyzerInterface
        , virtual public armarx::SemanticGraphStorageComponentPluginUser
    {

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // SupportAnalyzerInterface
        SupportGraphBasePtr extractSupportGraph(
            const ShapeBaseListPtr& objects,
            const ShapeIdList& safeObjectIDs,
            const Ice::Current& = Ice::emptyCurrent) override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        Eigen::Vector3f getGravityFromRobotStateComponent();


        /// The support analysis.
        semrel::SupportAnalysis supportAnalysis;


        // PROXIES

        /// The robot state component.
        RobotStateComponentInterfacePrx robotStateComponent;

        /// The shape listener proxy.
        SupportGraphListenerPrx supportGraphListener;

        /// The debug drawer topic.
        DebugDrawerTopic debugDrawer;


        // OPTIONS

        std::string pointCloudFrameName;

        /// If true, enable life visualization.
        bool verboseVisualization = false;

    };
}
