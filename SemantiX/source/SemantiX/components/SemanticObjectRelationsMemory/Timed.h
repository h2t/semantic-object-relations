#pragma once

#include <IceUtil/Time.h>


namespace armarx
{
    template <class _ValueT>
    struct Timed
    {
        using ValueT = _ValueT;

        IceUtil::Time time;
        ValueT value;
    };
}
