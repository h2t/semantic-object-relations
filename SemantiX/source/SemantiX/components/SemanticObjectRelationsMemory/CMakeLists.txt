armarx_component_set_name("SemanticObjectRelationsMemory")


set(COMPONENT_LIBS
    ArmarXCore
    ArmarXCoreInterfaces  # for DebugObserverInterface
    RobotAPICore

    SemantiXVariants SemantiXHooks
    SemantiXInterfaces
)

set(SOURCES
    History.cpp
    SemanticObjectRelationsMemory.cpp
    Timed.cpp
)
set(HEADERS
    History.h
    SemanticObjectRelationsMemory.h
    Timed.h
)


armarx_add_component("${SOURCES}" "${HEADERS}")

# Add unit tests.
add_subdirectory(test)


# Add application.
armarx_component_set_name("SemanticObjectRelationsMemoryApp")
set(COMPONENT_LIBS SemanticObjectRelationsMemory)
armarx_add_component_executable(main.cpp)
