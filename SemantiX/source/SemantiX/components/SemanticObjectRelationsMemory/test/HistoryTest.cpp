/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::SemanticObjectRelationsMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE SemantiX::ArmarXObjects::SemanticObjectRelationsMemory

#define ARMARX_BOOST_TEST

#include <iostream>
#include <memory>

#include <SemantiX/Test.h>
#include <SemantiX/components/SemanticObjectRelationsMemory/History.h>


using namespace armarx;


struct Fixture
{
};


BOOST_FIXTURE_TEST_SUITE(HistoryTest, Fixture)


BOOST_AUTO_TEST_CASE(test_insert_int)
{
    // Copyable type.
    History<int> history;
    const int i = 2;

    history.insert(IceUtil::Time::seconds(2), i);

    BOOST_CHECK_EQUAL(history.getLatestValue(), i);
}


BOOST_AUTO_TEST_CASE(test_insert_unique)
{
    // Non-copyable type.
    History<std::unique_ptr<int>> history;

    // Construction in-place.
    history.insert(IceUtil::Time::seconds(2), std::make_unique<int>(4));
    BOOST_CHECK_EQUAL(*history.getLatestValue(), 4);

    // Passed via std::move()
    std::unique_ptr<int> u = std::make_unique<int>(3);
    history.insert(IceUtil::Time::seconds(4), std::move(u));
    BOOST_CHECK(!u);
    BOOST_CHECK_EQUAL(*history.getLatestValue(), 3);
}


BOOST_AUTO_TEST_CASE(test_insert_max_size)
{
    // Copyable type.
    const std::size_t maxSize = 3;
    History<int> history(maxSize);
    BOOST_CHECK_EQUAL(history.size(), 0);

    history.insert(IceUtil::Time::seconds(2), 1);
    history.insert(IceUtil::Time::seconds(3), 2);
    history.insert(IceUtil::Time::seconds(4), 3);
    BOOST_CHECK_EQUAL(history.size(), maxSize);
    BOOST_CHECK_EQUAL(history.getLatestValue(), 3);

    history.insert(IceUtil::Time::seconds(5), 4);
    BOOST_CHECK_EQUAL(history.size(), maxSize);
    BOOST_CHECK_EQUAL(history.getLatestValue(), 4);
}


BOOST_AUTO_TEST_SUITE_END()
