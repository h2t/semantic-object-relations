#pragma once

#include <map>

#include <IceUtil/Time.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "Timed.h"


namespace armarx
{

    template <class _ValueT>
    class History
    {
    public:
        using ValueT = _ValueT;
        using Time = IceUtil::Time;

    public:

        /// Construct an empty history.
        History() = default;
        History(std::size_t maxSize) : maxSize(maxSize) {}

        void insert(Time time, const ValueT& value);
        void insert(Time time, ValueT&& value);

        Timed<const ValueT&> getLatest() const;
        Timed<ValueT&> getLatest();

        const ValueT& getLatestValue() const;
        ValueT& getLatestValue();

        bool empty() const
        {
            return history.empty();
        }
        std::size_t size() const
        {
            return history.size();
        }

        std::size_t getMaxSize() const
        {
            return maxSize;
        }
        void setMaxSize(std::size_t size)
        {
            this->maxSize = size;
            ensureMaxSize();
        }


    private:

        /// Ensure that history size does not exceed `maxSize` by deleting oldest entries.
        void ensureMaxSize();


    private:

        /// The history.
        std::map<Time, ValueT> history;

        /// Maximal history size, if > 0.
        std::size_t maxSize = 0;

    };


    template <class V>
    void History<V>::insert(Time time, const ValueT& value)
    {
        history[time] = value;
        ensureMaxSize();
    }


    template <class V>
    void History<V>::insert(Time time, ValueT&& value)
    {
        history[time] = std::move(value);
        ensureMaxSize();
    }


    template <class V>
    auto History<V>::getLatest() const -> Timed<const ValueT&>
    {
        const auto& item = history.rbegin();
        return { item->first, item->second };
    }

    template <class V>
    auto History<V>::getLatest() -> Timed<ValueT&>
    {
        const auto& item = history.rbegin();
        return { item->first, item->second };
    }

    template <class V>
    auto History<V>::getLatestValue() const -> const ValueT&
    {
        ARMARX_CHECK(!empty());
        return history.rbegin()->second;
    }
    template <class V>
    auto History<V>::getLatestValue() -> ValueT&
    {
        ARMARX_CHECK(!empty());
        return history.rbegin()->second;
    }

    template <class V>
    void History<V>::ensureMaxSize()
    {
        while (maxSize > 0 && history.size() > maxSize)
        {
            history.erase(history.begin());
        }
    }


}

