armarx_component_set_name("SupportGraphExtractor")


set(COMPONENT_LIBS
    ArmarXCore ArmarXCoreInterfaces  # for DebugObserverInterface
    RobotAPICore  # for DebugDrawerTopic

    SemanticObjectRelations

    SemantiXInterfaces SemantiXVariants
)

set(SOURCES
    SupportGraphExtractor.cpp
)
set(HEADERS
    SupportGraphExtractor.h
)


armarx_add_component("${SOURCES}" "${HEADERS}")

#find_package(MyLib QUIET)
#armarx_build_if(MyLib_FOUND "MyLib not available")
# all target_include_directories must be guarded by if(Xyz_FOUND)
# for multiple libraries write: if(X_FOUND AND Y_FOUND)....
#if(MyLib_FOUND)
#    target_include_directories(SupportGraphExtractor PUBLIC ${MyLib_INCLUDE_DIRS})
#endif()

# add unit tests
add_subdirectory(test)


armarx_component_set_name("SupportGraphExtractorApp")
set(COMPONENT_LIBS SupportGraphExtractor)
armarx_add_component_executable(main.cpp)
