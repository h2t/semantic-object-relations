/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::SupportGraphExtractor
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SemanticObjectRelations/SupportAnalysis/SupportAnalysis.h>

#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>

#include <SemantiX/interface/ShapeListener.h>
#include <SemantiX/interface/SupportGraphListener.h>


namespace semantix
{
    /**
     * @class SupportGraphExtractorPropertyDefinitions
     * @brief
     */
    class SupportGraphExtractorPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        SupportGraphExtractorPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-SupportGraphExtractor SupportGraphExtractor
     * @ingroup SemantiX-Components
     * A description of the component SupportGraphExtractor.
     *
     * @class SupportGraphExtractor
     * @ingroup Component-SupportGraphExtractor
     * @brief Brief description of class SupportGraphExtractor.
     *
     * Detailed description of class SupportGraphExtractor.
     */
    class SupportGraphExtractor :
        virtual public armarx::Component,
        virtual public ShapeListener
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;

        // ShapeListener interface
        void reportShapes(const TimedShapeBaseList& shapes, const Ice::Current&) override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        virtual void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        virtual void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        virtual void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        virtual void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        semrel::ShapeID findTable(const semrel::ShapeMap& shapes);


    private:

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;
        /// Debug drawer. Used for 3D visualization.
        armarx::DebugDrawerTopic debugDrawer;


        /// The support analysis.
        semrel::SupportAnalysis supportAnalysis;
        Eigen::Vector3f gravity = - Eigen::Vector3f::UnitZ();


        // PROXIES

        /// The shape listener proxy.
        SupportGraphListenerPrx supportGraphListener;


        // OPTIONS

        std::string pointCloudFrameName;

        /// If true, enable life visualization.
        bool verboseVisualization = false;

    };
}
