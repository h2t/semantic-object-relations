# Semantic Object Relations for Robotic Applications

Robots need to understand how objects in its environment physically interact with each other.
This library provides methods to extract a support relation between objects.
A support relation is represented as a graph, in which nodes are objects and edges indicate support.
We consider object A to support object B, iff removing A causes B to lose its motionless state.

## Example

The following example illustrates how to extract support relations from a segmented point cloud.

Input Point Cloud (Color)     |  Input Point Cloud (Labels)  | Extracted Support Graph
:----------------------------:|:----------------------------:|:----------------------------:
![Input Point Cloud (Color)](SemanticObjectRelations/data/readme/example-point-cloud-color.png)  |  ![Input Point Cloud (Labels)](SemanticObjectRelations/data/readme/example-point-cloud-labels.png) | ![Extracted Support Graph](SemanticObjectRelations/data/readme/example-support-graph.png)


You can find the example code to generate the support graph above in [example.cpp](example.cpp).

```cpp
#include <SemanticObjectRelations/Hooks/Log.h>
#include <SemanticObjectRelations/ShapeExtraction/ShapeExtraction.h>
#include <SemanticObjectRelations/SupportAnalysis/SupportAnalysis.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


int main(int argc, const char* argv[])
{
    semrel::LogInterface::setMinimumLogLevel(semrel::LogLevel::INFO);
    
    // pcd file with example point cloud
    std::string pcdFilename = "segmented-point-cloud.pcd";
    if (argc > 1)
    {
        pcdFilename = argv[1];
    }
    // output filename
    std::string graphOutputFilename = "support-graph";
    if (argc > 2)
    {
        graphOutputFilename = argv[2];
    }
      
    // load point cloud
    std::cout << "Loading point cloud from '" << pcdFilename << "' ..." << std::endl;
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr pointcloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
    if (pcl::io::loadPCDFile<pcl::PointXYZRGBL>(pcdFilename, *pointcloud))
    {
        std::cout << "Could not load PCD file '" << pcdFilename << "'." << std::endl;;
        return -1;
    }
    std::cout << "Loaded point cloud with " << pointcloud->size() << " points." << std::endl;;
    
    // extract objects
    semrel::ShapeExtraction shapeExtraction;
    shapeExtraction.setMaxIterations(100);
    shapeExtraction.setDistanceThreshold(5.0);
    shapeExtraction.setMinInlierRate(0.7);
    shapeExtraction.setOutlierRate(0.00125);
    shapeExtraction.setConcurrencyEnabled(true);
    
    std::cout << "Running shape extraction ..." << std::endl;;
    semrel::ShapeExtractionResult shapexResult = shapeExtraction.extract(*pointcloud);
    std::cout << "Extracted shapes: " << shapexResult.objects << std::endl;;
    
    // analyze support relations
    semrel::SupportAnalysis supportAnalysis;
    supportAnalysis.setGravityVector(- Eigen::Vector3f::UnitZ());
    supportAnalysis.setContactMargin(10);
    supportAnalysis.setVertSepPlaneAngleMax(10);
    supportAnalysis.setSupportAreaRatioMin(0.7);
    
    std::cout << "Performing support analysis ..." << std::endl;
    semrel::SupportGraph supportGraph = supportAnalysis.performSupportAnalysis(shapexResult.objects);
    
    std::cout << "Support graph: " << supportGraph << std::endl;
    
    // write a png file of the support graph
    semrel::SupportGraph::getGraphvizWriter().write(supportGraph, graphOutputFilename);

    return 0;
}
```

## Citation

If you use this code, please consider citing the following paper: [IEEE Xplore](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8419333&isnumber=8386768)

    @ARTICLE{KartmannPaus2018,
      author                     = {R. Kartmann and F. Paus and M. Grotz and T. Asfour},
      journal                    = {IEEE Robotics and Automation Letters (RA-L)} },
      title                      = {Extraction of Physically Plausible Support Relations to Predict and Validate Manipulation Action Effects},
      year                       = {2018},
      volume                     = {3},
      number                     = {4},
      pages                      = {3991--3998},
      doi                        = {10.1109/LRA.2018.2859448},
      ISSN                       = {2377-3766},
      month                      = {October}
    }

If you encounter any problems with the code, please contact us via the issue tracker or mail:
* Fabian Paus: paus@kit.edu
* Rainer Kartmann: rainer.kartmann@student.kit.edu

## Further Documentation

You can find further documentation on how to build and use the library in the  [Wiki](https://gitlab.com/h2t/semantic-object-relations/wikis/home).
Additionally, we include a guide on how to reproduce the [evaluation results](https://gitlab.com/h2t/semantic-object-relations/wikis/evaluation) presented in the paper.
