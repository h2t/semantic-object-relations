#include "ContactDetectionVisualizer.h"

using namespace semrel;


ContactDetectionVisualizer& ContactDetectionVisualizer::get()
{
    static ContactDetectionVisualizer instance;
    return instance;
}

void ContactDetectionVisualizer::drawContactPoints(const ContactPointList& contactPointList)
{
    draw(VisuLevel::VERBOSE, [&]
    {
        for (std::size_t i = 0; i < contactPointList.size(); ++i)
        {
            drawContactPoint(contactPointList[i], i);
        }
    });
}


void ContactDetectionVisualizer::drawShapeMargin(const Shape& shape, float margin)
{
    draw(VisuLevel::VERBOSE, [&]
    {
        if (dynamic_cast<const Box*>(&shape))
        {
            drawBoxMargin(*static_cast<const Box*>(&shape), margin);
        }
        else if (dynamic_cast<const Cylinder*>(&shape))
        {
            drawCylinderMargin(*static_cast<const Cylinder*>(&shape), margin);
        }
        else if (dynamic_cast<const Sphere*>(&shape))
        {
            drawSphereMargin(*static_cast<const Sphere*>(&shape), margin);
        }
        else if (dynamic_cast<const MeshShape*>(&shape))
        {
            drawMeshShapeMargin(*static_cast<const MeshShape*>(&shape), margin);
        }
        else
        {
            // should not be reached
            std::stringstream msg;
            msg << "Received shape of unexpected type: \n" << shape.str();
            throw std::invalid_argument(msg.str());
        }
    });
}

void ContactDetectionVisualizer::drawBoxMargin(const Box& box, float margin)
{
    Box scaled(box);
    scaled.addMargin(margin);
    drawBox(VisuLevel::VERBOSE, scaled, DrawColor(0., 0., 0.8f, 0.3f));
}

void ContactDetectionVisualizer::drawCylinderMargin(const Cylinder& cylinder, float margin)
{
    Cylinder scaled(cylinder);
    scaled.addMargin(margin);
    drawCylinder(VisuLevel::VERBOSE, scaled, DrawColor(0., 0.8f, 0, 0.3f));
}

void ContactDetectionVisualizer::drawSphereMargin(const Sphere& sphere, float margin)
{
    Sphere scaled = sphere;
    scaled.setRadius(scaled.getRadius() + margin);
    drawSphere(VisuLevel::VERBOSE, scaled, DrawColor(0.8f, 0, 0, 0.3f));
}

void ContactDetectionVisualizer::drawMeshShapeMargin(const MeshShape& meshShape, float margin)
{
    MeshShape scaled(meshShape);
    scaled.addMargin(margin);
    drawMeshShape(VisuLevel::VERBOSE, scaled, DrawColor(0.5f, 0.5f, 0.5f, 0.3f));
}

void ContactDetectionVisualizer::drawContactPoint(const ContactPoint& cp, std::size_t id)
{
    draw(VisuLevel::VERBOSE, [&]
    {
        std::stringstream ss;
        ss << "contactPos_" << id;
        
        drawImportantPoint(ss.str(), cp.getPosition(), 7, redGreenBlue(0));
    
        impl().drawArrow(visuID(int(id), "contactNormalB_"),
                         cp.getPosition(), cp.getNormalOnB(), 
                         20, 1.0, redGreenBlue(0));
    });
}

