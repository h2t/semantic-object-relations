#pragma once

#include <SemanticObjectRelations/ContactDetection/ContactPoint.h>

#include "Visualizer.h"


namespace semrel
{

class ContactDetectionVisualizer : public Visualizer
{
public:
    
    static ContactDetectionVisualizer& get();
    
    
    ContactDetectionVisualizer() : Visualizer("ContactDetection") {}

    void drawContactPoints(const ContactPointList& contactPointList);
    
    void drawShapeMargin(const Shape& shape, float margin);
    
    void drawBoxMargin(const Box& box, float margin);
    void drawCylinderMargin(const Cylinder& cylinder, float margin);
    void drawSphereMargin(const Sphere& sphere, float margin);
    void drawMeshShapeMargin(const MeshShape& meshShape, float margin);


private:
    
    void drawContactPoint(const ContactPoint& contactPoint, std::size_t id);
    
    
};

}
