#include "SupportAnalysisVisualizer.h"

using namespace semrel;
namespace bg = boost::geometry;

SupportAnalysisVisualizer& SupportAnalysisVisualizer::get()
{
    static SupportAnalysisVisualizer visu;
    return visu;
}


void SupportAnalysisVisualizer::drawGravity(const Eigen::Vector3f& gravity, const Eigen::Vector3f& at)
{
    draw(VisuLevel::VERBOSE, [&]
    {
        DrawColor color(1.0, 0.25, 0., 0.375);
        float length = 300;
        float width = 30;

        impl().drawArrow(visuID("gravity"), at, gravity, length, width, color);
    });
}

void SupportAnalysisVisualizer::drawSeparatingPlane(const Hyperplane3f& sepPlane, const Eigen::Vector3f& at, ShapeID idA, ShapeID idB)
{
    std::stringstream name;
    name << "sepplane_" << idA << "_" << idB;
    drawPlane(VisuLevel::VERBOSE, name.str(), sepPlane, at, 150, 50);
}

void SupportAnalysisVisualizer::drawActGraph(const SupportGraph& act, const ShapeMap& objects)
{
    drawSupportGraph(VisuLevel::VERBOSE, "act", act, objects, {1, .5, 0}, {1, 0, 1}, {0, 0, 0});
}

void SupportAnalysisVisualizer::drawSupportGraph(const SupportGraph& graph, const ShapeMap& objects)
{
    drawSupportGraph(VisuLevel::RESULT, "support", graph, objects, {}, {0, 0, 1}, {1, 0, 0});
}

void SupportAnalysisVisualizer::drawSupportPolygon(
        const SupportPolygon& supportPolygon, const Shape& object)
{
    draw(VisuLevel::VERBOSE, [&]
    {
        Polygon2D objectProjection = supportPolygon.getSupportedObjectProjection();
        Polygon2D supportPoly = supportPolygon.getSupportPolygon();

        Eigen::Vector3f basisA = supportPolygon.getBasis().first;
        Eigen::Vector3f basisB = supportPolygon.getBasis().second;

        drawProjectionPolygon(
                    visuID(object.getID(), "projectionPolygon_"), objectProjection,
                    basisA, basisB, {0, 1, 1}, 1.);

        drawProjectionPolygon(
                    visuID(object.getID(), "supportPolygon_"), supportPoly,
                    basisA, basisB, {0, 1, 0}, 1.);

        // object ID and basis

        Point2D centroid2D;
        bg::centroid(objectProjection, centroid2D);

        Eigen::Vector3f centroid3D = supportPolygon.to3DGround(centroid2D);

        std::stringstream ssID;
        ssID << object.getID();

        impl().drawText(visuID(object.getID(), "polygonsLabel"), ssID.str(),
                        centroid3D, 10, {});

        // basis

        impl().drawArrow(visuID(object.getID(), "supportPolygonBasisA_"),
                         centroid3D, basisA, 10, 0.5, {1, 0, 0});

        impl().drawArrow(visuID(object.getID(), "supportPolygonBasisB_"),
                         centroid3D, basisB, 10, 0.5, {0, 1, 0});

    });
}

void SupportAnalysisVisualizer::drawSupportGraph(
        VisuLevel level, const std::string& namePrefix,
        const SupportGraph& graph, const ShapeMap& objects,
        DrawColor edgeColorBase, DrawColor edgeColorVert, DrawColor edgeColorUD)
{
    draw(level, [&]
    {
        Eigen::Vector3f vertexOffset = Eigen::Vector3f(0, 0, 5);

        for (SupportGraph::ConstEdge edge : graph.edges())
        {
            SupportGraph::ConstVertex a = edge.source();
            SupportGraph::ConstVertex b = edge.target();

            DrawColor color = edgeColorBase;
            std::string label = "";
            if (edge.attrib().fromUncertaintyDetection)
            {
                color = edgeColorUD;
                label = "UD";
            }
            else if (edge.attrib().verticalSeparatingPlane)
            {
                color = edgeColorVert;
                label = "V";
            }

            ShapeID idA = a.objectID();
            ShapeID idB = b.objectID();

            Eigen::Vector3f posA = objects.at(idA)->getPosition() + vertexOffset * impl().metricScaling;
            Eigen::Vector3f posB = objects.at(idB)->getPosition() - vertexOffset * impl().metricScaling;

            float dist = (posA - posB).norm();
            float length = dist - 5 * impl().metricScaling;
            float arrowWidth = 2.f * impl().metricScaling;

            std::stringstream ss;
            ss << namePrefix << "_arrow_" << idA << "_" << idB;
            impl().drawArrow(visuID(ss.str()), posA, (posB - posA) / dist,
                             length, arrowWidth, color);

            float labelSize = 10.f * impl().textScaling;
            if (!label.empty())
            {
                ss << "_label";
                impl().drawText(visuID(ss.str()), label, 0.5 * (posA+posB), labelSize, {});
            }
        }
    });
}

void SupportAnalysisVisualizer::drawProjectionPolygon(
    VisuMetaInfo id,
    const SupportAnalysisVisualizer::Polygon2D& polygon,
    const Eigen::Vector3f& basisA,
    const Eigen::Vector3f& basisB,
    const DrawColor& color,
    float lineWidth)
{
    DrawColor inner = {color.r, color.g, color.b, 0.3f };
    DrawColor border = {color.r, color.g, color.b, 1.0 };

    std::vector<Eigen::Vector3f> points;
    for (Point2D p : polygon.outer())
    {
        Eigen::Vector3f point3D = basisA * bg::get<0>(p) + basisB * bg::get<1>(p);
        points.push_back(point3D);
    }

    impl().drawPolygon(id, points, lineWidth, inner, border);
}
