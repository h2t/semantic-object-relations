#pragma once

#include <SemanticObjectRelations/Shapes.h>

#include "SpatialGraph.h"


namespace semrel::spatial
{

    /// Parameters for spatial relation evaluation.
    struct Parameters
    {
        /// The distance threshold, e.g. to decide whether objects moved.
        float distanceEqualityThreshold = 30;
    };


    /**
     * @brief Evalaute the spatial relations between given current and past
     * objects and return them as a `SpatialGraph`.
     *
     * @param currentObjects The objects at time `t`.
     * @param pastObjects The objects at time `t - 1`.
     * @param parameters The parameters.
     * @return The spatial relations as `SpatialGraph`.
     */
    SpatialGraph evaluateRelations(const ShapeMap& currentObjects, const ShapeMap& pastObjects,
                                   const Parameters& parameters = {});

    /**
     * @brief Computes the contact and static spatial relations between given
     * objects and returns them as a `SpatialGraph`.
     *
     * @param objects The objects at time `t`.
     * @return The spatial relations as `SpatialGraph`.
     */
    SpatialGraph evaluateStaticRelations(const ShapeMap& objects);


    /**
     * @brief Construct a SpatialGraph for the given objects.
     *
     * The returned graph:
     * - has nodes for each object in `currentObjects`.
     * - has AABBs for current and past objects stored in vertex attributes.
     * - has edges between each pair of objects (without self loops).
     *
     * If an object ID in `currentObjects` is not found in `pastObjects`, the
     * corresponding AABB are set empty.
     */
    SpatialGraph initializeGraph(const ShapeMap& currentObjects, const ShapeMap& pastObjects);


    /**
     * @brief Evaluate the contact relation.
     * @param graph The graph (already containing bidirectional edges).
     */
    void evaluateContactRelations(SpatialGraph& graph);


    /**
     * @brief Evaluate the static relations.
     * For static contact relations, `evaluateContactRelations()` must be called beforehand.
     *
     * @param graph The graph (already containing bidirectional edges).
     */
    void evaluateStaticRelations(SpatialGraph& graph);


    /**
     * @brief Evaluate the dynamic relations.
     * @param graph The graph (already containing bidirectional edges).
     * @param parameters The parameters.
     */
    void evaluateDynamicRelations(SpatialGraph& graph, const Parameters& parameters = {});

}
