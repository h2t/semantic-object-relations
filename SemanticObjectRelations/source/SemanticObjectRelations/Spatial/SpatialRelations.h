#pragma once


// STD/STL
#include <bitset>
#include <map>
#include <vector>


namespace semrel
{

    /**
     * @brief Spatial relations based on [1].
     *
     * Static relations use the following connotation of axes:
     * - x: left / right
     * - y: below / above
     * - z: back / front
     *
     *
     *
     * [1] F. Ziaeetabar, T. Kulvicius, M. Tamosiunaite, and F. Wörgötter,
     * ‘Recognition and Prediction of Manipulation Actions Using Enriched
     * Semantic Event Chains’, RAS, vol. 110, pp. 173--188, Dec. 2018.
     */
    class SpatialRelations
    {
    public:

        /// Spatial relation types.
        enum class Type : int
        {
            contact = 0,

            // Static
            static_above = 1,
            static_below = 2,
            static_left_of = 3,
            static_right_of = 4,
            static_behind = 5,
            static_in_front_of = 6,
            static_around = 7,  // <=> above or below or right or left or behind
            static_inside = 8,
            static_surrounding = 9,

            // Static contact
            static_bottom = 10,  // <=> contact and below
            static_top = 11,  // <=> contact and above
            static_contact_around = 12,  // <=> contact and around

            // Dynamic
            dynamic_moving_together = 13,
            dynamic_halting_together = 14,
            dynamic_fixed_moving_together = 15,
            dynamic_getting_close = 16,
            dynamic_moving_apart = 17,
            dynamic_stable = 18,

            num_relations = 19
        };

        /// All relations types.
        static const std::vector<Type> types;

        /// Static relation types (excluding contact).
        static const SpatialRelations staticTypes;
        /// Dynamic relation types (excluding contact).
        static const SpatialRelations dynamicTypes;

        /// Human-readable names for relation types.
        static const std::map<Type, std::string> names;


    public:

        /// Default constructor where no relation is active.
        SpatialRelations();

        /// Construct relations with
        SpatialRelations(const std::vector<Type>& activeRelations);

        /// Get whether the given relation type is active.
        bool get(Type type) const;
        /// Set whether the relation type is active.
        void set(Type type, bool value);


        /**
         * @brief Filter the relations by the given mask.
         * @param filterMask Mask to be applied as filter.
         * @return New instance of relations, filtered by `filterMask`.
         */
        SpatialRelations filter(const SpatialRelations& filterMask) const;
        /// Get whether any relation is active.
        bool any() const;


        /// Get the names of the active relations.
        std::vector<std::string> getStringList() const;


    private:
        // STATIC

        /// Typedef for a bitset large enough to hold all relations.
        using relations_bitset = std::bitset<static_cast<int>(Type::num_relations)>;

        static std::map<Type, std::string> makeRelationNames();
        static std::vector<Type> makeRelationTypes();


    private:

        /// @brief Implementation-relevant constructor to construct a relations object from a bitset.
        SpatialRelations(const relations_bitset&);


        /**
         * @brief Active relations represented by a bitset, where HI means
         * that the given relation is active, and LO that a given relation is
         * not active.
         */
        relations_bitset _bitset;

    };


    std::ostream& operator<<(std::ostream& os, SpatialRelations::Type type);
    std::ostream& operator<<(std::ostream& os, const SpatialRelations& rels);


}
