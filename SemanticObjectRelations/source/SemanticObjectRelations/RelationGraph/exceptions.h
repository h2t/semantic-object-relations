#pragma once

#include <sstream>
#include <stdexcept>

#include <SemanticObjectRelations/exceptions.h>
#include <SemanticObjectRelations/Shapes/Shape.h>


namespace semrel::error
{
    /// Base class for all exceptions thrown by `RelationGraph`.
    class RelationGraphError : public SemanticObjectRelationsError
    {
    public:
        RelationGraphError(const std::string& msg, const std::string& graphStr = "");
    };


    /// Indicates that there is no vertex at a given index.
    class NoVertexAtIndex : public RelationGraphError
    {
    public:
        NoVertexAtIndex(std::size_t index, std::size_t numVertices);
    };


    /// Indicates that there is no vertex with a given (shape) ID.
    class NoVertexWithID : public RelationGraphError
    {
    public:
        NoVertexWithID(semrel::ShapeID id, const std::string& graphStr = "");
    };


    /// Indicates that there was no edge between two vertices.
    class NoEdgeBetween : public RelationGraphError
    {
    public:
        template <typename VertexDescriptor>
        NoEdgeBetween(const VertexDescriptor& from, const VertexDescriptor& to, const std::string& graphStr = "") :
            RelationGraphError(makeMsg(from, to), graphStr)
        {}

    private:
        template <typename VertexDescriptor>
        static std::string makeMsg(const VertexDescriptor& from, const VertexDescriptor& to)
        {
            std::stringstream ss;
            ss << "No edge from " << from << " to " << to << ".";
            return ss.str();
        }
    };


    /// Indicates that the object pointer in a vertex was null when trying to access it.
    class ObjectPointerInVertexIsNull : public RelationGraphError
    {
    public:
        template <typename Vertex>
        ObjectPointerInVertexIsNull(const Vertex& vertex, const std::string& graphStr = "") :
            RelationGraphError(makeMsg(vertex), graphStr)
        {}

    private:
        template <typename Vertex>
        static std::string makeMsg(const Vertex& vertex)
        {
            std::stringstream ss;
            ss << "Object pointer in vertex " << vertex << " with ID " << vertex.objectID() << " is null.";
            return ss.str();
        }
    };

}
