#pragma once

#include <SemanticObjectRelations/RelationGraph/json/attributes.h>
#include <SemanticObjectRelations/Serialization/type_name.h>
#include <SemanticObjectRelations/Serialization/exceptions.h>
#include <SemanticObjectRelations/RelationGraph/RelationGraph.h>


namespace semrel
{

    /**
     * @brief Serialized vertex attributes.
     *
     * The following keys have a special meaning:
     *
     * - "object" (`KEY_OBJECT`)
     *   - The object represented by this vertex.
     */
    struct AttributedVertex : public semrel::ShapeVertex
    {
        static const std::string KEY_OBJECT;

        /// Serialize this object into `json`.
        void setObject(const Shape& object);
        /// Deserialize this object from `json`.
        /// Returns null if there is no object information.
        ShapePtr getObject() const;

        /// The serialized attributes.
        nlohmann::json json;
    };

    /// Serialized edge attributes.
    struct AttributedEdge
    {
        /// The serialized attributes.
        nlohmann::json json;
    };

    /**
     * @brief Serialized graph attributes.
     *
     * The following keys have a special meaning:
     *
     * - "type" (`KEY_TYPE_NAME`)
     *   - The (demangled) type name of the serialized graph.
     */
    struct AttributedGraphAttributes
    {
        static const std::string KEY_TYPE_NAME;

        /// Store the type name in `json`.
        void setTypeName(const std::string& typeName);
        /// Get the type name from `json`.
        /// Returns an empty string if no type name is stored.
        std::string getTypeName() const;

        /// The serialized attributes.
        nlohmann::json json;
    };

    /**
     * @brief A RelationGraph with serialized attributes in JSON format.
     */
    class AttributedGraph :
            public semrel::RelationGraph<AttributedVertex, AttributedEdge, AttributedGraphAttributes>
    {
    public:

        using RelationGraph::RelationGraph;

        /// Serialize `graph` into `*this`.
        template <class GraphT>
        void fromGraph(const GraphT& graph);

        /// Deserialize `*this` into a `GraphT`.
        template <class GraphT>
        GraphT toGraph() const;


        /// Write object information into vertex attributes (according to vertices' object IDs).
        void serializeObjects(const ShapeMap& objects);
        /// Read objects from vertex attributes (if vertices contain object information).
        ShapeList deserializeObjects() const;

        /// Get the stored type name. Returns an empty string if none is stored.
        std::string getTypeName() const;
        /// Set the type name.
        void setTypeName(const std::string& typeName);

    };


    template <typename VA, typename EA, typename GA>
    void toAttributedGraph(AttributedGraph& result, const RelationGraph<VA, EA, GA>& input)
    {
        using GraphT = RelationGraph<VA, EA, GA>;

        // Serialize graph attributes.
        result.attrib().json = input.attrib();
        result.setTypeName(serial::getTypeName(input));

        std::map<typename GraphT::VertexDescriptor, AttributedGraph::VertexDescriptor> input2result;

        for (auto inputVertex : input.vertices())
        {
            // Add vertex.
            AttributedGraph::Vertex resultVertex = result.addVertex(inputVertex.objectID());
            // Serialize vertex attributes.
            resultVertex.attrib().json = inputVertex.attrib();
            // Store new vertex descriptor.
            input2result.emplace(inputVertex.descriptor(), resultVertex.descriptor());
        }

        for (auto edge : input.edges())
        {
            // Find vertex descriptors.
            auto sourceDesc = input2result.at(edge.sourceDescriptor());
            auto targetDesc = input2result.at(edge.targetDescriptor());
            // Add edge.
            AttributedGraph::Edge resultEdge = result.addEdge(sourceDesc, targetDesc);
            // Serialize edge attributes.
            resultEdge.attrib().json = edge.attrib();
        }
    }

    template <typename VA, typename EA, typename GA>
    AttributedGraph toAttributedGraph(const RelationGraph<VA, EA, GA>& input)
    {
        AttributedGraph result;
        toAttributedGraph(result, input);
        return result;
    }

    template <typename VA, typename EA, typename GA>
    AttributedGraph toAttributedGraph(const RelationGraph<VA, EA, GA>& graph, const ShapeMap& objects)
    {
        AttributedGraph result;
        toAttributedGraph(result, graph);
        result.serializeObjects(objects);
        return result;
    }


    template <typename VA, typename EA, typename GA>
    void fromAttributedGraph(const AttributedGraph& input, RelationGraph<VA, EA, GA>& result)
    {
        using GraphT = RelationGraph<VA, EA, GA>;

        if (auto typeName = input.getTypeName(); !typeName.empty())
        {
            if (typeName != serial::getTypeName(result))
            {
                throw error::TypeNameMismatch(typeName, serial::getTypeName(result));
            }
        }

        // Deserialize graph attributes.
        result.attrib() = input.attrib().json.get<GA>();

        std::map<AttributedGraph::VertexDescriptor,
                typename GraphT::VertexDescriptor> input2result;

        for (auto inputVertex : input.vertices())
        {
            typename GraphT::Vertex resultVertex = result.addVertex(
                        inputVertex.objectID(), inputVertex.attrib().json.get<VA>());

            input2result.emplace(inputVertex.descriptor(), resultVertex.descriptor());
        }

        for (auto edge : input.edges())
        {
            auto sourceDesc = input2result.at(edge.sourceDescriptor());
            auto targetDesc = input2result.at(edge.targetDescriptor());

            result.addEdge(sourceDesc, targetDesc, edge.attrib().json.get<EA>());
        }
    }

    template <class GraphT>
    GraphT fromAttributedGraph(const AttributedGraph& input)
    {
        GraphT result;
        fromAttributedGraph(input, result);
        return result;
    }


    template <class GraphT>
    void AttributedGraph::fromGraph(const GraphT& graph)
    {
        toAttributedGraph(*this, graph);
    }

    template <class GraphT>
    GraphT AttributedGraph::toGraph() const
    {
        return fromAttributedGraph<GraphT>(*this);
    }


    void to_json(nlohmann::json& j, const AttributedVertex& value);
    void from_json(const nlohmann::json& j, AttributedVertex& value);

    void to_json(nlohmann::json& j, const AttributedEdge& value);
    void from_json(const nlohmann::json& j, AttributedEdge& value);

    void to_json(nlohmann::json& j, const AttributedGraphAttributes& value);
    void from_json(const nlohmann::json& j, AttributedGraphAttributes& value);

    void to_json(nlohmann::json& j, const AttributedGraph& graph);
    void from_json(const nlohmann::json& j, AttributedGraph& graph);

}
