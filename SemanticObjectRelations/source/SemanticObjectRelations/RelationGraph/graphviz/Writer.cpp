#include "Writer.h"


namespace semrel
{

    graphviz::WriterMode graphviz::operator|(WriterMode lhs, WriterMode rhs)
    {
        return static_cast<WriterMode>(static_cast<int>(lhs) | static_cast<int>(rhs));
    }

    graphviz::WriterMode graphviz::operator&(WriterMode lhs, WriterMode rhs)
    {
        return static_cast<WriterMode>(static_cast<int>(lhs) & static_cast<int>(rhs));
    }

    bool graphviz::hasFlag(WriterMode mode, WriterMode flag)
    {
        return (mode & flag) == flag;
    }

}
