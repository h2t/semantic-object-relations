#pragma once

#include <filesystem>
#include <fstream>
#include <functional>
#include <ostream>

#include <SemanticObjectRelations/Shapes/Shape.h>
#include <SemanticObjectRelations/Shapes/shape_containers.h>

#include "formats.h"


namespace semrel::graphviz
{

    /// Indicates which files shall be generated. Can be evaluated as flags.
    enum class WriterMode
    {
        DOT = 1,     ///< Create only dot file.
        PNG = 2,     ///< Create only png file (remove dot file after creation).
        DOT_PNG = DOT | PNG, ///< Create both dot and png files.
    };

    // Operators for FormatMode: binary |, &; logical &&, !

    WriterMode operator|(WriterMode lhs, WriterMode rhs);
    WriterMode operator&(WriterMode lhs, WriterMode rhs);
    bool hasFlag(WriterMode mode, WriterMode flag);


    /**
     * @brief Class to write a graph visualization as GraphViz dot and/or png
     * format.
     *
     * Uses `boost::write_graphviz()` to write a given graph to a dot format
     * file. The `dot` system command is used to generate an image file from
     * the dot file. A mode specifies which files shall be created. If it
     * specifies that no dot file shall be created, the generated dot file
     * will be removed after creating the image file.
     * @see WriterMode
     *
     * Vertex, edge and graph formats can be used to customize the generated
     * dot file. A vertex, edge or graph format is a function which takes a
     * vertex, edge or graph, respectively, and returns its graphviz
     * attributes.
     * There are two kinds of vertex and edge formats: object-agnostic and
     * object-aware. Object-agnostic formats only take the vertex or edge as
     * arguments, while object-agnostic formats additionally take the
     * involved shapes.
     * The graphviz attributes for vertices and edges are a
     * collection of name-value-pairs (i.e. a map of string to string).
     * Graph attributes (i.e. `graphviz::GraphAttributes`) contain attributes
     * for the whole graph, as well as default attributes for vertices and
     * edges.
     * @see `graphviz::Attributes`
     * @see `graphviz::GraphAttributes`
     *
     * For example, to use a vertex attribute `name` as vertex label, and an
     * edge attribute `weight` as edge label, specify the following formats:
     *
     * @code
     * using namespace semrel;
     *
     * // Type definitions.
     * struct VertexAttrib : public ShapeVertex { std::string name; }
     * struct EdgeAttrib { int weight; }
     * using Graph = RelationGraph<VertexAttrib, EdgeAttrib>;
     *
     * // Make (object-agnostic) formats.
     * auto vertexFormat = [](Graph::ConstVertex v)
     * {
     *     return graphviz::Attributes{{ "label", v.attrib().name }};
     * };
     * auto edgeFormat = [](Graph::ConstEdge e)
     * {
     *     return graphviz::Attributes{{ "label", std::to_string(e.attrib().weight) }};
     * };
     *
     * // Build writer.
     * // There are some predefined constructors. If none of them match your
     * // needs, you can use the named argument paradigm:
     * graphviz::Writer<Graph> format = graphviz::Writer<Graph>()
     *                                          .setVertexFormat(vertexFormat)
     *                                          .setEdgeFormat(edgeFormat);
     *
     * // ...
     *
     * Graph graph;
     * // build graph ...
     *
     * writer.write(graph, "graph"); // write graph to "graph.dot" and "graph.png"
     * @endcode
     *
     * Using object-aware formats:
     *
     * @code
     * // Make object-aware formats.
     * auto vertexFormat = [](Graph::ConstVertex v, const Shape* object)
     * {
     *     return graphviz::Attributes{{ "label", object->tag() }};
     * };
     * auto edgeFormat = [](Graph::ConstEdge e, const Shape* sourceObject, const Shape* targetObject)
     * {
     *     return graphviz::Attributes{{ "label", sourceObject->tag() + " -> " + targetObject->tag() }};
     * };
     *
     * // Build writer.
     * graphviz::Writer<Graph> format = graphviz::Writer<Graph>()
     *                                          .setVertexShapeFormat(vertexShapeFormat)
     *                                          .setEdgeShapeFormat(edgeShapeFormat);
     *
     * ShapeMap objects = // ...;
     * // Write graph to "graph.dot" and "graph.png" using object-aware formats.
     * writer.write(graph, "graph", objects);
     * @endcode
     *
     *
     * `filename` can be
     * (1) a base name without extension (e.g. `"my_graph"`). In this
     *     case, the dot file will be called "my_graph.dot", and the image
     *     file will be called "my_graph.png".
     * (2) a full name with arbitrary extension, e.g. `"my_graph.png"`. In
     *     this case, the dot file will be named using the stem of `filename`
     *     (e.g. `"my_graph.dot"`). The image file will be named exactly
     *     `filename`, independent of its extension. Note that `dot` will
     *     always be called with the `-Tpng` option.
     *
     * @see https://www.boost.org/doc/libs/1_67_0/libs/graph/doc/write-graphviz.html
     * @see https://graphviz.gitlab.io/_pages/pdf/dotguide.pdf
     *
     * @param RelationGraphT The RelationGraph type.
     */
    template <class RelationGraphT>
    class Writer
    {

    public:
        using VertexDescriptor = typename RelationGraphT::VertexDescriptor;
        using EdgeDescriptor = typename RelationGraphT::EdgeDescriptor;
        using ConstVertex = typename RelationGraphT::ConstVertex;
        using ConstEdge = typename RelationGraphT::ConstEdge;

        using VertexFormat = graphviz::VertexFormat<RelationGraphT>;
        using VertexShapeFormat = graphviz::VertexShapeFormat<RelationGraphT>;
        using EdgeFormat = graphviz::EdgeFormat<RelationGraphT>;
        using EdgeShapeFormat = graphviz::EdgeShapeFormat<RelationGraphT>;
        using GraphFormat = graphviz::GraphFormat<RelationGraphT>;


    public:

        /// Construct with noop-formats and optional mode.
        Writer(WriterMode mode = WriterMode::DOT_PNG);

        /// Construct with given object-agnostic vertex, edge and graph formats and optional mode.
        Writer(VertexFormat vertexFormat, EdgeFormat edgeFormat, GraphFormat graphFormat,
               WriterMode mode = WriterMode::DOT_PNG);

        /// Construct with given object-aware vertex, edge and graph formats and optional mode.
        Writer(VertexShapeFormat vertexShapeFormat, EdgeShapeFormat edgeShapeFormat,
               GraphFormat graphFormat, WriterMode mode = WriterMode::DOT_PNG);

        /// Construct with given object-agnostic and object-aware vertex, edge and graph formats
        /// and optional mode.
        Writer(VertexFormat vertexFormat, VertexShapeFormat vertexShapeFormat,
               EdgeFormat edgeFormat, EdgeShapeFormat edgeShapeFormat,
               GraphFormat graphFormat, WriterMode mode = WriterMode::DOT_PNG);


        /**
         * @brief Write visualization file(s) of the given graph.
         * Uses object-agnostic formats.
         * @return true if successful, false otherwise (no exceptions are thrown).
         */
        bool write(const RelationGraphT& graph, const std::string& filename) const;
        /**
         * @brief Write visualization file(s) of the given graph.
         * Uses object-aware formats.
         * @return true if successful, false otherwise (no exceptions are thrown).
         */
        bool write(const RelationGraphT& graph, const ShapeMap& shapes, const std::string& filename) const;


        /// Get the vertex format.
        VertexFormat getVertexFormat() const { return vertexFormat; }
        /// Set the vertex format. (Can be chained with other setters.)
        Writer& setVertexFormat(VertexFormat vf) { this->vertexFormat = vf; return *this; }

        /// Get the vertex format.
        VertexShapeFormat getVertexShapeFormat() const { return vertexShapeFormat; }
        /// Set the vertex format. (Can be chained with other setters.)
        Writer& setVertexShapeFormat(VertexShapeFormat vsf) { this->vertexShapeFormat = vsf; return *this; }

        /// Get the edge format.
        EdgeFormat getEdgeFormat() const { return edgeFormat; }
        /// Set the edge format. (Can be chained with other setters.)
        Writer& setEdgeFormat(EdgeFormat ef) { this->edgeFormat = ef; return *this; }

        /// Get the edge format.
        EdgeShapeFormat getEdgeShapeFormat() const { return edgeShapeFormat; }
        /// Set the edge format. (Can be chained with other setters.)
        Writer& setEdgeShapeFormat(EdgeShapeFormat ef) { this->edgeShapeFormat = ef; return *this; }

        /// Get the graph format.
        GraphFormat getGraphFormat() const { return graphFormat; }
        /// Set the graph format. (Can be chained with other setters.)
        Writer& setGraphFormat(GraphFormat gf) { this->graphFormat = gf; return *this; }

        /// Get the mode.
        WriterMode getMode() const { return mode; }
        /// Set the mode. (Can be chained with other setters.)
        Writer& setMode(WriterMode mode) { this->mode = mode; return *this; }


    private:

        using path = std::filesystem::path;

        bool write(const path& filename, std::function<bool (const path&)> writeDotFn) const;
        bool writeDot(const path& dotFilename, const RelationGraphT& graph) const;
        bool writeDot(const path& dotFilename, const RelationGraphT& graph, const ShapeMap& shapes) const;
        bool writeImage(const path& imgFilename, const path& dotFilename) const;


    private:

        /// The object-agnostic vertex format. No-op by default.
        VertexFormat vertexFormat = [](ConstVertex) { return Attributes(); };
        /// The object-aware vertex format. No-op by default.
        VertexShapeFormat vertexShapeFormat = [](ConstVertex, const Shape*) { return Attributes(); };

        /// The object-agnostic edge format. No-op by default.
        EdgeFormat edgeFormat = [](ConstEdge) { return Attributes(); };
        /// The object-aware edge format. No-op by default.
        EdgeShapeFormat edgeShapeFormat = [](ConstEdge, const Shape*, const Shape*) { return Attributes(); };

        /// The graph format. No-op by default.
        GraphFormat graphFormat = [](const RelationGraphT&) { return GraphAttributes(); };


        /// The mode.
        WriterMode mode = WriterMode::DOT_PNG;

    };

}

