#include "exceptions.h"

namespace semrel::error
{

template <typename T>
std::string str(const T& t)
{
    return std::to_string(t);
}


RelationGraphError::RelationGraphError(const std::string& msg, const std::string& graphStr) :
    SemanticObjectRelationsError(msg + (graphStr.empty() ? "" : "\nIn graph: \n" + graphStr))
{}


NoVertexAtIndex::NoVertexAtIndex(std::size_t index, std::size_t numVertices) :
    RelationGraphError ("No vertex at index " + str(index)
                        + " (number of vertices: " + str(numVertices) + ").")
{}


NoVertexWithID::NoVertexWithID(ShapeID id, const std::string& graphStr) :
    RelationGraphError ("No vertex with object ID " + str(id) + " found.", graphStr)
{}


}
