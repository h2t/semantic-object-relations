#pragma once

#include <functional>

#include <boost/graph/adjacency_list.hpp>
#include <boost/iterator/transform_iterator.hpp>

#include <SemanticObjectRelations/Shapes/Shape.h>

#include "exceptions.h"


namespace semrel
{

namespace detail
{

/**
 * @brief The DescriptorTo function object transforms a boost graph descriptor into a proxy.
 *
 * We use this function object to transform e.g. `VertexDescriptor` to `Vertex`
 * and `EdgeDescriptor` to `Edge`. The descriptor transparently handles the
 * constness of the given proxy.
 */
template <typename ProxyT>
struct DescriptorTo : std::unary_function<typename ProxyT::Descriptor, ProxyT>
{
    using Graph = typename ProxyT::Graph;

    DescriptorTo(Graph* graph)
        : graph(graph)
    { }

    ProxyT operator ()(typename ProxyT::Descriptor descriptor) const
    {
        return ProxyT(graph, descriptor);
    }

    Graph* graph;
};


/**
 * @brief The Range class applies a transform to another range.
 *
 * We use this class to transform descriptor iterator ranges to
 * ranges over proxy objects like `Vertex` and `Edge`.
 */
template <class Iter, class Transform>
class Range
{
    using TransformedIter = boost::transform_iterator<Transform, Iter>;

public:

    /// Construct from begin and end iterators.
    Range(typename Transform::Graph* graph, Iter begin, Iter end)
        : begin_(begin, Transform(graph)), end_(end, Transform(graph))
    {}
    /// Construct from std::pair of begin and end iterators.
    Range(typename Transform::Graph* graph, std::pair<Iter, Iter> beginEnd)
        : Range(graph, beginEnd.first, beginEnd.second)
    {}

    TransformedIter begin() const { return begin_; }
    TransformedIter end() const { return end_; }

private:

    TransformedIter begin_;
    TransformedIter end_;

};

}


// forward declaration vertex types

template <typename RelationGraph>
struct ConstVertexT;

template <typename RelationGraph>
struct VertexT;


/**
 * @brief A graph edge (const).
 *
 * Provides access to source and target vertex as well as
 * the edge attributes.
 */
template <typename RelationGraph>
struct ConstEdgeT
{
    using Graph = const RelationGraph;
    using Descriptor = typename Graph::EdgeDescriptor;


    /// Construct from graph and descriptor.
    explicit ConstEdgeT(Graph* graph = nullptr, Descriptor descriptor = {})
        : _descriptor(descriptor), graph(const_cast<RelationGraph*>(graph))
    { }

    /// Get the source vertex.
    ConstVertexT<RelationGraph> source() const;
    /// Get the target vertex.
    ConstVertexT<RelationGraph> target() const;

    /// Get the descriptor of the source vertex.
    typename Graph::VertexDescriptor sourceDescriptor() const { return boost::source(_descriptor, *graph); }
    /// Get the descriptor of the target vertex.
    typename Graph::VertexDescriptor targetDescriptor() const { return boost::target(_descriptor, *graph); }

    /// Get the edge attributes.
    typename Graph::EdgeAttrib const& attrib() const { return (*this->graph)[this->_descriptor]; }

    /// Get the object ID of the source vertex.
    ShapeID sourceObjectID() const { return source().objectID(); }
    /// Get the object ID of the target vertex.
    ShapeID targetObjectID() const { return target().objectID(); }

    /// Get the edge descriptor.
    Descriptor descriptor() const { return _descriptor; }

    /// Equality operator (defined in terms of descriptor equality).
    bool operator ==(const ConstEdgeT<RelationGraph>& e) const { return this->_descriptor== e._descriptor; }
    /// Inequality operator (inverse of equality).
    bool operator !=(const ConstEdgeT<RelationGraph>& e) const { return !(*this == e); }


protected:

    Descriptor _descriptor; ///< The edge descriptor.
    RelationGraph* graph;  ///< The graph.

};


/**
 * @brief A graph edge (non-const).
 * @see ConstEdgeT
 */
template <typename RelationGraph>
struct EdgeT : ConstEdgeT<RelationGraph>
{
    using Graph = RelationGraph;
    using ConstEdge = ConstEdgeT<RelationGraph>;
    using Descriptor = typename ConstEdge::Descriptor;


    /// Construct from graph and descriptor.
    explicit EdgeT(Graph* graph = nullptr, Descriptor descriptor = {})
        : ConstEdge(graph, descriptor)
    { }


    // Bring const overloads into scope.

    using ConstEdge::source;
    using ConstEdge::target;
    using ConstEdge::attrib;
    using ConstEdge::sourceObjectID;
    using ConstEdge::targetObjectID;


    // Add non-const overloads.

    VertexT<RelationGraph> source();
    VertexT<RelationGraph> target();

    ShapeID& sourceObjectID() { return source().objectID(); }
    ShapeID& targetObjectID() { return target().objectID(); }

    typename RelationGraph::EdgeAttrib& attrib() { return (*this->graph)[this->_descriptor]; }
};


/**
 * @brief A graph vertex (const).
 *
 * Provides access to in- and out-edges as well as vertex attributes.
 */
template <typename RelationGraph>
struct ConstVertexT
{
    using Graph = const RelationGraph;
    using Descriptor = typename Graph::VertexDescriptor;
    using ConstInEdgeRange = detail::Range<typename Graph::Traits::in_edge_iterator, detail::DescriptorTo<ConstEdgeT<RelationGraph>>>;
    using ConstOutEdgeRange = detail::Range<typename Graph::Traits::out_edge_iterator, detail::DescriptorTo<ConstEdgeT<RelationGraph>>>;


    /// Construct from graph and descriptor.
    explicit ConstVertexT(Graph* graph = nullptr, Descriptor descriptor = {})
        : _descriptor(descriptor), graph(const_cast<RelationGraph*>(graph))
    { }

    /// Get the vertex attributes.
    const typename Graph::VertexAttrib& attrib() const { return (*graph)[_descriptor]; }

    /// Get the vertex object ID.
    ShapeID objectID() const { return attrib().objectID; }

    /// Get a range over the in-edges.
    ConstInEdgeRange inEdges() const { return ConstInEdgeRange(graph, boost::in_edges(_descriptor, *graph)); }
    /// Get a range over the the out-edges.
    ConstOutEdgeRange outEdges() const { return ConstOutEdgeRange(graph, boost::out_edges(_descriptor, *graph)); }

    /// Get the number of in-edges.
    std::size_t inDegree() const { return boost::in_degree(_descriptor, *graph); }
    /// Get the number of out-edges.
    std::size_t outDegree() const { return boost::out_degree(_descriptor, *graph); }

    /// Return the vertex descriptor.
    Descriptor descriptor() const { return _descriptor; }

    /// Equality operator (defined in terms of descriptor equality).
    bool operator ==(const ConstVertexT<RelationGraph>& v) const { return this->_descriptor== v._descriptor; }
    /// Inequality operator (inverse of equality operator).
    bool operator !=(const ConstVertexT<RelationGraph>& v) const { return !(*this == v); }


protected:

    Descriptor _descriptor; ///< The vertex descriptor.
    RelationGraph* graph;   ///< The graph.

};


/**
 * @brief A graph vertex (non-const).
 * @see ConstVertexT
 */
template <typename RelationGraph>
struct VertexT : public ConstVertexT<RelationGraph>
{
    using Graph = RelationGraph;
    using ConstVertex = ConstVertexT<RelationGraph>;
    using Descriptor = typename ConstVertex::Descriptor;
    using InEdgeRange = detail::Range<typename Graph::Traits::in_edge_iterator, detail::DescriptorTo<EdgeT<RelationGraph>>>;
    using OutEdgeRange = detail::Range<typename Graph::Traits::out_edge_iterator, detail::DescriptorTo<EdgeT<RelationGraph>>>;

    /// Construct from graph and descriptor.
    explicit VertexT(Graph* graph = nullptr, Descriptor descriptor = {})
        : ConstVertex(graph, descriptor)
    { }

    // Bring const overloads into scope.

    using ConstVertex::attrib;
    using ConstVertex::objectID;
    using ConstVertex::inEdges;
    using ConstVertex::outEdges;

    // Add non-const overloads.

    typename Graph::VertexAttrib& attrib() { return (*this->graph)[this->_descriptor]; }

    /// @see ConstVertexT::objectID()
    ShapeID& objectID() { return attrib().objectID; }

    InEdgeRange inEdges() { return InEdgeRange(this->graph, boost::in_edges(this->_descriptor, *this->graph)); }
    OutEdgeRange outEdges() { return OutEdgeRange(this->graph, boost::out_edges(this->_descriptor, *this->graph)); }
};


// These methods cannot be implemented inline because of
// a cyclic dependency between Vertex and Edge

template <typename RelationGraph>
ConstVertexT<RelationGraph> ConstEdgeT<RelationGraph>::source() const
{
    return this->graph->vertex(this->sourceDescriptor());
}

template <typename RelationGraph>
ConstVertexT<RelationGraph> ConstEdgeT<RelationGraph>::target() const
{
    return this->graph->vertex(this->targetDescriptor());
}

template <typename RelationGraph>
VertexT<RelationGraph> EdgeT<RelationGraph>::source()
{
    return this->graph->vertex(this->sourceDescriptor());
}

template <typename RelationGraph>
VertexT<RelationGraph> EdgeT<RelationGraph>::target()
{
    return this->graph->vertex(this->targetDescriptor());
}

template <typename RelationGraph>
std::ostream& operator <<(std::ostream& os, const ConstVertexT<RelationGraph>& v)
{
    return os << v.descriptor();
}

template <typename RelationGraph>
std::ostream& operator <<(std::ostream& os, const ConstEdgeT<RelationGraph>& e)
{
    return os << e.descriptor();
}

}
