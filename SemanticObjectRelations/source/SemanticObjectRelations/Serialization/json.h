#pragma once

#include <functional>

#include <SimoxUtility/json.h>

#include "exceptions.h"


namespace semrel::serial
{

    /**
     * @brief Get the type name stored in `j`.
     * @param key The key of the type name entry.
     * @throw `error::NoTypeNameEntryInJsonObject`
     *      If `j` is not an object or does not contain `key`.
     */
    std::string getTypeName(const nlohmann::json& j, const std::string& key);


    /**
     * @brief Store the type name in `j`.
     * @param key The key where the type name shall be stored.
     * @param typeName The type name.
     *
     * @throws `error::TypeNameEntryAlreadyInJsonObject`
     *      If `j` already contains `key`.
     */
    void setTypeName(nlohmann::json& j, const std::string& key, const std::string& typeName);


    /**
     * A raw function pointer to a function with signature:
     * @code
     * void to_json(nlohmann::json& j, const Value& v);
     * @endcode
     */
    template <class ValueT>
    using RawToJsonFn = void (*)(nlohmann::json& j, const ValueT& v);
    /**
     * A raw function pointer to a function with signature:
     * @code
     * void from_json(const nlohmann::json& j, Value& v);
     * @endcode
     */
    template <class ValueT>
    using RawFromJsonFn = void (*)(const nlohmann::json& j, ValueT& v);


    /**
     * A `std::function` pointer to a function with signature:
     * @code
     * void to_json(nlohmann::json& j, const Value& v);
     * @endcode
     */
    template <class ValueT>
    using ToJsonFn = std::function<void(nlohmann::json& j, const ValueT& v)>;
    /**
     * A `std::function` pointer to a function with signature:
     * @code
     * void from_json(const nlohmann::json& j, Value& v);
     * @endcode
     */
    template <class ValueT>
    using FromJsonFn = std::function<void(const nlohmann::json& j, ValueT& v)>;

}

