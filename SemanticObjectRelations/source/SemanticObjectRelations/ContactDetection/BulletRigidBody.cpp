#include "BulletRigidBody.h"

#include <SemanticObjectRelations/Shapes/Shape.h>

namespace semrel
{

static btTransform eigenToBullet(const Eigen::Quaternionf& q, const Eigen::Vector3f& v)
{
    return btTransform(btQuaternion(q.x(), q.y(), q.z(), q.w()), btVector3(v(0), v(1), v(2)));
}

BulletRigidBody::BulletRigidBody(const Shape& shape, float margin)
{
    // get bullet collision shape depending on shape type
    bulletShape = shape.getBulletCollisionShape(margin);
    
    bulletMotionState.reset(new btDefaultMotionState(
                eigenToBullet(shape.getOrientation(), shape.getPosition())));
    
    btScalar mass = 1;
    btVector3 inertia(0, 0, 0);
    bulletShape->calculateLocalInertia(mass, inertia);

    btRigidBody::btRigidBodyConstructionInfo ci(
                mass, bulletMotionState.get(), bulletShape.get(), inertia);
    bulletRigidBody.reset(new btRigidBody(ci));
    this->shape = &shape;
}

}
