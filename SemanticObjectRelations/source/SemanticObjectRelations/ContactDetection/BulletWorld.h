#pragma once

#include <btBulletDynamicsCommon.h>
#include <memory>


namespace semrel
{

/**
 * @brief Wrapper for bullet's btDiscreteDynamicsWorld that takes care of allocating
 * and keeping the necessary members.
 */
class BulletWorld
{

public:

    /**
     * @brief Create a new bullet world.
     */
    BulletWorld();

    const btDbvtBroadphase& getPairCache() const { return *pairCache; }
    const btDefaultCollisionConfiguration& getCollisionConfiguration() const { return *collisionConfiguration; }
    const btCollisionDispatcher& getDispatcher() const { return *dispatcher; }
    const btSequentialImpulseConstraintSolver& getSolver() const { return *solver; }
    const btDiscreteDynamicsWorld& getWorld() const { return *world; }

    /// Access the world's members.
    btDiscreteDynamicsWorld* operator->() { return world.get(); }
    /// Access the world's members (const).
    const btDiscreteDynamicsWorld* operator->() const { return world.get(); }

private:

    // Allocation should happen in this order.
    std::unique_ptr<btDbvtBroadphase> pairCache;
    std::unique_ptr<btDefaultCollisionConfiguration> collisionConfiguration;
    std::unique_ptr<btCollisionDispatcher> dispatcher;
    std::unique_ptr<btSequentialImpulseConstraintSolver> solver;

    std::unique_ptr<btDiscreteDynamicsWorld> world;

};


}


