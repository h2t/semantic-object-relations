#include "ContactDetection.h"

#include <SemanticObjectRelations/Hooks/Log.h>
#include <SemanticObjectRelations/Visualization/ContactDetectionVisualizer.h>

#include "BulletWorld.h"
#include "BulletRigidBody.h"


namespace semrel
{

namespace
{

static ContactDetectionVisualizer& visu = ContactDetectionVisualizer::get();


Eigen::Vector3f bullet2Eigen(const btVector3& v)
{
    return Eigen::Matrix<btScalar, 3, 1>(v.x(), v.y(), v.z()).cast<float>();
}

struct ContactTestCallback : public btCollisionWorld::ContactResultCallback
{
public:

    ContactTestCallback(ContactPointList& contactList) :
        btCollisionWorld::ContactResultCallback(), contactList(contactList)
    { }


    const Shape* shapeA = nullptr;
    const Shape* shapeB = nullptr;
    ContactPointList& contactList;

    void setObjects(const Shape* A, const Shape* B)
    {
        this->shapeA = A;
        this->shapeB = B;
    }

    virtual btScalar addSingleResult(
            btManifoldPoint& pt,
            const btCollisionObjectWrapper*, int, int,
            const btCollisionObjectWrapper*, int, int) override
    {
        SR_LOG_DEBUG << "+-- Contact Test Callback (" << shapeA->getID() << "--" << shapeB->getID() << ") --";

        if (pt.getDistance() <= 0)
        {
            SR_LOG_DEBUG << "| Contact:";

            Eigen::Vector3f posA = bullet2Eigen(pt.getPositionWorldOnA());
            Eigen::Vector3f posB = bullet2Eigen(pt.getPositionWorldOnB());
            const btVector3& normalB = pt.m_normalWorldOnB;
            //Eigen::Vector3f normalB = (posA - posB).normalized();

            Eigen::Vector3f pos = 0.5 * (posA + posB);
            SR_LOG_DEBUG << "| position: " << pos(0) << " " << pos(1) << " " << pos(2);
            SR_LOG_DEBUG << "| (posA:    " << posA.x() << " " << posA.y() << " " << posA.z() << ")";
            SR_LOG_DEBUG << "| (posB:    " << posB.x() << " " << posB.y() << " " << posB.z() << ")";
            SR_LOG_DEBUG << "| normalB:  " << normalB.x() << " " << normalB.y() << " " << normalB.z();

            contactList.emplace_back(shapeA, shapeB, pos, bullet2Eigen(normalB));
        }
        else
        {
            SR_LOG_DEBUG << "| No contact";
        }
        return 0;
    }
};

ContactPointList runContactTests(std::vector<BulletRigidBody>& rigidBodies, BulletWorld& world)
{
    ContactPointList contactList;
    ContactTestCallback callback(contactList);

    // run mutual contact tests
    for (std::size_t i = 0; i < rigidBodies.size(); ++i)
    {
        for (std::size_t j = i+1; j < rigidBodies.size(); ++j)
        {
            BulletRigidBody& bodyA = rigidBodies[j];
            BulletRigidBody& bodyB = rigidBodies[i];

            SR_LOG_DEBUG << "=== TESTING (A) " << bodyA.getShape()->tag()
                         << " <-> " << bodyB.getShape()->tag() << " (B) ===";
            callback.setObjects(bodyA.getShape(), bodyB.getShape());
            world->contactPairTest(bodyA.get(), bodyB.get(), callback);
        }
    }

    SR_LOG_DEBUG << "== Contact list: ==";
    for (auto cp : contactList)
    {
        SR_LOG_DEBUG << "| " << cp;
    }

    return contactList;
}

/// Fill the rigid body container.
std::vector<BulletRigidBody> buildRigidBodies(const ShapeMap &objects, float margin)
{
    std::vector<BulletRigidBody> rigidBodies;
    rigidBodies.reserve(objects.size());

    for (const auto& [id, object] : objects)
    {
        SR_LOG_VERBOSE << "... " << object->str();

        rigidBodies.emplace_back(*object, margin);
        visu.drawShapeMargin(*object, margin);
    }

    return rigidBodies;
}

}

ContactGraph buildContactGraph(ContactPointList& contactPoints, const ShapeMap& objects)
{
    SR_LOG_DEBUG << "Making graph for objects " << objects << " (" << objects.size() << " vertices)";
    ContactGraph graph(objects);

    SR_LOG_DEBUG << "Initial " << graph;

    SR_LOG_DEBUG << "=== Adding edges... ===";

    for (ContactPoint& cp : contactPoints)
    {
        SR_LOG_DEBUG << "+-- " << cp;

        const Shape* objA = cp.getShapeA();
        const Shape* objB = cp.getShapeB();

        ContactGraph::Vertex vertexA = graph.vertex(objA->getID());
        ContactGraph::Vertex vertexB = graph.vertex(objB->getID());

        ContactGraph::Edge edge;

        if (!graph.hasEdge(vertexA, vertexB)) // if edge does not exist
        {
            SR_LOG_DEBUG << "| Adding edge (" << objA->tag() << ", " << objB->tag() << ")";
            edge = graph.addEdge(vertexA, vertexB);
        }
        else
        {
            edge = graph.edge(vertexA, vertexB);
        }
        edge.attrib().contactPoints.push_back(cp);

        SR_LOG_DEBUG << "| Edge now contains " << edge.attrib().contactPoints.size() << " contact points";
    }

    SR_LOG_DEBUG << graph;

    return graph;
}

ContactGraph buildContactGraph(ContactPointList& contactPoints, const ShapeList& objects)
{
    return buildContactGraph(contactPoints, toShapeMap(objects));
}

ContactDetection::ContactDetection(float margin) : margin(margin)
{
}

ContactGraph ContactDetection::computeContacts(const ShapeMap& objects) const
{
    visu.clearLayer();

    SR_LOG_VERBOSE << "Finding contacts of " << objects;

    SR_LOG_VERBOSE << "Preparing world and rigid bodies";
    BulletWorld world;

    auto rigidBodies = buildRigidBodies(objects, margin);

    SR_LOG_VERBOSE << "Running contact tests...";
    ContactPointList contactPoints = runContactTests(rigidBodies, world);

    visu.drawContactPoints(contactPoints);

    SR_LOG_VERBOSE << "Building contact graph ...";
    ContactGraph graph = buildContactGraph(contactPoints, objects);

    SR_LOG_VERBOSE << "Contact graph: " << graph.str();

    return graph;
}

ContactGraph ContactDetection::computeContacts(const ShapeList& objects) const
{
    return computeContacts(toShapeMap(objects));
}


void ContactDetection::setMargin(float value)
{
    this->margin = value;
}

float ContactDetection::getMargin() const
{
    return margin;
}

}

