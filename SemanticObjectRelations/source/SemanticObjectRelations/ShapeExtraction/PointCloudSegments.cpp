#include "PointCloudSegments.h"

using namespace semrel;

PointCloudSegments::PointCloudSegments()
{
}

PointCloudSegments::PointCloudSegments(const pcl::PointCloud<PointCloudSegments::PointT>& pointCloud)
{
    addPointCloud(pointCloud);
}


void PointCloudSegments::clear()
{
    segmentMap.clear();
}

void PointCloudSegments::addPointCloud(
        const pcl::PointCloud<PointCloudSegments::PointT>& pointCloud)
{
    for (const PointT& point : pointCloud.points)
    {
        // look up whether an entry for point's label exists
        auto mapEntry = segmentMap.find(point.label);
        
        pcl::PointCloud<PointT>::Ptr segment;
        
        if (mapEntry == segmentMap.end())
        {
            // label not mapped => add a new segment
            segment = addEmptySegment(point.label);
        }
        else
        {
            // label mapped
            segment = mapEntry->second;
        }
        
        segment->push_back(point);
    }
}

pcl::PointCloud<PointCloudSegments::PointT>::Ptr PointCloudSegments::addEmptySegment(
        uint32_t label)
{
    pcl::PointCloud<PointT>::Ptr newSegment(new pcl::PointCloud<PointT>());
    segmentMap[label] = newSegment;
    return newSegment;
}

void PointCloudSegments::removeSegment(uint32_t label)
{
    segmentMap.erase(label);
}


std::size_t PointCloudSegments::numSegments() const
{
    return segmentMap.size();
}

std::size_t PointCloudSegments::numPoints() const
{
    std::size_t size = 0;
    for (auto& segmentEntry : segmentMap)
    {
        size += segmentEntry.second->size();
    }
    return size;
}

std::vector<uint32_t> PointCloudSegments::getLabels() const
{
    std::vector<uint32_t> labels;
    labels.reserve(segmentMap.size());
    for (auto& segmentEntry : segmentMap)
    {
        labels.push_back(segmentEntry.first);
    }
    return labels;
}

pcl::PointCloud<PointCloudSegments::PointT>::Ptr const& PointCloudSegments::getSegment(uint32_t label) const
{
    return segmentMap.at(label);
}

std::vector<pcl::PointCloud<PointCloudSegments::PointT>::Ptr> PointCloudSegments::getSegments() const
{
    std::vector<pcl::PointCloud<PointT>::Ptr> segments;
    segments.reserve(segmentMap.size());
    for (auto& segmentEntry : segmentMap)
    {
        segments.push_back(segmentEntry.second);
    }
    return segments;
}

const PointCloudSegments::SegmentMap& PointCloudSegments::getSegmentMap() const
{
    return segmentMap;
}

pcl::PointCloud<PointCloudSegments::PointT>::Ptr PointCloudSegments::merged() const
{
    pcl::PointCloud<PointT>::Ptr wholePointCloud(new pcl::PointCloud<PointT>());
    for (auto& segmentEntry : segmentMap)
    {
        (*wholePointCloud) += *segmentEntry.second;
    }
    return wholePointCloud;
}

PointCloudSegments::SegmentMap::const_iterator PointCloudSegments::begin() const 
{ 
    return segmentMap.begin(); 
}

PointCloudSegments::SegmentMap::const_iterator PointCloudSegments::end() const 
{ 
    return segmentMap.end(); 
}

