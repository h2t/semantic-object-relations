#pragma once

#include <SemanticObjectRelations/Shapes.h>

#include "PointCloudSegments.h"

#include "ransac/BoxRansac.h"
#include "ransac/CylinderRansac.h"
#include "ransac/SphereRansac.h"


namespace semrel
{

/// Result of shape extraction from a labeled point cloud (multiple segments).
struct ShapeExtractionResult
{
    /// Map from labels to extraction properties.
    std::map<uint32_t, RansacResultInfo> extractionInfo;

    /// Returns a ShapeSet containing all found shapes.
    /// Each shape's ID is set to the respective segment label.
    ShapeList objects;
};


/**
 * @brief Extraction of primitive shapes from a labeled point cloud.
 *
 * Shape extraction attempts to fit primitive shapes (i.e. boxes/cuboids,
 * cylinders and spheres) to segments in a labeled point cloud.
 * Extraction is done by `extract()`. The input is a labeled point cloud,
 * where each segment (i.e. points with the same label) should represent one
 * object. Shape extraction will attempt to fit one model of each shape type
 * to each segment, and return the best fitting model of each segment.
 *
 * The fitting error is computed as mean squared error, i.e. the sum of
 * squared distances of all segment points to the model, divided by the
 * number of points in the segment. Distance is measured to the model
 * surface - points inside shapes yield a positive error value (just as
 * points on the outside).
 *
 * The result is a `ShapeList` with one entry per found shape. The shapes'
 * IDs are set to the label of the corresponding segment. Additional
 * information per segment (e.g. fitting error) is provided as a result
 * as well.
 *
 * ## Parameters
 * - Maximal model error (default: 10000)
 *   A model is discarded if its error is above this threshold.
 *
 * ### From RansacBase:
 * - Maximal number of iterations (default: 100)
 *   How many model candidates shall be fitted to the point cloud.
 *
 * - Distance threshold (Box RANSAC, PCL RANSAC) (default: 1.0)
 *   The maximal distance distance of a point to the model to be
 *   considered an inlier.
 *
 * - Size penalty factor and exponent (default: 0.000125 * size ^ 2.0)
 *   Size penalty attempts to avoid very large models trivially fiting
 *   the point cloud (e.g. a huge sphere fitting a plane). Given a size
 *   measure size (e.g. the radius of a sphere), the size penalty is
 *   penalty = factor * size ^ exponent.
 *
 * - Outlier rate (must be in [0, 0.5], default: 0.0)
 *   Allows a proportion of points to be considered as "outliers" and
 *   neglected for fitting and error calculation. This helps to cope
 *   with unclean segmentations. (@see SoftMinMax)
 *   In addition, outliers are also neglected when estimating the
 *   extents of a box along its axes.
 *
 * ### From BoxRansac:
 * - Minimal inlier rate: (default: 0.75)
 *   A candidate model is discarded if the proportion of inliers to the
 *   number of points in the point cloud is less than this value.
 *
 * - Outlier rate (see above)
 *
 */
class ShapeExtraction
{

public:

    /// Constructor with default parameters.
    ShapeExtraction(float maxModelError = 10000);


    // COMPUTE

    /// Extract shapes from the given point cloud.
    /// If concurrency is not disabled, one thread per segment is started.
    ShapeExtractionResult extract(const pcl::PointCloud<pcl::PointXYZRGBL>& labeledPointCloud);


    // PARAMETERS

    void useRandomSeed(bool useRandomSeed);

    /// Set the maximal model error.
    void setMaxModelError(float maxError);

    /// Set the maximal number of iterations.
    /// @see RansacBase
    void setMaxIterations(int iterations);

    /// Set the distance threshold (box, cylinder and sphere).
    /// @see RansacBase
    void setDistanceThreshold(float distanceThreshold);
    /// Set the distance threshold for box RANSAC.
    /// @see BoxRansac
    void setDistanceThresholdBox(float distanceThreshold);
    /// Set the distance threshold for PCL based RANSAC.
    /// @see CylinderRansac, @see SphereRansac.
    void setDistanceThresholdPCL(float distanceThreshold);

    /// Set the outlier rate.
    /// @see RansacBase
    void setOutlierRate(float outlierRate);

    /// Set the minimal inlier rate. (Box RANSAC)
    void setMinInlierRate(float minInlierRate);

    /// Set the size penalty factor and exponent.
    /// @see RansacBase
    void setSizePenalty(float factor, float exponent);
    void setSizePenaltyFactor(float factor);
    void setSizePenaltyExponent(float exponent);


    /// Enable/disable box extraction.
    void setBoxExtractionEnabled(bool enabled);
    /// Enable/disable box extraction.
    void setCylinderExtractionEnabled(bool enabled);
    /// Enable/disable box extraction.
    void setSphereExtractionEnabled(bool enabled);

    /// Whether to extract each segment in a separate thread or not.
    void setConcurrencyEnabled(bool enabled);


private:

    /// Whether or not box shall be extracted.
    bool enabledBoxExtraction = true;
    /// Whether or not cylinder shall be extracted.
    bool enabledCylinderExtraction = true;
    /// Whether or not spheres shall be extracted.
    bool enabledSphereExtraction = true;

    /// Whether to extract in multiple threads.
    bool enabledConcurrency = true;


    // PROTOTYPES (get copied for parallel usage)
    // this is useful to store the respective parameters

    /// The box ransac.
    BoxRansac protoBoxRansac;
    /// The cylinder ransac.
    CylinderRansac protoCylinderRansac;
    /// The sphere ransac.
    SphereRansac protoSphereRansac;

    /// Vector of all ransacs. Useful for actions on all shape ransacs.
    std::vector<std::reference_wrapper<RansacBase>> allRansacs =
    {
        protoBoxRansac, protoCylinderRansac, protoSphereRansac
    };


    // PARAMETERS

    /// Discard a model if its error is above this threshold.
    float maxModelError;

};

}
