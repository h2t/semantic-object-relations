#pragma once

#include <SimoxUtility/math/SoftMinMax.h>


namespace semrel
{

    /**
     * @brief The SoftMinMax class can be used to find soft min and max of a set of floats.
     * Soft means that some values are allowed to be >= soft min / <= soft max
     * (including the soft min/max), respectively.
     * A percentile argument in [0..0.5] specifies the percentage of values
     * which are allowed to excess the soft min/max.
     *
     * This class has been moved to `SimoxUtility`. This definition provides legacy
     * aliases for renamed functions.
     * @see `simox::math::SoftMinMax`.
     */
    class SoftMinMax : public simox::math::SoftMinMax
    {
    public:

        using simox::math::SoftMinMax::SoftMinMax;

        /// No initialization constructor.
        SoftMinMax();

        /// Get the current soft min.
        /// @throws std::out_of_range if no element was added
        float getSoftMin() const
        {
            return simox::math::SoftMinMax::getSoftMin();
        }
        /// Get the current soft max.
        /// @throws std::out_of_range if no element was added
        float getSoftMax() const
        {
            return simox::math::SoftMinMax::getSoftMax();
        }
    };

}
