#include "BoxRansac.h"

#include <set>

#include <SemanticObjectRelations/Hooks/Log.h>
#include <SemanticObjectRelations/Visualization/BoxRansacVisualizer.h>

#include <SemanticObjectRelations/ShapeExtraction/ShapeExtraction.h>


namespace semrel
{

static BoxRansacVisualizer& lifeVisu = BoxRansacVisualizer::get();


BoxRansac::BoxRansac(int maxIterations, float distanceThreshold, 
                     float minInlierRate, float outlierRate) :
    RansacBase(maxIterations, distanceThreshold, outlierRate), 
    boxFitter(outlierRate), minInlierRate(minInlierRate)
{
    errorFunction = BoxErrorFunction;
}

RansacResult BoxRansac::fitShape(const pcl::PointCloud<PointL>& pointCloud)
{
    SR_LOG_VERBOSE << "== STARTING BOX RANSAC ==";
    
    setPointCloud(pointCloud);

    // check point cloud size
    int mssSize = boxFitter.getMssSize();
    if (mssSize < 0 || points.size() < unsigned(mssSize))
    {
        return RansacResult();
    }

    
    goodModels = 0;
    trivialFails = 0;

    lifeVisu.drawPointCloudLife(points);

    
    std::optional<Box> bestModel;
    Evaluation bestEval = {std::numeric_limits<float>::max(), 0};
    
    int numSuccessiveTrivialFails = 0;
    const int maxSuccessiveTrivialFails = 3;

    for (int i = 0; i < maxIterations; ++i)
    {
        lifeVisu.clearLayer();

        lifeVisu.drawPointCloudLife(points);

        std::vector<Eigen::Vector3f> mss = selectMSS();
        lifeVisu.drawMSS(mss);
        lifeVisu.shortSleep();

        bool trivialFail = false;
        std::optional<Box> candidate = runIteration(mss, i, trivialFail);

        // only count trivial fails if they happened n times in a row
        if (trivialFail)
        {
            ++numSuccessiveTrivialFails;
            if (numSuccessiveTrivialFails < maxSuccessiveTrivialFails)
            {
                // don't count this iteration
                --i;
            }
            else
            {
                // count this iteration, reset counter
                numSuccessiveTrivialFails = 0;
            }
            continue;
        }
        
        // non-trivial fail
        if (candidate)
        {
            // compute error
            Evaluation eval = evaluate(*candidate);

            SR_LOG_VERBOSE << "[" << i << "] Found model with error: " << eval.error
                        << " (current best: " << bestEval.error << ")";

            if (eval.error < bestEval.error)
            {
                bestModel = candidate;
                bestEval.error = eval.error;
            }
        }
    }
        
    SR_LOG_VERBOSE << "== BOX RANSAC COMPLETE "
                << "(error: " << bestEval.error
                << " | good models: " << goodModels
                << " | trivial fails: " << trivialFails
                << ") ==";
    
    RansacResult result;
    if (bestModel)
    {
        result.shape.reset(new Box(*bestModel));
        result.info = {bestEval.error, bestEval.coverage};
    }
    return result;
}

void BoxRansac::setUseRandomSeed(bool useRandomSeed)
{
    // Use same logic as PCL-based RANSAC methods
    if (useRandomSeed)
    {
        randomEngine.seed(static_cast<unsigned> (std::time(nullptr)));
    }
    else
    {
        randomEngine.seed(12345u);
    }
}

void BoxRansac::setMinInlierRate(float minInlierRate)
{
    this->minInlierRate = minInlierRate;
}

std::optional<Box> BoxRansac::runIteration(std::vector<Eigen::Vector3f> mss, int iteration, bool& trivialFail)
{
    std::optional<Box> result;

    int it = iteration + 1;
    
    SR_LOG_DEBUG << "[" << it << "] -- Iteration #" << it << " --";

    SR_LOG_DEBUG << "[" << it << "] Computing axes ...";
    // modelCandidate will be of a specific subtype of Shape,
    // as determined by the fitter
    Eigen::Matrix3f boxAxes = boxFitter.fitBoxAxesToMSS(mss);

    if (boxAxes.isZero(1e-6f))
    {
        // bad MSS
        trivialFail = true;
        return result;
    }

    lifeVisu.drawAxes(boxAxes, mss[0]);
    lifeVisu.shortSleep();


    SR_LOG_DEBUG << "[" << it << "] Computing visible planes ...";

    std::vector<Hyperplane3f> visiblePlanes = boxFitter.computeVisiblePlanes(boxAxes, points);

    lifeVisu.drawVisiblePlanes(visiblePlanes, mss[0]);
    lifeVisu.shortSleep();


    SR_LOG_DEBUG << "[" << it << "] Computing inliers per plane ...";

    // compute inliers for each plane
    std::vector<Eigen::Vector3f> planeInliers[3];
    for (unsigned int i = 0; i < 3; ++i)
    {
        Hyperplane3f& plane = visiblePlanes[i];
        std::vector<Eigen::Vector3f>& inliers = planeInliers[i];

        inliers.reserve(points.size());
        for (Eigen::Vector3f& p : points)
        {
            if (plane.absDistance(p) < distanceThreshold)
            {
                inliers.push_back(p);
            }
        }
    }

    lifeVisu.drawVisiblePlaneInliers(planeInliers);


    int numInliers = 0;
    int numZeroInliers = 0; // how many inliers contain zero points?
    for (auto& inliers : planeInliers)
    {
        numInliers += inliers.size();
        numZeroInliers += inliers.size() == 0; // 1 if 0, else 0 
    }

    // discard if too few inliers or two planes have 0 inliers
    if (numInliers < minInlierRate * points.size() || numZeroInliers >= 2)
    {
        SR_LOG_DEBUG << "Too few inliers (" << numInliers << " < " << minInlierRate * points.size() << ")";
        trivialFails++;
        lifeVisu.shortSleep();
        trivialFail = true;
        return result;
    }

    // good planes found
    trivialFail = false;

    goodModels++;

    lifeVisu.longSleep();

    SR_LOG_DEBUG << "[" << it << "] Found good planes. Computing box ...";

    result = boxFitter.computeBox(visiblePlanes, planeInliers);

    lifeVisu.drawBoxCandidate(*result);
    lifeVisu.shortSleep();

    lifeVisu.clearLayer();
    lifeVisu.shortSleep();

    lifeVisu.drawPointCloudLife(points);
    lifeVisu.drawBoxCandidate(*result);
    lifeVisu.longSleep();

    return result;
}

std::string BoxRansac::shapeName() const
{
    return "Box";
}

float BoxRansac::getSizeMeasure(const Shape& shape) const
{
    return static_cast<const Box&>(shape).getExtents().maxCoeff();
}

std::vector<Eigen::Vector3f> BoxRansac::selectMSS()
{
    int mssSize = boxFitter.getMssSize();
    std::set<int> indices;
    
    std::uniform_int_distribution<int> dist(0, points.size() - 1);
    
    while (int(indices.size()) < mssSize)
    {
        // add a random index (is ignored if already contained)
        indices.insert(dist(randomEngine));
    }
    
    std::vector<Eigen::Vector3f> mss;
    mss.reserve(mssSize);
    
    for (int index : indices)
    {
        mss.push_back(points[index]);
    }
    
    return mss;
}

}
