#include "CylinderRansac.h"

#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/sac_model_cylinder.h>

#include <SemanticObjectRelations/ShapeExtraction/util/SoftMinMax.h>

namespace semrel
{

CylinderRansac::CylinderRansac()
{
    errorFunction = CylinderErrorFunction;
}

void semrel::CylinderRansac::setUseRandomSeed(bool useRandomSeed)
{
    this->useRandomSeed = useRandomSeed;
}

pcl::SampleConsensusModel<CylinderRansac::PointR>::Ptr CylinderRansac::getSampleConsensusModel()
{
    // normal estimation
    pcl::NormalEstimation<PointR, pcl::PointNormal> normalEstimation;
    normalEstimation.setInputCloud(pointCloud);

    pcl::search::KdTree<PointR>::Ptr searchTree(new pcl::search::KdTree<PointR>());
    normalEstimation.setSearchMethod(searchTree);
    normalEstimation.setKSearch(20);

    // perform normal estimation
    pcl::PointCloud<pcl::PointNormal>::Ptr pcNormals(new pcl::PointCloud<pcl::PointNormal>());
    normalEstimation.compute(*pcNormals);


    // build model
    pcl::SampleConsensusModelCylinder<PointR, pcl::PointNormal>::Ptr modelCylinder(
                new pcl::SampleConsensusModelCylinder<PointR, pcl::PointNormal>(pointCloud, useRandomSeed));

    modelCylinder->setInputNormals(pcNormals);

    return modelCylinder;
}

std::unique_ptr<Shape> CylinderRansac::makeShape(const Eigen::VectorXf& coefficients,
                                                 const std::vector<int>& inlierIndices)
{
    /*
     * Cylinder coefficients (7D):
     * [point_on_axis.x point_on_axis.y point_on_axis.z
     *  axis_direction.x axis_direction.y axis_direction.z
     *  radius]
     * Source: http://docs.pointclouds.org/trunk/group__sample__consensus.html
     */
    Eigen::Vector3f position = coefficients.head(3);
    Eigen::Vector3f direction = coefficients.segment(3, 3); // starting at 3, 3 coefficients
    float radius = coefficients(6) * 1.1;

    // intermediate result
    std::unique_ptr<Cylinder> cylinder(new Cylinder(position, direction, radius));

    // compute height from inliers
    // project inliers onto cylinder axis and get their line parameter
    // take the min and max line parameter => top, bottom
    // recompute cylinder position

    SoftMinMax tMinMax(outlierRate, inlierIndices.size());

    ParametrizedLine3f axis = cylinder->getAxis();
    for (int i : inlierIndices)
    {
        // line(t) = o + t*d = p  ==>   t*d = p - o   ==>  t = '(p - o) / d'
        Eigen::Vector3f pointOnAxis = axis.projection(points[i]);
        float t = (pointOnAxis(0) - axis.origin()(0)) / axis.direction()(0);
        tMinMax.add(t);
    }

    float minT = tMinMax.getSoftMin();
    float maxT = tMinMax.getSoftMax();

    Eigen::Vector3f bot = axis.pointAt(minT);
    Eigen::Vector3f top = axis.pointAt(maxT);

    // mid point is new position
    Eigen::Vector3f newPosition = 0.5 * (bot + top);

    // height is distance between top and bot
    float height = (bot - top).norm();
    
    cylinder->setHeight(height);
    cylinder->setPosition(newPosition);

    return cylinder;
}

std::string CylinderRansac::shapeName() const
{
    return "Cylinder";
}

float CylinderRansac::getSizeMeasure(const Shape& shape) const
{
    return static_cast<const Cylinder&>(shape).getRadius();
}

}


