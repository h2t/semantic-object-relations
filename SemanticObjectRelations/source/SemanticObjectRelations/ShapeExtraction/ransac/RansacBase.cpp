#include "RansacBase.h"

#include <SemanticObjectRelations/Shapes.h>

#include <SemanticObjectRelations/ShapeExtraction/util/SoftMinMax.h>

using namespace semrel;


RansacBase::RansacBase(int maxIterations, float distanceThreshold, float outlierRate,
                       float sizePenaltyFactor, float sizePenaltyExponent) :
    maxIterations(maxIterations), distanceThreshold(distanceThreshold), outlierRate(outlierRate),
    sizePenaltyFactor(sizePenaltyFactor), sizePenaltyExponent(sizePenaltyExponent)
{
}

void RansacBase::setPointCloud(const pcl::PointCloud<PointL>& pointCloud)
{
    points.clear();
    points.reserve(pointCloud.size());
    std::transform(pointCloud.begin(), pointCloud.end(), std::back_inserter(points),
                   [](const PointL & p)
    {
        return Eigen::Vector3f(p.x, p.y, p.z);
    });
}

RansacBase::Evaluation RansacBase::evaluate(const Shape& model) const
{
    return evaluate(model, distanceThreshold * distanceThreshold);
}

RansacBase::Evaluation RansacBase::evaluate(const Shape& model, float inlierErrorThreshold) const
{
    // allow some points to be ignored for error sum to cope with outliers
    std::size_t numInliers = 0;

    std::vector<float> errors;
    SoftMinMax errorSoftMax(outlierRate, points.size());


    std::vector<float> pointErrors = errorFunction(points, model);

    for (std::size_t i = 0; i < pointErrors.size(); ++i)
    {
        float pointError = pointErrors[i];
        if (pointError <= inlierErrorThreshold)
        {
            numInliers++;
        }
        errors.push_back(pointError);
        errorSoftMax.add(pointError);
    }

    float maxError = errorSoftMax.getSoftMax();

    // only add errors <= maxError
    float errorSum = 0;
    for (float pointError : errors)
    {
        errorSum += (pointError <= maxError ? pointError : 0);
    }

    //float error = errorSum / numInliers;
    float error = (numInliers > 0 ? errorSum / points.size() : std::numeric_limits<float>::max());
    error += sizePenalty(model);

    float coverage = numInliers / float(points.size());

    return Evaluation {error, coverage};
}

float RansacBase::sizePenalty(const Shape& model) const
{
    // get the size measure depending on the shape type.
    float sizeMeasure = getSizeMeasure(model);

    return sizePenaltyFactor * std::pow(sizeMeasure, sizePenaltyExponent);
}

void RansacBase::setMaxIterations(int maxIterations)
{
    this->maxIterations = maxIterations;
}

void RansacBase::setDistanceThreshold(float distanceThreshold)
{
    this->distanceThreshold = distanceThreshold;
}

void RansacBase::setOutlierRate(float outlierRate)
{
    this->outlierRate = outlierRate;
}

void RansacBase::setSizePenalty(float factor, float exponent)
{
    this->sizePenaltyFactor = factor;
    this->sizePenaltyExponent = exponent;
}

void RansacBase::setSizePenaltyFactor(float factor)
{
    this->sizePenaltyFactor = factor;
}

void RansacBase::setSizePenaltyExponent(float exponent)
{
    this->sizePenaltyExponent = exponent;
}

RansacResult::RansacResult() : RansacResult(nullptr, 0, 0)
{
}

RansacResult::RansacResult(std::unique_ptr<Shape>&& shape, float error, float coverage)
    : shape(std::move(shape)), info {error, coverage}
{
}

