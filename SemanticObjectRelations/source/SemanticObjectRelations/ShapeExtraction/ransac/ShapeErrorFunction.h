#pragma once

#include <functional>
#include <vector>

#include <Eigen/Core>

#include <SemanticObjectRelations/Shapes/Shape.h>


namespace semrel
{

    /// Returns the squared distance of the given point to the shape.
    using ShapeErrorFunction = std::function<
        std::vector<float>(const std::vector<Eigen::Vector3f>& points, const Shape& model)>;

    std::vector<float> BoxErrorFunction(const std::vector<Eigen::Vector3f>& points,
                                        const Shape& model);

    std::vector<float> CylinderErrorFunction(const  std::vector<Eigen::Vector3f>& points,
                                             const Shape& model);

    std::vector<float> SphereErrorFunction(const std::vector<Eigen::Vector3f>& points,
                                           const Shape& model);


}
