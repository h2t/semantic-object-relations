#pragma once

#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model.h>

#include "RansacBase.h"

namespace semrel
{

/**
 * @brief Base class for RANSAC shape fitting based on PCL sample consensus.
 *
 * The main logic is implemented in `fitShape()`.
 * To specialize this class for a specific shape type, implement the pure
 * virtual methods `getSampleConsensusModel()` and `makeShape()` for the
 * specific shape type.
 *
 * @param @see RansacBase
 */
class RansacPCL : public RansacBase
{

public:

    /// Constructor for default parameters.
    RansacPCL();

    /// Constructor with given max iterations and distance threshold.
    RansacPCL(int maxIterations, float distanceThreshold);


    /// @see RansacBase.
    virtual RansacResult fitShape(const pcl::PointCloud<PointL>& pointCloud) override;


protected:

    using PointR = pcl::PointXYZ; ///< Point type for PCL RANSAC.

    /// @see RansacBase. Also stores the point cloud in a format for PCL RANSAC.
    virtual void setPointCloud(const pcl::PointCloud<PointL>& pointCloud) override;

    /// Return the pcl::SampleConsensusModel for the fitted Shape type.
    virtual pcl::SampleConsensusModel<PointR>::Ptr getSampleConsensusModel() = 0;

    /**
     * From the given model coefficients and inliers, build the Shape model.
     * @param coefficients the model coefficients (as returned by PCL RANSAC)
     * @param inlierIndices the indices of the inliers
     */
    virtual std::unique_ptr<Shape> makeShape(const Eigen::VectorXf& coefficients,
                                             const std::vector<int>& inlierIndices) = 0;


    /// The point cloud formatted for PCL RANSAC.
    pcl::PointCloud<PointR>::Ptr pointCloud;

};

}
