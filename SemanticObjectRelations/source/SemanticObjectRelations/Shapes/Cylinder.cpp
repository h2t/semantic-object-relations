#include "Cylinder.h"

#include <btBulletCollisionCommon.h>

#include <SemanticObjectRelations/Shapes/utils/LineIntersection.h>


namespace semrel
{

using namespace Eigen;


Cylinder::Cylinder(ShapeID id) : Shape(id)
{
}

Cylinder::Cylinder(const Vector3f& position, const Vector3f& direction,
                   float radius, float height, ShapeID id) :
    Shape(id), axis(position, direction.normalized()), radius(radius), height(height)
{
}

Cylinder::Cylinder(const ParametrizedLine3f& centralAxis,
                   float radius, float height, ShapeID id) :
    Shape(id), axis(centralAxis), radius(radius), height(height)
{
}

Vector3f Cylinder::getPosition() const
{
    return axis.origin();
}

void Cylinder::setPosition(const Vector3f& position)
{
    axis = ParametrizedLine3f(position, axis.direction());
}


Vector3f Cylinder::getAxisDirection() const
{
    return axis.direction();
}

void Cylinder::setAxisDirection(const Vector3f& direction)
{
    axis = ParametrizedLine3f(axis.origin(), direction.normalized());
}

Quaternionf Cylinder::getOrientation() const
{
    // return rotation from y axis to direction
    // rotation around central axis does not matter
    return Quaternionf::FromTwoVectors(Vector3f::UnitY(), getAxisDirection());
}

void Cylinder::setOrientation(const Quaternionf& orientation)
{
    setAxisDirection(orientation * Vector3f::UnitY());
}


ParametrizedLine3f Cylinder::getAxis() const
{
    return axis;
}

void Cylinder::setAxis(const ParametrizedLine3f& value)
{
    axis = value;
}

void Cylinder::setAxis(const Vector3f& position, Vector3f& direction)
{
    axis = ParametrizedLine3f(position, direction.normalized());
}


float Cylinder::getRadius() const
{
    return radius;
}

void Cylinder::setRadius(float value)
{
    radius = value;
}


float Cylinder::getHeight() const
{
    return height;
}

void Cylinder::setHeight(float value)
{
    height = value;
}

bool Cylinder::hasHeight() const
{
    return std::abs(height) > 1e-6f;
}

Vector3f Cylinder::getTop() const
{
    return axis.pointAt(height / 2);
}

Vector3f Cylinder::getBottom() const
{
    return axis.pointAt(- height / 2);
}

Hyperplane3f Cylinder::getTopPlane() const
{
    return Hyperplane3f(axis.direction(), getTop());
}

Hyperplane3f Cylinder::getBottomPlane() const
{
    return Hyperplane3f(- axis.direction(), getBottom());
}

TriMesh Cylinder::getTriMeshLocal() const
{
    TriMesh mesh;

    /* sample the top and bottom circle with 8 points each
     * standard orientation: central axis = y-axis
     *
     *       v0          ^
     *   v7  |  v1       | x
     *     \ | /         |
     * v6--- c ---v2     *---->
     *     / | \            z
     *   v5  |  v3
     *       v4
     *
     * scaled to radius r
     */

    // generate vertices

    Vector3f x = Vector3f::UnitX();
    Vector3f y = Vector3f::UnitY();
    Vector3f z = Vector3f::UnitZ();

    auto addCircle = [&](Vector3f ctr)
    {
        mesh.addVertex(ctr + radius * x);
        mesh.addVertex(ctr + radius * (x + z).normalized());
        mesh.addVertex(ctr + radius * z);
        mesh.addVertex(ctr + radius * (-x + z).normalized());
        mesh.addVertex(ctr + radius * (-x));
        mesh.addVertex(ctr + radius * (-x - z).normalized());
        mesh.addVertex(ctr + radius * (-z));
        mesh.addVertex(ctr + radius * (x - z).normalized());
    };

    addCircle(height / 2 * y); // top
    addCircle(-height / 2 * y); // bottom

    // normals
    mesh.addNormal(y);
    mesh.addNormal(-y);
    int normalTop = 0;
    int normalBottom = 1;

    // generate indices

    // top circle and bottom circle: 6 triangles each (triangle fan)
    // 0-7 = top circle, 8-15 = bottom circle

    auto triangulateCircle = [&](int base, int normal)
    {
        for (int i = base + 1; i <= base + 6; ++i)
        {
            mesh.addTriangle(TriMesh::Triangle::singleNormal(base, i, i + 1, normal));
        }
    };

    triangulateCircle(0, normalTop); // top
    triangulateCircle(8, normalBottom); // bottom


    /* mantle = sequence of rectangles
     *
     * t: i--(i+)
     *    | \ |
     * b: j--(j+)   with j = i+8
     *
     * => (i i+8 i+9), (i i+9 i+1) with i in [0, ..., 6] (i+1 <= 7)
     */

    // vertices i and i+8 have same normal => add 8 normals
    int n0 = static_cast<int>(mesh.getNormals().size());
    for (int i = 0; i < 8; ++i)
    {
        Eigen::Vector3f n = mesh.getVertex(i);
        // discard y component and normalize
        n(1) = 0;
        mesh.addNormal(n.normalized());
    }
    // vertex i and i+8 have normal n0 + i

    for (int i = 0; i <= 6; ++i)
    {
        int n = n0 + i;
        mesh.addTriangle(TriMesh::Triangle(i, i + 8, i + 9, n, n, n + 1));
        mesh.addTriangle(TriMesh::Triangle(i, i + 9, i + 1, n, n + 1, n + 1));
    }
    /* final rectangle: 15 8
         *                  7  0 */
    mesh.addTriangle(TriMesh::Triangle(0, 8, 7, n0, n0, n0 + 7));
    mesh.addTriangle(TriMesh::Triangle(15, 7, 8, n0 + 7, n0 + 7, n0));

    return mesh;
}

AxisAlignedBoundingBox Cylinder::getAABBLocal() const
{
    AxisAlignedBoundingBox aabb;
    aabb.setExtents(2 * Eigen::Vector3f(radius, height/2, radius));
    return aabb;
}

float Cylinder::getBoundingSphereRadius() const
{
    /// (r_s)^2 = (h/2)^2 + (r_c)^2
    return std::sqrt(height*height/4 + radius*radius);
}

std::string Cylinder::name() const
{
    return "Cylinder";
}

std::string Cylinder::str() const
{
    std::stringstream ss;
    ss << "Cylinder ["
       << "axis: " << toString(axis.origin())
       << " -> " << toString(axis.direction())
       << "), radius: " << radius
       << ", height: " << height
       << ", " << Shape::str()
       << "]";
    return ss.str();
}

std::shared_ptr<btCollisionShape> Cylinder::getBulletCollisionShape(float margin) const
{
    // btCylinderShape is Y-aligned => half extents x and z = radius, half extent y = height/2
    float radius = this->radius + margin;
    float halfHeight = 0.5f * this->height + margin;
    return std::make_shared<btCylinderShape>(btVector3(static_cast<btScalar>(radius),
                                                       static_cast<btScalar>(halfHeight),
                                                       static_cast<btScalar>(radius)));
}

void Cylinder::addMargin(float margin)
{
    radius += margin;
    height += 2 * margin;
}

}
