#include "Box.h"

#include <set>

#include <btBulletCollisionCommon.h>

#include <SemanticObjectRelations/Shapes/utils/LineIntersection.h>

#include "Sphere.h"


namespace semrel
{

Box::Box(ShapeID id) : Shape(id)
{}

Box::Box(const Eigen::Vector3f& position, const Eigen::Quaternionf& orientation,
         const Eigen::Vector3f& extents, ShapeID id) :
    Shape(id), position(position), orientation(orientation), extents(extents)
{}

Box::Box(const Eigen::Vector3f& position, const Eigen::Matrix3f& axes,
         const Eigen::Vector3f& extents, ShapeID id) :
    Shape(id), position(position), orientation(axes), extents(extents)
{}

Box::Box(const AxisAlignedBoundingBox& aabb, ShapeID id) :
    Box(aabb, aabb.center(), Eigen::Quaternionf::Identity(), id)
{}

Box::Box(const AxisAlignedBoundingBox& aabb, const Eigen::Vector3f& position,
         const Eigen::Quaternionf& orientation, ShapeID id) :
    Box(position, orientation, aabb.extents(), id)
{}

Box Box::fromAABBLocal(const Shape& shape)
{
    return Box(shape.getAABBLocal(), shape.getPosition(), shape.getOrientation(), shape.getID());
}

Box Box::fromAABB(const Shape& shape)
{
    return Box(shape.getAABB(), shape.getPosition(), shape.getOrientation(), shape.getID());
}

Eigen::Vector3f Box::getPosition() const
{
    return position;
}

void Box::setPosition(const Eigen::Vector3f& position)
{
    this->position = position;
}

Eigen::Quaternionf Box::getOrientation() const
{
    return orientation;
}

void Box::setOrientation(const Eigen::Quaternionf& orientation)
{
    this->orientation = orientation;
}

Eigen::Matrix3f Box::getAxes() const
{
    return orientation.toRotationMatrix();
}

void Box::setAxes(const Eigen::Matrix3f& axes)
{
    orientation = Eigen::Quaternionf(axes);
}

Eigen::Vector3f Box::getExtents() const
{
    return extents;
}

void Box::setExtents(const Eigen::Vector3f& extents)
{
    this->extents = extents;
}

std::vector<Hyperplane3f> Box::getFaces() const
{
    Eigen::Matrix3f axes = getAxes();

    std::vector<Hyperplane3f> faces;
    for (int i = 0; i < 3; ++i)
    {
        Eigen::Vector3f normal = axes.col(i);
        // "front side"
        faces.push_back(Hyperplane3f(normal, position + extents[i] / 2 * normal));
        // "back side"
        faces.push_back(Hyperplane3f(- normal, position - extents[i] / 2 * normal));
    }
    return faces;
}

std::vector<Eigen::Vector3f> Box::getCorners() const
{
    return getTriMesh().getVertices();
}

/*
 *
 *        7_________________ 3
 *       /|                /|
 *      / |               / |    ^
 *    4/________________0/  |    |
 *     |  |              |  |    | y
 *     |  |              |  |    |
 *     |  |      x       |  |
 *     |  |6_____________|__|2
 *     |  /              |  /    /
 *     | /               | /    / z
 *     |/________________|/    v
 *    5                  1
 *         x ----->
 */
TriMesh Box::getTriMeshLocal() const
{
    TriMesh mesh;

    // local => unrotated => axes = identity
    Eigen::Matrix3f axes = Eigen::Matrix3f::Identity();
    const Eigen::Vector3f& nx = axes.col(0);
    const Eigen::Vector3f& ny = axes.col(1);
    const Eigen::Vector3f& nz = axes.col(2);

    auto addFaceVertices = [&](Eigen::Vector3f base)
    {
        mesh.addVertex(base + extents[1] / 2 * ny + extents[2] / 2 * nz);
        mesh.addVertex(base - extents[1] / 2 * ny + extents[2] / 2 * nz);
        mesh.addVertex(base - extents[1] / 2 * ny - extents[2] / 2 * nz);
        mesh.addVertex(base + extents[1] / 2 * ny - extents[2] / 2 * nz);
    };

    addFaceVertices(extents[0] / 2 * nx); // first face: along +n1
    addFaceVertices(- extents[0] / 2 * nx); // second face along -n1

    // normals
    mesh.addNormal(nx);
    mesh.addNormal(-nx);
    mesh.addNormal(ny);
    mesh.addNormal(-ny);
    mesh.addNormal(nz);
    mesh.addNormal(-nz);

    int n1Pos = 0, n1Neg = 1, n2Pos = 2, n2Neg = 3, n3Pos = 4, n3Neg = 5;

    mesh.addTriangle(TriMesh::Triangle::singleNormal(0, 1, 5, n3Pos));   // 0 4
    mesh.addTriangle(TriMesh::Triangle::singleNormal(0, 5, 4, n3Pos));   // 1 5

    mesh.addTriangle(TriMesh::Triangle::singleNormal(0, 3, 2, n1Pos));   // 3 0
    mesh.addTriangle(TriMesh::Triangle::singleNormal(0, 2, 1, n1Pos));   // 2 1

    mesh.addTriangle(TriMesh::Triangle::singleNormal(0, 7, 3, n2Pos));   // 7 4
    mesh.addTriangle(TriMesh::Triangle::singleNormal(0, 4, 7, n2Pos));   // 3 0

    mesh.addTriangle(TriMesh::Triangle::singleNormal(6, 1, 2, n2Neg));   // 2 1
    mesh.addTriangle(TriMesh::Triangle::singleNormal(6, 5, 1, n2Neg));   // 6 5

    mesh.addTriangle(TriMesh::Triangle::singleNormal(6, 3, 7, n3Neg));   // 7 3
    mesh.addTriangle(TriMesh::Triangle::singleNormal(6, 2, 3, n3Neg));   // 6 2

    mesh.addTriangle(TriMesh::Triangle::singleNormal(6, 7, 4, n1Neg));   // 4 7
    mesh.addTriangle(TriMesh::Triangle::singleNormal(6, 4, 5, n1Neg));   // 5 6

    return mesh;
}

AxisAlignedBoundingBox Box::getAABBLocal() const
{
    AxisAlignedBoundingBox aabb;
    aabb.setExtents(extents);
    return AxisAlignedBoundingBox(aabb);
}

float Box::getBoundingSphereRadius() const
{
    return (extents / 2).norm();
}

std::string Box::name() const
{
    return "Box";
}

Eigen::Quaternionf Box::orientationFromAxes(const Eigen::Matrix3f& axes)
{
    // axes is a rotation matrix describing the same rotation as orientation
    return Eigen::Quaternionf(axes);
}

std::string Box::str() const
{
    std::stringstream ss;
    ss << name() << " ["
       << "pos: " << toString(position)
       << ", ori: " << toString(orientation)
       << ", extents: " << toString(extents)
       << ", " << Shape::str()
       << "]";
    return ss.str();
}

std::shared_ptr<btCollisionShape> Box::getBulletCollisionShape(float margin) const
{
    // btBoxShape uses half extents
    Eigen::Matrix<btScalar, 3, 1> btExtents = (0.5 * extents + margin * Eigen::Vector3f(1, 1, 1)).cast<btScalar>();
    return std::make_shared<btBoxShape>(btVector3(btExtents(0), btExtents(1), btExtents(2)));
}

void Box::addMargin(float margin)
{
    extents += 2 * margin * Eigen::Vector3f(1, 1, 1);
}

}
