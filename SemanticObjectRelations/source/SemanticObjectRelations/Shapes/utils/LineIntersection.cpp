#include "LineIntersection.h"

#include <set>


namespace semrel
{


LineIntersectionResult intersectWithLine(const Shape& shape, const ParametrizedLine3f& line, bool asRay)
{
    if (auto* box = dynamic_cast<const Box*>(&shape))
    {
        return intersectWithLine(*box, line, asRay);
    }
    else if (auto* cyl = dynamic_cast<const Cylinder*>(&shape))
    {
        return intersectWithLine(*cyl, line, asRay);
    }
    else if (auto* msh = dynamic_cast<const MeshShape*>(&shape))
    {
        return intersectWithLine(*msh, line, asRay);
    }
    else if (auto* sph = dynamic_cast<const Sphere*>(&shape))
    {
        return intersectWithLine(*sph, line, asRay);
    }
    throw std::logic_error("Shape of unexpected type: \n" + shape.str());
}


LineIntersectionResult intersectWithLine(const Box& box, const ParametrizedLine3f& line, bool asRay)
{
    // intersect with the six planes
    std::vector<float> intersections;
    intersections.reserve(6);

    const std::vector<Hyperplane3f> faces = box.getFaces();
    const Eigen::Vector3f dimensions = box.getExtents();
    const Eigen::Matrix3f axes = box.getAxes();

    for (std::size_t i = 0; i < faces.size(); ++i)
    {
        const Hyperplane3f& face = faces[i];

        const int axisNum = static_cast<int>(i) / 2; // 0,1 -> 0 | 2,3 -> 2 | 4,5 -> 3
        std::set<int> otherAxes = {0, 1, 2};
        otherAxes.erase(axisNum);

        // intersect
        float t = line.intersectionParameter(face);
        if (asRay && t < 0)
        {
            continue; // ingore negative values
        }

        // check whether the intersection is inside the face
        const Eigen::Vector3f point = line.pointAt(t);
        const Eigen::Vector3f pointRel = point - box.getPosition();

        // project point onto other axes and check for boundaries
        bool inside = true;
        for (int axisIndex : otherAxes)
        {
            const Eigen::Vector3f axis = axes.col(axisIndex);
            float onAxis = axis.dot(pointRel);
            if (std::abs(onAxis) > dimensions(axisIndex))
            {
                inside = false;
                break;
            }
        }

        if (inside)
        {
            intersections.push_back(t);
        }
    }

    LineIntersectionResult result;
    result.found = (intersections.size() > 0);
    if (result.found)
    {
        detail::storeNearestIntersection(intersections, line, result);
    }
    return result;
}


LineIntersectionResult intersectWithLine(const Cylinder& cylinder, const ParametrizedLine3f& line, bool asRay)
{
    std::vector<float> intersections;
    intersections.reserve(4);

    const float radius = cylinder.getRadius();

    auto intersectTopBot = [&](Hyperplane3f plane, Eigen::Vector3f center)
    {
        float t = line.intersectionParameter(plane);
        if ((line.pointAt(t) - center).squaredNorm() <= radius * radius)
        {
            intersections.push_back(t);
        }
    };

    intersectTopBot(cylinder.getTopPlane(), cylinder.getTop());
    intersectTopBot(cylinder.getBottomPlane(), cylinder.getBottom());


    // intersect with mantle

    // rotate line to space where cylinder axis is aligned with y axis and solve quadratic equation
    // (rotation keeps length)

    const Eigen::Quaternionf orientation = cylinder.getOrientation(); // rotates y axis to cylinder axis
    const Eigen::Quaternionf toYAligned = orientation.inverse();

    const Eigen::Vector3f origin = toYAligned * line.origin();
    const Eigen::Vector3f dir = toYAligned * line.direction();

    /* Solve equation system for t:
     * Line: p = origin + t * dir
     * Cyl:  x^2 + z^2 = r^2
     *
     * (p = (x,y,z), origin=(ox,oy,oz), dir=(dx,dy,dz))
     *
     * Results in the quadratic equation t^2 * a + t * b + c = 0:
     *
     * t^2 * (dx^2 + dz^2) + t * (2 ox dx + 2 oz dz) + (ox^2 + oz^2 - r^2) = 0
     *
     * Solve with abc-formula:
     *
     * t1/2 = (-b +- sqrt(b^2 - 4ac)) / 2a
     */

    const float ox = origin(0), oz = origin(2);
    const float dx = dir(0), dz = dir(2);

    const float a = dx * dx + dz * dz;
    const float b = 2 * (ox * dx + oz * dz);
    const float c = ox * ox + oz * oz - radius * radius;

    detail::solveQuadratic(a, b, c, asRay, intersections);

    LineIntersectionResult result;
    result.found = (intersections.size() > 0);
    if (result.found)
    {
        detail::storeNearestIntersection(intersections, line, result);
    }
    return result;
}


LineIntersectionResult intersectWithLine(const MeshShape& meshShape, const ParametrizedLine3f& line, bool asRay)
{
    const TriMesh& mesh = meshShape.mesh();

    const Eigen::Vector3f& origin = line.origin();
    const Eigen::Vector3f& dir = line.direction();

    bool hit = false;

    float tMin = std::numeric_limits<float>::max();

    for (const TriMesh::Triangle& triangle : mesh.getTriangles())
    {
        // from: http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/raytri/

        const Eigen::Vector3f& v0 = mesh.getVertex(triangle.v0);
        const Eigen::Vector3f& v1 = mesh.getVertex(triangle.v1);
        const Eigen::Vector3f& v2 = mesh.getVertex(triangle.v2);

        const Eigen::Vector3f edge2 = v2 - v0;
        const Eigen::Vector3f edge1 = v1 - v0;

        const Eigen::Vector3f pvec = dir.cross(edge2);

        const float det = edge1.dot(pvec);

        // if determinant is near zero, the ray lies in plane of triangle
        if (std::abs(det) < 1e-6f)
        {
            continue;
        }

        const float detInv = 1.f / det;

        // calculate distance from v0 to origin
        const Eigen::Vector3f tvec = origin - v0;

        // calculate U parameter and test bounds
        const float u = tvec.dot(pvec) * detInv;
        if (u < 0.f || u > 1.f)
        {
            continue;
        }

        // prepare to test V parameter
        const Eigen::Vector3f qvec = tvec.cross(edge1);

        // calculate V parameter and test bounds
        const float v = dir.dot(qvec) * detInv;
        if (v < 0.f || u + v > 1.f)
        {
            continue;
        }

        // calculate t, ray intersects triangle
        const float t = edge2.dot(qvec) * detInv;

        if (t < 0 && asRay)
        {
            continue;
        }

        hit = true;
        if (std::abs(t) < std::abs(tMin))
        {
            tMin = t;
        }
    }

    LineIntersectionResult result;
    result.found = hit;
    if (result.found)
    {
        result.parameter = tMin;
        result.point = line.pointAt(tMin);
    }
    return result;
}


LineIntersectionResult intersectWithLine(const Sphere& sphere, const ParametrizedLine3f& line, bool asRay)
{
    const Eigen::Vector3f originRel = line.origin() - sphere.getPosition();
    const Eigen::Vector3f dir = line.direction();
    const float radius = sphere.getRadius();

    /* Solve equation system:
     * Sphere: x^2 + y^2 + z^2 = r^2
     * Line:   (x y z) = o + t * d
     *
     * t^2 * a + t * b + c = 0 with:
     *
     * a = dx^2 + dy^2 + dz^2 = d.squaredNorm()
     * b = 2 * (ox*dx + oy*dy + oz*dz) = o.dot(d)
     * c = ox^2 + oy^2 + oz^2 - r^2 = o.squaredNorm() - r^2
    */

    const float a = dir.squaredNorm();
    const float b = originRel.dot(dir);
    const float c = originRel.squaredNorm() - radius * radius;

    std::vector<float> intersections;
    intersections.reserve(2);
    detail::solveQuadratic(a, b, c, asRay, intersections);

    LineIntersectionResult result;
    result.found = (intersections.size() > 0);
    if (result.found)
    {
        detail::storeNearestIntersection(intersections, line, result);
    }
    return result;
}


namespace detail
{

void solveQuadratic(float a, float b, float c, bool asRay, std::vector<float>& intersections)
{
    float discriminant = b * b - 4 * a * c;
    if (discriminant < 0)
    {
        // do nothing
    }
    else if (discriminant <= 0)  // use <= to squelch compiler warning
    {
        float t = -b / 2 * a;
        if (!asRay || t >= 0)
        {
            intersections.push_back(-b / 2 * a);
        }
    }
    else
    {
        float t1 = (-b + std::sqrt(discriminant)) / 2 * a;
        float t2 = (-b - std::sqrt(discriminant)) / 2 * a;

        if (!asRay || t1 >= 0)
        {
            intersections.push_back(t1);
        }
        if (!asRay || t2 >= 0)
        {
            intersections.push_back(t2);
        }
    }
}


void storeNearestIntersection(const std::vector<float>& intersections,
                              const ParametrizedLine3f& line,
                              LineIntersectionResult& result)
{
    // find intersection nearest line origin
    auto absComp = [](float t, float s)
    {
        return std::abs(t) < std::abs(s);
    };
    float parameter = *std::min_element(intersections.begin(), intersections.end(), absComp);
    result.parameter = parameter;
    result.point = line.pointAt(parameter);
}

}

}
