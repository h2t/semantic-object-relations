#pragma once

#include <SemanticObjectRelations/Shapes.h>


namespace semrel
{
    struct LineIntersectionResult
    {
        /// True if an intersection has been found, false otherwise.
        bool found;
        /// The intersection line parameter if found.
        float parameter;
        /// The intersection point if found.
        Eigen::Vector3f point;
    };




    /**
     * @brief Computes the intersection between this shape and a line or ray.
     *
     * This method computes the point of intersection between a parametrized line and a
     * shape object which is closest to the line's origin.
     * If asRay is true (default), only intersections in the positive direction of the
     * line are considered. If asRay is false, the point of intersection with the smallest
     * absolute distance to the line's origin is kept.
     */

    LineIntersectionResult intersectWithLine(const Shape& shape, const ParametrizedLine3f& line, bool asRay);

    LineIntersectionResult intersectWithLine(const Box& box, const ParametrizedLine3f& line, bool asRay);
    LineIntersectionResult intersectWithLine(const Cylinder& cylinder, const ParametrizedLine3f& line, bool asRay);
    LineIntersectionResult intersectWithLine(const MeshShape& mesh, const ParametrizedLine3f& line, bool asRay);
    LineIntersectionResult intersectWithLine(const Sphere& sphere, const ParametrizedLine3f& line, bool asRay);


    namespace detail
    {
        /**
         * @brief Solve a quadratic equation of the form `a * t^2 + b * t + c = 0`
         * and store found (unique) solutions in the given intersections vector.
         * @param asRay if true, only positive solutions are stored
         */
        void solveQuadratic(float a, float b, float c, bool asRay, std::vector<float>& intersections);


        /**
         * @brief Store the nearest intersection in a line intersection result.
         * @param intersections
         * @param line
         * @param result
         */
        void storeNearestIntersection(const std::vector<float>& intersections,
                                      const ParametrizedLine3f& line,
                                      LineIntersectionResult& result);
    }

}
