#pragma once

#include <iosfwd>
#include <memory>

#include <Eigen/Geometry>

#include <boost/serialization/strong_typedef.hpp>

#include "AxisAlignedBoundingBox.h"
#include "EigenTypedefs.h"
#include "TriMesh.h"


// Forward declaration of bullet's btCollisionShape.
class btCollisionShape;


namespace semrel
{

/// A shape's ID for arbitrary usage.
BOOST_STRONG_TYPEDEF(long, ShapeID)
/// Output stream operator for ShapeID.
std::ostream& operator <<(std::ostream& os, const ShapeID& id);


class Sphere;


/**
 * @brief A geometric shape in 3D space.
 *
 * A shape has a position and an orientation in its parent coordinate frame.
 * Every shape has a numeric ID whose use is up to the user.
 * Every shape is representable by a triangle mesh (which may be only approximate).
 */
class Shape
{

public:

    /// Construct Shape with given ID.
    explicit Shape(ShapeID id = ShapeID(0));

    /// Virtual destructor.
    virtual ~Shape() = default;


    /// Get the ID.
    ShapeID getID() const;
    /// Set the ID.
    void setID(ShapeID id);


    // POSE

    /// Get the position.
    virtual Eigen::Vector3f getPosition() const = 0;
    /// Set the position.
    virtual void setPosition(const Eigen::Vector3f& position) = 0;
    /// Get the orientation.
    virtual Eigen::Quaternionf getOrientation() const = 0;
    /// Set the orientation.
    virtual void setOrientation(const Eigen::Quaternionf& orientation) = 0;


    // TRIANGLE MESH

    /**
     * @brief Get a triangle mesh in the local coordinate system (centered
     * around the origin).
     */
    virtual TriMesh getTriMeshLocal() const = 0;
    /**
     * @brief Get a triangle mesh in the global coordinate system.
     *
     * This is the result of applying the transformation specificed by getPosition()
     * and getOrientation() to the local triangle mesh (getTriMeshLocal()).
     */
    TriMesh getTriMesh() const;


    // BOUNDING BOX

    /// Get the axis aligned bounding box (AABB) in the local coordinate system.
    virtual AxisAlignedBoundingBox getAABBLocal() const;
    /// Get the axis aligned bounding box (AABB) in the global coordinate system.
    virtual AxisAlignedBoundingBox getAABB() const;

    /**
     * @brief Get the radius of the bounding sphere.
     * That is, the radius of the smallest sphere centered at the origin of
     * the local coordinate system that encompasses the whole shape.
     *
     * The default implementation returns the biggest norm of a vertex in the
     * local trangle mesh.
     */
    virtual float getBoundingSphereRadius() const;
    /// Get the bounding sphere in the local coordinate system (with position = 0).
    /// The returned Sphere's ID is the ID of this.
    virtual Sphere getBoundingSphereLocal() const;
    /// Get the bounding sphere in the global coordinate system (with position = this->getPosition()).
    /// The returned Sphere's ID is the ID of this.
    virtual Sphere getBoundingSphere() const;


    // MISC

    /// Get the name of the shape type.
    virtual std::string name() const;

    /// @brief Get a short prefix for the tag.
    /// By default, uses the first three letters of `name()`.
    virtual std::string tagPrefix() const;

    /// Get a short string "[type]_[id]"
    /// Calls `tagPrefix()` for [type] (by default, the first three letters of `name()`).
    std::string tag() const;

    /// Get a human readable string representation of this Shape.
    virtual std::string str() const;


    /// Return a bullet BtCollisionShape of this shape.
    virtual std::shared_ptr<btCollisionShape> getBulletCollisionShape(float margin = 0) const = 0;

    /// Increase the shape size by the given margin.
    virtual void addMargin(float margin) = 0;


protected:

    /// Print the vector to a string (helper for str() in derived classes).
    static std::string toString(const Eigen::Vector3f& vector);
    /// Print the quaternion to a string (helper for str() in derived classes).
    static std::string toString(const Eigen::Quaternionf& quaternion);


private:

    /// The shape's ID.
    ShapeID id {0};

};

/// An owning shape pointer.
using ShapePtr = std::unique_ptr<Shape>;

/// Print a description of `shapeList`'s contents to `os`.
std::ostream& operator <<(std::ostream& os, const Shape& shapeList);


}
