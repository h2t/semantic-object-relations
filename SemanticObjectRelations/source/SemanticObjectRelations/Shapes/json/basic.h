#pragma once

#include <SemanticObjectRelations/Serialization/json.h>
#include <SemanticObjectRelations/Shapes.h>


namespace semrel
{

namespace json
{
    /**
     * @brief Serializes general attributes of `shape` to JSON:
     *
     * - ID (`shape.getID()`)
     * - position (`shape.getPosition()`)
     * - orientation (`shape.getOrientation()`)
     */
    void to_json_base(nlohmann::json& j, const Shape& shape);
    /**
     * @brief Deerializes general attributes of `shape` from JSON:
     *
     * - ID (`shape.setID()`)
     * - position (`shape.setPosition()`)
     * - orientation (`shape.setOrientation()`)
     */
    void from_json_base(const nlohmann::json& j, Shape& shape);
}

    void to_json(nlohmann::json& j, const ShapeID& id);
    void from_json(const nlohmann::json& j, ShapeID& id);

    void to_json(nlohmann::json& j, const Box& box);
    void from_json(const nlohmann::json& j, Box& box);

    void to_json(nlohmann::json& j, const Cylinder& cyl);
    void from_json(const nlohmann::json& j, Cylinder& cyl);

    void to_json(nlohmann::json& j, const Sphere& sph);
    void from_json(const nlohmann::json& j, Sphere& sph);

    /*
     * Not supported.
    void to_json(nlohmann::json& j, const MeshShape& mesh);
    void from_json(const nlohmann::json& j, MeshShape& mesh);
    */

    void to_json(nlohmann::json& j, const AxisAlignedBoundingBox& value);
    void from_json(const nlohmann::json& j, AxisAlignedBoundingBox& value);

    /// For `to_json()`, `from_json()` for type `Shape`, see `json::ShapeSerializers`.

}
