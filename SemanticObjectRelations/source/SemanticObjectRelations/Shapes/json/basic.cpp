#include "basic.h"


void semrel::json::to_json_base(nlohmann::json& j, const Shape& shape)
{
    j["id"] = shape.getID();
    j["position"] = shape.getPosition();
    j["orientation"] = shape.getOrientation();
}

void semrel::json::from_json_base(const nlohmann::json& j, Shape& shape)
{
    shape.setID(j.at("id").get<ShapeID>());
    shape.setPosition(j.at("position").get<Eigen::Vector3f>());
    shape.setOrientation(j.at("orientation").get<Eigen::Quaternionf>());
}


void semrel::to_json(nlohmann::json& j, const semrel::ShapeID& id)
{
    j = static_cast<long>(id);
}

void semrel::from_json(const nlohmann::json& j, semrel::ShapeID& id)
{
    id = ShapeID(j.get<long>());
}


void semrel::to_json(nlohmann::json& j, const Box& box)
{
    json::to_json_base(j, box);
    j["extents"] = box.getExtents();
}

void semrel::from_json(const nlohmann::json& j, Box& cyl)
{
    json::from_json_base(j, cyl);
    cyl.setExtents(j.at("extents").get<Eigen::Vector3f>());
}

void semrel::to_json(nlohmann::json& j, const Cylinder& cyl)
{
    json::to_json_base(j, cyl);
    j["radius"] = cyl.getRadius();
    j["height"] = cyl.getHeight();
}

void semrel::from_json(const nlohmann::json& j, Cylinder& cyl)
{
    json::from_json_base(j, cyl);
    cyl.setRadius(j.at("radius").get<float>());
    cyl.setHeight(j.at("height").get<float>());
}

void semrel::to_json(nlohmann::json& j, const Sphere& sph)
{
    json::to_json_base(j, sph);
    j["radius"] = sph.getRadius();
}

void semrel::from_json(const nlohmann::json& j, Sphere& sph)
{
    json::from_json_base(j, sph);
    sph.setRadius(j.at("radius").get<float>());
}

void semrel::to_json(nlohmann::json& j, const semrel::AxisAlignedBoundingBox& value)
{
    j["limits"] = value.limits();
}

void semrel::from_json(const nlohmann::json& j, semrel::AxisAlignedBoundingBox& value)
{
    value.setLimits(j.at("limits").get<Eigen::Matrix32f>());
}


