#include "Sphere.h"

#include <btBulletCollisionCommon.h>

#include <SemanticObjectRelations/Shapes/utils/LineIntersection.h>


namespace semrel
{

Sphere::Sphere(ShapeID id) :
    Shape(id), position(Eigen::Vector3f::Zero()), radius(0)
{
}

Sphere::Sphere(const Eigen::Vector3f& position, float radius, ShapeID id) :
    Shape(id), position(position), radius(radius)
{
}

Eigen::Vector3f Sphere::getPosition() const
{
    return position;
}

void Sphere::setPosition(const Eigen::Vector3f& position)
{
    this->position = position;
}

Eigen::Quaternionf Sphere::getOrientation() const
{
    // does not have an orientation
    return Eigen::Quaternionf::Identity();
}

void Sphere::setOrientation(const Eigen::Quaternionf& orientation)
{
    // ignore, since Sphere has no orientation
    (void) orientation;
}

float Sphere::getRadius() const
{
    return radius;
}

void Sphere::setRadius(float radius)
{
    this->radius = radius;
}

TriMesh Sphere::getTriMeshLocal() const
{
    TriMesh mesh;

    /* Front view:
     *      __ 0 __
     *     /  /  \  \
     *   1 - 2 -- 3 - 4     (+ 5 6 7 8)
     *  /   /      \   \
     * 9 --10 ----11 -- 12  (+ 13 14 15 16)
     *  \   \      /   /
     *  17 -18 --19 - 20    (+ 21 22 23 24)
     *     \__\  /___/
     *         25
     * ^ z
     * |
     * +---> x
     */

    /* levels (vertically):
     * top    1
     * upper  8
     * mid    8
     * lower  8
     * bottom 1
     */

    /* spherical coordinate system:
     * x = r * sin(polar) * cos(azimuth)
     * y = r * sin(polar) * sin(azimuth)
     * z = r * cos(polar)
     */

    // normals (vertices are generated from normals by multiplying with r)

    auto addNormal = [&mesh](float x, float y, float z)
    {
        mesh.addNormal(Eigen::Vector3f(x, y, z));
    };

    addNormal(0, 0, 1); // top

    auto addLevel = [&addNormal](float polar)
    {
        for (int i = 0; i < 8; ++i)
        {
            float azimuth = i * 2 * M_PI / 8;
            addNormal(std::sin(polar) * std::cos(azimuth),
                      std::sin(polar) * std::sin(azimuth),
                      std::cos(polar));
        }
    };

    addLevel(M_PI_4);       // upper
    addLevel(M_PI_2);         // mid
    addLevel(3 * M_PI_4);   // lower

    addNormal(0, 0, -1); // bottom


    // vertices from normals
    for (Eigen::Vector3f& normal : mesh.getNormals())
    {
        mesh.addVertex(radius * normal);
    }


    // indices (vertex and normal indices are equal)
    auto addTriangle = [&mesh](int a, int b, int c)
    {
        mesh.addTriangle(TriMesh::Triangle(a, b, c, a, b, c));
    };

    // top to upper (<0 1 2> <0 2 3> ... <0 7 8> <0 8 1>)
    for (int i = 1; i < 8; ++i)
    {
        addTriangle(0, i, i + 1);
    };
    addTriangle(0, 8, 1);

    /* upper to mid: sequence of rectangles
     * u: i--(i+1)
     *    | \  |
     * m: j--(j+1)
     *
     * with j = i+8
     *
     * => (i i+8 i+9), (i i+9 i+1)
     * with i in [base, ..., base+6] (i+1 <= base+7)
     */

    auto addTween = [&](int base)
    {
        for (int i = base; i <= base + 6; ++i)
        {
            addTriangle(i, i + 8, i + 9);
            addTriangle(i, i + 9, i + 1);
        }
        addTriangle(base + 7, base + 15, base + 8);
        addTriangle(base + 7, base + 8, base);
    };

    addTween(1); // upper to mid
    addTween(9); // mid to lower

    // lower to bottom (<25 18 17> <25 19 18> ... <25 24 23> <25 17 24>)
    for (int i = 17; i < 24; ++i)
    {
        addTriangle(25, i + 1, i);
    };
    addTriangle(25, 17, 24);

    return mesh;
}

AxisAlignedBoundingBox Sphere::getAABBLocal() const
{
    AxisAlignedBoundingBox aabb;
    aabb.setExtents(2 * radius * Eigen::Vector3f::Ones());
    return aabb;
}

AxisAlignedBoundingBox Sphere::getAABB() const
{
    AxisAlignedBoundingBox aabb = getAABBLocal();
    aabb.setCenter(aabb.center() + position);
    return aabb;
}

float Sphere::getBoundingSphereRadius() const
{
    return radius;
}

std::string Sphere::name() const
{
    return "Sphere";
}

std::string Sphere::str() const
{
    std::stringstream ss;
    ss << "Sphere [ "
       << "pos: " << toString(position)
       << ", radius: " << radius
       << ", " << Shape::str()
       << "]";
    return ss.str();
}

std::shared_ptr<btCollisionShape> Sphere::getBulletCollisionShape(float margin) const
{
    return std::make_shared<btSphereShape>(static_cast<btScalar>(radius + margin));
}

void Sphere::addMargin(float margin)
{
    radius += margin;
}

}
