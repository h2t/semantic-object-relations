#pragma once

#include "Shapes/Shape.h"
#include "Shapes/Box.h"
#include "Shapes/Cylinder.h"
#include "Shapes/MeshShape.h"
#include "Shapes/Sphere.h"
#include "Shapes/shape_containers.h"
