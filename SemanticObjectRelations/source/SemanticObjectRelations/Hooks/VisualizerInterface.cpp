#include "VisualizerInterface.h"


using namespace semrel;


DrawColor::DrawColor() : DrawColor(0, 0, 0, 1)
{
}

DrawColor::DrawColor(float r, float g, float b, float a) : r(r), g(g), b(b), a(a)
{
}

VisuMetaInfo::VisuMetaInfo() : VisuMetaInfo("", "")
{
}

VisuMetaInfo::VisuMetaInfo(const std::string layer, const std::string& id) : 
    layer(layer), name(id)
{
}


std::shared_ptr<VisualizerInterface> VisualizerInterface::implementation = 
        std::make_shared<VisualizerInterface>();

VisuLevel VisualizerInterface::minimumLevel = VisuLevel::RESULT;


VisualizerInterface& VisualizerInterface::get()
{
    return *implementation;
}

void VisualizerInterface::setImplementation(std::shared_ptr<VisualizerInterface> implementation)
{
    VisualizerInterface::implementation = implementation;
}

VisuLevel VisualizerInterface::getMinimumVisuLevel()
{
    return minimumLevel;
}

void VisualizerInterface::setMinimumVisuLevel(VisuLevel level)
{
    VisualizerInterface::minimumLevel = level;
}

VisualizerInterface::VisualizerInterface() {}
VisualizerInterface::~VisualizerInterface() {}

void VisualizerInterface::clearAll() {}
void VisualizerInterface::clearLayer(const std::string& layer)
{ 
    (void) layer; 
}

void VisualizerInterface::drawLine(VisuMetaInfo id, const Eigen::Vector3f& start, const Eigen::Vector3f& end, float lineWidth, DrawColor color)
{
    (void) id, (void) start, (void) end, (void) lineWidth, (void) color; // unused
}

void VisualizerInterface::drawArrow(VisuMetaInfo id, const Eigen::Vector3f& origin, const Eigen::Vector3f& direction, float length, float width, DrawColor color)
{
    (void) id, (void) origin, (void) direction, (void) length, (void) width, (void) color; // unused
}

void VisualizerInterface::drawBox(VisuMetaInfo id, const Box& box, DrawColor color)
{
    (void) id, (void) box, (void) color; // unused
}

void VisualizerInterface::drawCylinder(VisuMetaInfo id, const Cylinder& cylinder, DrawColor color)
{
    (void) id, (void) cylinder, (void) color;
}

void VisualizerInterface::drawSphere(VisuMetaInfo id, const Sphere& sphere, DrawColor color)
{
    (void) id, (void) sphere, (void) color;
}

void VisualizerInterface::drawTriMesh(VisuMetaInfo id, const TriMesh& mesh, DrawColor color)
{
    (void) id, (void) mesh, (void) color;
}

void VisualizerInterface::drawPolygon(VisuMetaInfo id, const std::vector<Eigen::Vector3f>& polygonPoints, float lineWidth, DrawColor colorInner, DrawColor colorBorder)
{
    (void) id, (void) polygonPoints, (void) lineWidth, (void) colorInner, (void) colorBorder;
}

void VisualizerInterface::drawText(VisuMetaInfo id, const std::string& text, const Eigen::Vector3f& position, float size, DrawColor color)
{
    (void) id, (void) text, (void) position, (void) size, (void) color;
}

void VisualizerInterface::drawPointCloud(VisuMetaInfo id, const std::vector<Eigen::Vector3f>& cloud, float pointSize, DrawColor color)
{
    (void) id, (void) cloud, (void) pointSize, (void) color;
}


