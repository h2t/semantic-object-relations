#include "SupportAnalysisParameters.h"

namespace semrel
{

SupportAnalysisParameters::SupportAnalysisParameters() = default;

SupportAnalysisParameters::SupportAnalysisParameters(const SupportAnalysis& supportAnalysis)
{
    setFrom(supportAnalysis);
}

void SupportAnalysisParameters::writeTo(SupportAnalysis& supportAnalysis) const
{
    supportAnalysis.setContactMargin(contactMargin);

    supportAnalysis.setVertSepPlaneAngleMax(vertSepPlaneAngleMax);
    supportAnalysis.setVertSepPlaneAssumeSupport(vertSepPlaneAssumeSupport);

    supportAnalysis.setUncertaintyDetectionEnabled(uncertaintyDetectionEnabled);
    supportAnalysis.setSupportAreaRatioMin(supportAreaRatioMin);
}

void SupportAnalysisParameters::setFrom(const SupportAnalysis& supportAnalysis)
{
    contactMargin = supportAnalysis.getContactMargin();

    vertSepPlaneAngleMax = supportAnalysis.getVertSepPlaneAngleMax();
    vertSepPlaneAssumeSupport = supportAnalysis.getVertSepPlaneAssumeSupport();

    uncertaintyDetectionEnabled = supportAnalysis.getUncertaintyDetectionEnabled();
    supportAreaRatioMin = supportAnalysis.getSupportAreaRatioMin();
}



}


