#include "json.h"

#include <SemanticObjectRelations/RelationGraph/json/attributes.h>


void semrel::to_json(nlohmann::json& j, const SupportVertex& vertex)
{
    json::to_json_base(j, vertex);

    const auto& ud = vertex.ud;
    j["ud"] =
    {
        { "enabled", ud.enabled },
        { "safe", ud.safe },
        { "supportAreaRatio", ud.supportAreaRatio },
    };

    // The object data will be filled externally, see JsonShapeSerializer
}


void semrel::from_json(const nlohmann::json& j, SupportVertex& vertex)
{
    json::from_json_base(j, vertex);

    const nlohmann::json ud = j.at("ud");
    vertex.ud.enabled = ud.at("enabled").get<bool>();
    vertex.ud.safe = ud.at("safe").get<bool>();
    vertex.ud.supportAreaRatio = ud.at("supportAreaRatio").get<float>();
}

void semrel::to_json(nlohmann::json& j, const SupportEdge& edge)
{
    j =
    {
        { "verticalSeparatingPlane", edge.verticalSeparatingPlane },
        { "fromUncertaintyDetection", edge.fromUncertaintyDetection },
    };
}

void semrel::from_json(const nlohmann::json& j, SupportEdge& edge)
{
    edge.verticalSeparatingPlane = j.at("verticalSeparatingPlane");
    edge.fromUncertaintyDetection = j.at("fromUncertaintyDetection");
}
