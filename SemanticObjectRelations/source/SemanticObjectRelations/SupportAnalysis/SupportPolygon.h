#pragma once


#include <boost/geometry.hpp>
#include <boost/geometry/multi/geometries/multi_point.hpp>
#include <boost/geometry/multi/geometries/multi_polygon.hpp>
#include <boost/geometry/geometries/polygon.hpp>

#include <SemanticObjectRelations/Shapes/shape_containers.h>

#include "SupportGraph.h"


namespace semrel
{

using Point2D = boost::geometry::model::point<float, 2, boost::geometry::cs::cartesian>;
using Polygon2D = boost::geometry::model::polygon<Point2D>;
using MultiPoint2D = boost::geometry::model::multi_point<Point2D>;
using MultiPolygon2D = boost::geometry::model::multi_polygon<Polygon2D>;


/**
 * @brief Represents the support polygon of a supported objects with
 * respect to its supporting objects.
 *
 * The support polygon of a supported object A is the intersection of its
 * projection onto the ground plane with the projections of its supporting
 * objects.
 *
 * The 2D polygons reside in a 2D coordinate system in the ground plane.
 * This ground plane is spanned by two 3D vectors. These two vectors
 * represent the basis vectors of the 2D coordinate system in 3D.
 * Use SupportPolygon::to2D() and SupportPolygon::to3DGround() to
 * transform vertices between the 2D and 3D spaces.
 */
class SupportPolygon
{
public:

    /// Constructor.
    SupportPolygon(const Eigen::Vector3f& gravity);


    /// Initializes projection plane and coordinate system (called on construction).
    void initGeometry(const Eigen::Vector3f& gravity);
    /// Clears input and output values (no initGeometry() is necessary).
    void clear();


    // INPUT

    /// Sets the supported object.
    void setSupportedObject(const Shape& supported);
    /// Adds a supporting object.
    void addSupportingObject(const Shape& supporting);

    /// Sets supported and supporting object from the given object
    /// collection and support graph. (Calls clear()).
    void setObjectsFromSupportGraph(SupportGraph::Vertex supportedVertex, const ShapeMap& objects);


    // COMPUTE

    /**
     * @brief Computes the output projections and polygons.
     *
     * If no supported object was set, the supported object projection,
     * the supporting polygons and the support polygons are left empty.
     */
    void compute();


    // RESULTS

    // Projections
    /// Returns supported object's projection.
    Polygon2D getSupportedObjectProjection() const;
    /// Returns the supporting objects' projections.
    MultiPolygon2D getSupportingObjectsProjections() const;

    // Support polygons
    /**
     * @brief Returns the single supporting polygons.
     *
     * These are the polygons resulting from intersecting the supporting
     * object's projections and the supported object projections.
     */
    MultiPolygon2D getSupportingPolygons() const;
    /**
     * @brief Returns the support polygon of the supported object.
     *
     * This is the the convex hull of the single supporting polygons as
     * returned by getSupportingPolygons().
     */
    Polygon2D getSupportPolygon() const;


    // PROJECTION

    /// Returns the plane to which objects are projected.
    Hyperplane3f getProjectionPlane();
    /// Returns the two basis vectors of the polygon coordinate system.
    std::pair<Eigen::Vector3f, Eigen::Vector3f> getBasis() const;

    /// Projects a 3D point to the ground plane.
    Point2D to2D(const Eigen::Vector3f& point3D) const;
    /// Returns the location of a 2D point (in the polygon coordinate
    /// system) on the 3D ground plane.
    Eigen::Vector3f to3DGround(const Point2D& point2D) const;


    // HELPER

    /**
     * @brief Returns the 3D object vertex that produced the given projection point.
     * This computes the projection of all key points of the given object and
     * returns the key point that produces the least distance to the given projection point.
     * This method is made for native object projections, not support polygons.
     *
     * @param objectProjectionPoint the 2D point of the object's projection polygon
     * @param object the respective object
     * @return the 3D key point that most likely produced the 2D projection point
     */
    Eigen::Vector3f findObject3DVertex(const Point2D& objectProjectionPoint,
                                       const Shape& object);

private:

    /**
     * @brief Computes the projection polygon of object in the 2D polygon
     * coordinate system.
     *
     * Gets the triangle mesh of object and projects all vertices onto
     * onto the ground plane, i.e. it brings them into the 2D polygon
     * coordinate system. The result is the convex hull of the 2D
     * projection points.
     */
    Polygon2D projectionPolygon(const Shape& object) const;


    // INPUT
    /// The supported object.
    const Shape* supportedObject;
    /// The supporting objects.
    std::vector<const Shape*> supportingObjects;


    // OUTPUT

    /// Projection of supportedObject.
    Polygon2D supportedProjection;
    /// Projections of supportingObjects.
    MultiPolygon2D supportingProjections;

    /// Intersections of supportedProjection with supportingProjections.
    MultiPolygon2D supportingPolygons;
    /// Convex hull of supportingPolygons.
    Polygon2D supportPolygon;


    // PROJECTION

    /// Plane through origin with normal = gravity.
    Hyperplane3f projectionPlane;
    /// First vector of orthonormal basis spanning the projection plane.
    /// It holds <basisA, gravity> = <basisA, basisB> = 0, and |basisA| = 1.
    Eigen::Vector3f basisA;
    /// Second vector of orthonormal basis spanning the projection plane.
    Eigen::Vector3f basisB;

};

}
