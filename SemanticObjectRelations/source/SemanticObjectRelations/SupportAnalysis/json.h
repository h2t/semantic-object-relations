#pragma once

#include <SemanticObjectRelations/Serialization/json.h>

#include "SupportGraph.h"


namespace semrel
{

    void to_json(nlohmann::json& j, const SupportVertex& vertex);
    void from_json(const nlohmann::json& j, SupportVertex& vertex);

    void to_json(nlohmann::json& j, const SupportEdge& edge);
    void from_json(const nlohmann::json& j, SupportEdge& edge);

}
