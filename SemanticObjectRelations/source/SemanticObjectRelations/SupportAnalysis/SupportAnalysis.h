#pragma once

#include "GeometricReasoning.h"
#include "SupportGraph.h"
#include "UncertaintyDetection.h"


namespace semrel
{
/**
 * @brief Support analysis determines the support relation between a
 * number of objects in a scene.
 *
 *
 * ## Parameters:
 * - Enable Uncertainty Detection: (default: true)
 *   Whether or not to run uncertainty detection on support graph.
 *
 * ### Other parameters:
 * @see ActReasoning
 * @see ContactDetection
 * @see GeometricReasoning
 * @see UncertaintyDetection
 */
class SupportAnalysis
{

public:

    /// Constructor.
    SupportAnalysis(bool enableUncertaintyDetection = true);


    /**
     * @brief Extract the support relation of the given shapes and return it
     * as support graph.
     * @param objects the objects
     * @param safeObjectIDs Specified objects will always be labeled safe by
     *  Uncertainty Detection (if enabled). If empty, all objects who are
     *  unsupported in the intermediate support graph (as determined by the
     *  act relation) will be used instead.
     * @return the support graph
     */
    SupportGraph performSupportAnalysis(const ShapeList& objects,
                                        const std::set<ShapeID>& safeObjectIDs = {},
                                        const ContactGraph* contactGraph = nullptr) const;
    SupportGraph performSupportAnalysis(const ShapeMap& objects,
                                        const std::set<ShapeID>& safeObjectIDs = {},
                                        const ContactGraph* contactGraph = nullptr) const;


    // PARAMETERS


    /// Indicate whether uncertainty detection is enabled.
    bool getUncertaintyDetectionEnabled() const;
    /// Enable or disable uncertainty detection.
    void setUncertaintyDetectionEnabled(bool enabled);

    /// @see GeometricReasoning, @see UncertaintyDetection
    void setGravityVector(const Eigen::Vector3f& gravity);

    /// @see ContactDetection
    float getContactMargin() const;
    void setContactMargin(float margin);

    /// @see ActReasoning
    float getVertSepPlaneAngleMax() const;
    void setVertSepPlaneAngleMax(float angleMax);
    /// @see GeometricReasoning
    bool getVertSepPlaneAssumeSupport() const;
    void setVertSepPlaneAssumeSupport(bool assumeSupport);

    /// @see UncertaintyDetection
    float getSupportAreaRatioMin() const;
    void setSupportAreaRatioMin(float supportAreaRatioMin);


private:

    /// Returns the IDs of all vertices which have no in-edges.
    static void insertUnsupportedObjects(const SupportGraph& supportGraph, std::set<ShapeID>& ids);

    /// Build the support graph from act graph.
    SupportGraph buildSupportGraph(const SupportGraph& act) const;

    /// The geometric reasoning.
    GeometricReasoning geometricReasoning;
    /// The uncertainty detection.
    UncertaintyDetection uncertaintyDetection;


    // PARAMETERS

    /// If true, run uncertainty detection on support graph.
    bool uncertaintyDetectionEnabled;

};

}
