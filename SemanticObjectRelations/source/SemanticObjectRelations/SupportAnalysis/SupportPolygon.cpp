#include "SupportPolygon.h"

#include <algorithm>

#include <boost/geometry/multi/geometries/multi_point.hpp>

#include <SemanticObjectRelations/Hooks/Log.h>


namespace semrel
{

namespace bg = boost::geometry;

SupportPolygon::SupportPolygon(const Eigen::Vector3f& gravity)
{
    initGeometry(gravity);
}

void SupportPolygon::initGeometry(const Eigen::Vector3f& gravity)
{
    projectionPlane = Hyperplane3f(gravity, 0);

    // get a point != 0 on plane (take [1, 0, 0] or [0, 1, 0] as seed)
    Eigen::Vector3f seed = !gravity.isApprox(Eigen::Vector3f(1, 0, 0))
            ? Eigen::Vector3f(1, 0, 0)
            : Eigen::Vector3f(0, 1, 0);

    // normalize => first Basis vector
    basisA = projectionPlane.projection(seed).normalized();

    // get another vector orthotognal to gravity and basisA
    basisB = basisA.cross(gravity);
}

void SupportPolygon::clear()
{
    // input
    supportedObject = nullptr;
    supportingObjects.clear();

    // output
    supportedProjection.clear();
    supportingProjections.clear();
    supportingPolygons.clear();
    supportPolygon.clear();
}

void SupportPolygon::setSupportedObject(const Shape& supported)
{
    supportedObject = &supported;
}

void SupportPolygon::addSupportingObject(const Shape& supporting)
{
    supportingObjects.push_back(&supporting);
}

void SupportPolygon::setObjectsFromSupportGraph(
        SupportGraph::Vertex supportedVertex, const ShapeMap& objects)
{
    clear();

    setSupportedObject(*objects.at(supportedVertex.objectID()));

    for (SupportGraph::Edge edge : supportedVertex.inEdges())
    {
        // e = (other, object)
        addSupportingObject(*objects.at(edge.source().objectID()));
    }
}

void SupportPolygon::compute()
{
    // compute projections of supporting objects
    supportingProjections.clear();
    supportingProjections.reserve(supportingObjects.size());

    for (const Shape* supportingObj : supportingObjects)
    {
        Polygon2D supportingProj = projectionPolygon(*supportingObj);
        supportingProjections.push_back(supportingProj);
    }

    if (supportedObject == nullptr)
    {
        // no supported object set => remainder is left empty
        return;
    }

    // projection of supported object
    supportedProjection = projectionPolygon(*supportedObject);

    // compute and accumulate intersections of supported with supporting projections
    MultiPoint2D supportPolygonPoints; // all points inside the final support polygon
    supportingPolygons.clear();

    for (Polygon2D supportingProjection : supportingProjections)
    {
        // compute intersection
        MultiPolygon2D intersections;
        bg::intersection(supportedProjection, supportingProjection, intersections);

        for (Polygon2D& intersection : intersections)
        {
            // append intersection to supporting polygons
            supportingPolygons.push_back(intersection);

            // add intersection to support polygon points
            for (Point2D& ip : intersection.outer())
            {
                supportPolygonPoints.push_back(ip);
            }
        }
    }

    // compute support polygon
    bg::convex_hull(supportPolygonPoints, supportPolygon);
}

Hyperplane3f SupportPolygon::getProjectionPlane()
{
    return projectionPlane;
}

std::pair<Eigen::Vector3f, Eigen::Vector3f> SupportPolygon::getBasis() const
{
    return std::make_pair(basisA, basisB);
}

Point2D SupportPolygon::to2D(const Eigen::Vector3f& p) const
{
    // get 2D coordinates inside plane => (p*a, p*b)
    return Point2D(p.dot(basisA), p.dot(basisB));
}

Eigen::Vector3f SupportPolygon::to3DGround(const Point2D& point2D) const
{
    return point2D.get<0>() * basisA + point2D.get<1>() * basisB;
}

Eigen::Vector3f SupportPolygon::findObject3DVertex(const Point2D& objectProjectionPoint, const Shape& object)
{
    std::vector<Eigen::Vector3f> keys3D = object.getTriMesh().getVertices();
    std::vector<float> distances;

    // Compute distances of projections.
    auto dist = [&](Eigen::Vector3f & v)
    {
        return bg::distance(to2D(v), objectProjectionPoint);
    };
    std::transform(keys3D.begin(), keys3D.end(), std::back_inserter(distances), dist);

    // Find minimal distance.
    long minIndex = std::min_element(distances.begin(), distances.end()) - distances.begin();
    return keys3D.at(static_cast<std::size_t>(minIndex));
}

Polygon2D SupportPolygon::projectionPolygon(const Shape& object) const
{
    std::vector<Eigen::Vector3f> keys3D = object.getTriMesh().getVertices();

    bg::model::multi_point<Point2D> keys2D;

    for (Eigen::Vector3f& point3D : keys3D)
    {
        bg::append(keys2D, to2D(point3D));
    };

    Polygon2D polygon;
    bg::convex_hull(keys2D, polygon);

    return polygon;
}

Polygon2D SupportPolygon::getSupportPolygon() const
{
    return supportPolygon;
}

MultiPolygon2D SupportPolygon::getSupportingPolygons() const
{
    return supportingPolygons;
}

MultiPolygon2D SupportPolygon::getSupportingObjectsProjections() const
{
    return supportingProjections;
}

Polygon2D SupportPolygon::getSupportedObjectProjection() const
{
    return supportedProjection;
}

}
