#include "SupportAnalysis.h"

#include <SemanticObjectRelations/Hooks/Log.h>
#include <SemanticObjectRelations/Visualization/SupportAnalysisVisualizer.h>

#include <boost/graph/transpose_graph.hpp>


namespace semrel
{


std::ostream& operator <<(std::ostream& os, const std::set<ShapeID>& idSet)
{
    os << "{ ";
    for (ShapeID id : idSet)
    {
        os << id << " ";
    }
    os << "}";
    return os;
}


SupportAnalysis::SupportAnalysis(bool enableUncertaintyDetection) :
    uncertaintyDetectionEnabled(enableUncertaintyDetection)
{
}


static SupportAnalysisVisualizer& visu = SupportAnalysisVisualizer::get();

SupportGraph SupportAnalysis::performSupportAnalysis(const ShapeMap& objects,
                                                     const std::set<ShapeID>& _safeObjectIDs,
                                                     const ContactGraph* contactGraph) const
{
    SR_LOG_INFO << "=== STARTING SUPPORT ANALYSIS ===";

    SR_LOG_INFO << "-- Determining act relation --";
    SupportGraph actGraph = geometricReasoning.performGeometricReasoning(objects, contactGraph);

    SR_LOG_INFO << "-- Building support graph from act graph --";
    SupportGraph supportGraph = buildSupportGraph(actGraph);

    if (uncertaintyDetectionEnabled)
    {
        SR_LOG_INFO << "-- Uncertainty Detection --";

        std::set<ShapeID> safeObjectIDs = _safeObjectIDs;
        if (safeObjectIDs.empty())
        {
            insertUnsupportedObjects(supportGraph, safeObjectIDs);
        }
        SR_LOG_INFO << "-- (Safe object IDs: " << safeObjectIDs << ")";

        supportGraph = uncertaintyDetection.performUncertaintyDetection(
                    supportGraph, objects, safeObjectIDs);
    }

    visu.drawSupportGraph(supportGraph, objects);

    SR_LOG_INFO << "=== SUPPORT ANALYSIS FINISHED ===";
    return supportGraph;
}


SupportGraph SupportAnalysis::performSupportAnalysis(const ShapeList& objects,
                                                     const std::set<ShapeID>& safeObjectIDs,
                                                     const ContactGraph* contactGraph) const
{
    return performSupportAnalysis(toShapeMap(objects), safeObjectIDs, contactGraph);
}



SupportGraph SupportAnalysis::buildSupportGraph(const SupportGraph& act) const
{
    SupportGraph::Boost suppBoost;
    boost::transpose_graph(act.asBoost(), suppBoost);
    SupportGraph supp(suppBoost);
    return supp;
}

bool SupportAnalysis::getUncertaintyDetectionEnabled() const
{
    return uncertaintyDetectionEnabled;
}
void SupportAnalysis::setUncertaintyDetectionEnabled(bool enabled)
{
    uncertaintyDetectionEnabled = enabled;
}

void SupportAnalysis::setGravityVector(const Eigen::Vector3f& gravity)
{
    geometricReasoning.setGravityVector(gravity);
    uncertaintyDetection.setGravityVector(gravity);
}

float SupportAnalysis::getContactMargin() const
{
    return geometricReasoning.getContactMargin();
}

void SupportAnalysis::setContactMargin(float margin)
{
    geometricReasoning.setContactMargin(margin);
}

float SupportAnalysis::getVertSepPlaneAngleMax() const
{
    return geometricReasoning.getVertSepPlaneAngleMax();
}

void SupportAnalysis::setVertSepPlaneAngleMax(float angleMax)
{
    geometricReasoning.setVertSepPlaneAngleMax(angleMax);
}

bool SupportAnalysis::getVertSepPlaneAssumeSupport() const
{
    return geometricReasoning.getVertSepPlaneAssumeSupport();
}

void SupportAnalysis::setVertSepPlaneAssumeSupport(bool assumeSupport)
{
    geometricReasoning.setVertSepPlaneAssumeSupport(assumeSupport);
}

float SupportAnalysis::getSupportAreaRatioMin() const
{
    return uncertaintyDetection.getSupportAreaRatioMin();
}

void SupportAnalysis::setSupportAreaRatioMin(float supportAreaRatioMin)
{
    uncertaintyDetection.setSupportAreaRatioMin(supportAreaRatioMin);
}

void SupportAnalysis::insertUnsupportedObjects(
        const SupportGraph& supportGraph, std::set<ShapeID>& ids)
{
    for (SupportGraph::ConstVertex v : supportGraph.vertices())
    {
        if (v.inDegree() == 0)
        {
            ids.insert(v.objectID());
        }
    }
}


}
