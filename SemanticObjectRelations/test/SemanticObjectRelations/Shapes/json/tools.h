#pragma once

#include <SemanticObjectRelations/Shapes/json.h>

#include "../../ShapeListFixture.h"


namespace std
{
    template <class ValueT>
    std::ostream& operator<<(std::ostream& os, const std::vector<ValueT>& rhs)
    {
        os << "Vector (" << rhs.size() << "):";
        for (const auto& i : rhs)
        {
            os << "\n- " << i;
        }
        return os;
    }
}

namespace semrel::test
{

    template <class ShapeT>
    void test_to_json_from_json()
    {
        const ShapeT shapeIn = makeShape<ShapeT>(42);

        BOOST_TEST_CONTEXT("Registered types: " << json::ShapeSerializers::getRegisteredTypes())
        {
            const nlohmann::json j = static_cast<const Shape&>(shapeIn);

            BOOST_TEST_CONTEXT(j.dump(2))
            {
                BOOST_CHECK(j.is_object());
                const ShapeT shapeOut = j.get<ShapeT>();
                test::check_equal(shapeOut, shapeIn);
            }
        }
    }


    template <class ShapeT>
    void test_to_json_from_json_pointer()
    {
        const ShapePtr shape = test::makeShapePtr<ShapeT>(42);

        BOOST_TEST_MESSAGE("Serializing.");
        const nlohmann::json j = shape;

        BOOST_TEST_CONTEXT(j.dump(2))
        {
            BOOST_TEST_MESSAGE("Deserializing.");
            const ShapePtr shapeOut = j.get<ShapePtr>();
            test::check_equal<ShapeT>(shapeOut, shape);
        }
    }

}

