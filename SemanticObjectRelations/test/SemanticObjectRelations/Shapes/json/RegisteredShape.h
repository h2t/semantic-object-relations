#pragma once

#include <SemanticObjectRelations/Shapes.h>
#include <SemanticObjectRelations/Shapes/json.h>

#include "tools.h"


namespace semrel::test
{

    template <int Overload>
    class CustomShape : public Shape
    {
    public:

        using Shape::Shape;

        CustomShape(ShapeID id = ShapeID(66)) :
            CustomShape(Eigen::Vector3f(5, 2, -1),
                       Eigen::Quaternionf(Eigen::AngleAxisf(0.5, Eigen::Vector3f::UnitY())),
                       42, ShapeID(id))
        {}

        CustomShape(const Eigen::Vector3f& position, const Eigen::Quaternionf& orientation,
                    int customData, ShapeID id) :
            Shape(id), position(position), orientation(orientation), customData(customData)
        {}


        // Shape interface
        Eigen::Vector3f getPosition() const override { return position; }
        void setPosition(const Eigen::Vector3f& position) override { this->position = position; }

        Eigen::Quaternionf getOrientation() const override { return orientation; }
        void setOrientation(const Eigen::Quaternionf& orientation) override { this->orientation = orientation; }

        TriMesh getTriMeshLocal() const override { return {}; }
        float getBoundingSphereRadius() const override { return 1; }
        std::shared_ptr<btCollisionShape> getBulletCollisionShape(float margin) const override { (void) margin; return nullptr; }
        void addMargin(float margin) override { (void) margin; }


        Eigen::Vector3f position;
        Eigen::Quaternionf orientation;

        int customData;

    private:

        static const int _JSON_REGISTRATION;

    };

    template <int Ov>
    const int CustomShape<Ov>::_JSON_REGISTRATION = 0; // No registration.


    // Unregistered.

    using UnregisteredShape = CustomShape<0>;

    void to_json(nlohmann::json& j, const UnregisteredShape& rhs);
    void from_json(const nlohmann::json& j, UnregisteredShape& rhs);

    void check_equal_derived(const UnregisteredShape& lhs, const UnregisteredShape& rhs);


    // Registered through static member.

    using MemberRegisteredShape = CustomShape<1>;
    void to_json(nlohmann::json& j, const MemberRegisteredShape& rhs);
    void from_json(const nlohmann::json& j, MemberRegisteredShape& rhs);
    template <> const int MemberRegisteredShape::_JSON_REGISTRATION;

    void check_equal_derived(const MemberRegisteredShape& lhs, const MemberRegisteredShape& rhs);


    // Registered through free variable.

    using FreeRegisteredShape = CustomShape<2>;

    void to_json(nlohmann::json& j, const FreeRegisteredShape& rhs);
    void from_json(const nlohmann::json& j, FreeRegisteredShape& rhs);
    extern const int _FREE_REGISTERED_SHAPE_REGISTRATION;

    void check_equal_derived(const FreeRegisteredShape& lhs, const FreeRegisteredShape& rhs);

}

