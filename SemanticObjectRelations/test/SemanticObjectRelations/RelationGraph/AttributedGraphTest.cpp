#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include <SemanticObjectRelations/RelationGraph/json/AttributedGraph.h>
#include <SemanticObjectRelations/Shapes.h>
#include <SemanticObjectRelations/Shapes/json.h>

#include "../ShapeListFixture.h"


using namespace semrel;


struct VA : public ShapeVertex
{
    int vertexData;
};
struct EA
{
    int edgeData;
};
struct GA
{
    int graphData;
};


void to_json(nlohmann::json& j, const VA& a)
{
    j["vertexData"] = a.vertexData;
}
void from_json(const nlohmann::json& j, VA& a)
{
    a.vertexData = j.at("vertexData").get<int>();
}

void to_json(nlohmann::json& j, const EA& a)
{
    j["edgeData"] = a.edgeData;
}
void from_json(const nlohmann::json& j, EA& a)
{
    a.edgeData = j.at("edgeData").get<int>();
}

void to_json(nlohmann::json& j, const GA& a)
{
    j["graphData"] = a.graphData;
}
void from_json(const nlohmann::json& j, GA& a)
{
    a.graphData = j.at("graphData").get<int>();
}


namespace
{
struct GraphFixture
{
    using Graph = RelationGraph<VA, EA, GA>;

    ShapeList objects;
    Graph graph;

    GraphFixture()
    {
        objects.emplace_back(test::makeShapePtr<Box>(43));
        objects.emplace_back(test::makeShapePtr<Cylinder>(44));
        objects.emplace_back(test::makeShapePtr<Sphere>(45));
        objects.emplace_back(test::makeShapePtr<Box>(46));

        graph = Graph(objects);

        graph.addEdge(ShapeID(43), ShapeID(44));
        graph.addEdge(ShapeID(43), ShapeID(45));
        graph.addEdge(ShapeID(44), ShapeID(45));
    }

};
}


void check_equal(const AttributedGraph& attributed, const GraphFixture::Graph& graph)
{
    BOOST_CHECK_EQUAL(attributed.numVertices(), graph.numVertices());
    BOOST_CHECK_EQUAL(attributed.numEdges(), graph.numEdges());

    BOOST_CHECK_EQUAL(attributed.attrib().json.at("graphData"),
                      graph.attrib().graphData);

    auto gvi = graph.vertices().begin();
    auto avi = attributed.vertices().begin();
    for (; gvi != graph.vertices().end(); ++gvi, ++avi)
    {
        BOOST_CHECK_EQUAL(avi->objectID(), gvi->objectID());
        BOOST_CHECK_EQUAL(avi->attrib().json.at("vertexData").get<int>(),
                          gvi->attrib().vertexData);
    }


    auto gei = graph.edges().begin();
    auto aei = attributed.edges().begin();
    for (; gei != graph.edges().end(); ++gei, ++aei)
    {
        BOOST_CHECK_EQUAL(aei->sourceObjectID(), gei->sourceObjectID());
        BOOST_CHECK_EQUAL(aei->targetObjectID(), gei->targetObjectID());
        BOOST_CHECK_EQUAL(aei->attrib().json.at("edgeData").get<int>(),
                          gei->attrib().edgeData);
    }
}

void check_equal(const GraphFixture::Graph& lhs, const GraphFixture::Graph& rhs)
{
    BOOST_CHECK_EQUAL(lhs.numVertices(), rhs.numVertices());
    BOOST_CHECK_EQUAL(lhs.numEdges(), rhs.numEdges());

    BOOST_CHECK_EQUAL(lhs.attrib().graphData, rhs.attrib().graphData);

    auto gvi = rhs.vertices().begin();
    auto avi = lhs.vertices().begin();
    for (; gvi != rhs.vertices().end(); ++gvi, ++avi)
    {
        BOOST_CHECK_EQUAL(avi->objectID(), gvi->objectID());
        BOOST_CHECK_EQUAL(avi->attrib().vertexData, gvi->attrib().vertexData);
    }


    auto gei = rhs.edges().begin();
    auto aei = lhs.edges().begin();
    for (; gei != rhs.edges().end(); ++gei, ++aei)
    {
        BOOST_CHECK_EQUAL(aei->sourceObjectID(), gei->sourceObjectID());
        BOOST_CHECK_EQUAL(aei->targetObjectID(), gei->targetObjectID());
        BOOST_CHECK_EQUAL(aei->attrib().edgeData, gei->attrib().edgeData);
    }
}

void check_object_data(const AttributedGraph& attributed, const ShapeList& objects)
{
    for (auto v : attributed.vertices())
    {
        BOOST_CHECK_EQUAL(v.attrib().json.count("object"), 1);
        const ShapePtr shape = v.attrib().json.at("object").get<ShapePtr>();
        BOOST_CHECK(shape);
        BOOST_CHECK_EQUAL(shape->getID(), v.objectID());
    }

    const ShapeList objectsOut = attributed.deserializeObjects();
    BOOST_CHECK_EQUAL(objectsOut.size(), objects.size());
    for (const auto& obj : objectsOut)
    {
        // Just check whether the pointer is valid.
        BOOST_CHECK(obj);
    }
}


BOOST_FIXTURE_TEST_SUITE(AttributedGraphTest_Construction, GraphFixture)


BOOST_AUTO_TEST_CASE(test_toAttributedGraph_as_argument)
{
    AttributedGraph attributed;
    toAttributedGraph(attributed, graph);
    check_equal(attributed, graph);
}

BOOST_AUTO_TEST_CASE(test_toAttributedGraph_as_return)
{
    const AttributedGraph attributed = toAttributedGraph(graph);
    check_equal(attributed, graph);
}

BOOST_AUTO_TEST_CASE(test_AttributedGraph_fromGraph)
{
    AttributedGraph attributed;
    attributed.fromGraph(graph);
    check_equal(attributed, graph);
}

BOOST_AUTO_TEST_CASE(test_from_AttributedGraph)
{
    const AttributedGraph attributed = toAttributedGraph(graph);
    check_equal(attributed, graph);
}


BOOST_AUTO_TEST_CASE(test_AttributedGraph_serializeObjects)
{
    AttributedGraph attributed = toAttributedGraph(graph);
    check_equal(attributed, graph);
    attributed.serializeObjects(toShapeMap(objects));
    check_object_data(attributed, objects);
}

BOOST_AUTO_TEST_CASE(test_from_AttributedGraph_with_objects)
{
    const AttributedGraph attributed = toAttributedGraph(graph, toShapeMap(objects));
    check_equal(attributed, graph);
    check_object_data(attributed, objects);
}


BOOST_AUTO_TEST_SUITE_END()


namespace
{
struct AttributedGraphFixture : public GraphFixture
{
    AttributedGraph attributed;

    AttributedGraphFixture()
    {
        attributed.fromGraph(graph);
    }
};
}


BOOST_FIXTURE_TEST_SUITE(AttributedGraphTest, AttributedGraphFixture)


BOOST_AUTO_TEST_CASE(test_fromAttributedGraph_as_argument)
{
    Graph graphOut;
    fromAttributedGraph(attributed, graphOut);
    check_equal(graphOut, graph);
}

BOOST_AUTO_TEST_CASE(test_fromAttributedGraph_return)
{
    const Graph graphOut = fromAttributedGraph<Graph>(attributed);
    check_equal(graphOut, graph);
}

BOOST_AUTO_TEST_CASE(test_AttributedGraph_toGraph)
{
    const Graph graphOut = attributed.toGraph<Graph>();
    check_equal(graphOut, graph);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(AttributedGraph_JSON_serialization_Test, AttributedGraphFixture)


BOOST_AUTO_TEST_CASE(test_to_json)
{
    const nlohmann::json j = attributed;
    BOOST_CHECK(j.is_object());
    BOOST_CHECK_EQUAL(j.count("attrib"), 1);
    BOOST_CHECK_EQUAL(j.count("vertices"), 1);
    BOOST_CHECK_EQUAL(j.count("edges"), 1);
}


BOOST_AUTO_TEST_CASE(test_to_json_from_json)
{
    const nlohmann::json j = attributed;
    const semrel::AttributedGraph out = j.get<AttributedGraph>();

    BOOST_CHECK_EQUAL(out.attrib().json, attributed.attrib().json);

    BOOST_CHECK_EQUAL(out.numVertices(), attributed.numVertices());
    BOOST_CHECK_EQUAL(out.numEdges(), attributed.numEdges());

    {
        auto vi2 = attributed.vertices().begin();
        for (AttributedGraph::ConstVertex v1 : out.vertices())
        {
            const AttributedGraph::ConstVertex& v2 = *vi2;

            BOOST_CHECK_EQUAL(v1.objectID(), v2.objectID());
            BOOST_CHECK_EQUAL(v1.attrib().json, v2.attrib().json);

            ++vi2;
        }
    }

    auto ei2 = attributed.edges().begin();
    for (AttributedGraph::ConstEdge e1 : out.edges())
    {
        const AttributedGraph::ConstEdge& e2 = *ei2;

        BOOST_CHECK_EQUAL(e1.sourceObjectID(), e2.sourceObjectID());
        BOOST_CHECK_EQUAL(e1.targetObjectID(), e2.targetObjectID());
        BOOST_CHECK_EQUAL(e1.attrib().json, e2.attrib().json);

        ++ei2;
    }
}


BOOST_AUTO_TEST_SUITE_END()


