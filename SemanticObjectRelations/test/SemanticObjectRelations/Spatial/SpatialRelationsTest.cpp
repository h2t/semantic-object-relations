#include <SemanticObjectRelations/Spatial.h>
#include <SemanticObjectRelations/RelationGraph/graphviz.h>
#include <SemanticObjectRelations/RelationGraph/graphviz/WriterImpl.h>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "../ShapeListFixture.h"

using namespace semrel;


using test::ShapeListFixture;


BOOST_AUTO_TEST_SUITE(SpatialRelationsTest)


BOOST_AUTO_TEST_CASE(test_SpatialReltations_types)
{
    BOOST_CHECK_EQUAL(SpatialRelations::types.size(), static_cast<std::size_t>(SpatialRelations::Type::num_relations));
    for (std::size_t i = 0; i < SpatialRelations::types.size(); ++i)
    {
        BOOST_CHECK_EQUAL(SpatialRelations::types.at(i), static_cast<SpatialRelations::Type>(i));
    }
}

BOOST_AUTO_TEST_CASE(test_SpatialReltations_names)
{
    for (SpatialRelations::Type t : SpatialRelations::types)
    {
        BOOST_CHECK_NO_THROW(SpatialRelations::names.at(t));
        BOOST_CHECK(!SpatialRelations::names.at(t).empty());
    }
}


ShapeID makeID(size_t x, size_t y, size_t z)
{
    return ShapeID(static_cast<long>(x * 100 + y * 10 + z));
}

ShapeID makeID(const Eigen::Vector3i& idx)
{
    return ShapeID(static_cast<long>(idx.x() * 100 + idx.y() * 10 + idx.z()));
}


BOOST_AUTO_TEST_CASE(test_evaluate_relations_static)
{
    /* Construct the following scene (viewed from any side):
     * |
     * |  +-+--+   +----+   - low min
     * |  +-+--+   +----+   - low max = mid min  (contact)
     * |  | |  |   |    |   - origin
     * |  +-+--+   +----+   - mid max
     * |
     * |  +-+--+   +----+   - high min
     * |  | |  |   |    |
     * |  | |  |   |    |
     * |  +-+--+   +----+   - high max
     * v
     *
     * In addition, all of these 27 objects are contained in a large sphere.
     */

    ShapeList objects;

    const float lowMin = -2;
    const float lowMax = -1;

    const float midMin = lowMax;
    const float midMax = 1;

    const float highMin = 2;
    const float highMax = 4;

    std::vector<float> mins = { lowMin, midMin, highMin };
    std::vector<float> maxs = { lowMax, midMax, highMax };

    for (size_t x = 0; x < 3; ++x)
    {
        for (size_t y = 0; y < 3; ++y)
        {
            for (size_t z = 0; z < 3; ++z)
            {
                Eigen::Vector3f min(mins[x], mins[y], mins[z]);
                Eigen::Vector3f max(maxs[x], maxs[y], maxs[z]);
                objects.emplace_back(new Box(AxisAlignedBoundingBox(min, max), makeID(x, y, z)));
            }
        }
    }
    // Add an object containing all.
    ShapeID surroundID(999);
    objects.emplace_back(new Sphere({0, 0, 0}, 2 * (std::max(std::abs(lowMin), highMax) + 1), surroundID));


    spatial::Parameters params;
    params.distanceEqualityThreshold = 0.01f;

    const SpatialGraph graph = spatial::evaluateStaticRelations(toShapeMap(objects));

    auto writer = SpatialGraph::getGraphvizWriter();
    writer.write(graph, "spatial-graph.png");

    BOOST_CHECK_EQUAL(graph.numVertices(), objects.size());
    BOOST_CHECK_EQUAL(graph.numEdges(), objects.size() * (objects.size() - 1));

    using Type = SpatialRelations::Type;


    auto getRelation = [&graph](Type type, const Eigen::Vector3i& idxU, const Eigen::Vector3i& idxV)
    {
        return graph.edge(graph.vertex(makeID(idxU)), graph.vertex(makeID(idxV))).attrib().relations.get(type);
    };

    // Check Left/Right, Below/Above, Behind/Front
    for (int x = 0; x < 2; ++x)
    {
        for (int y = 0; y < 2; ++y)
        {
            for (int z = 0; z < 2; ++z)
            {
                BOOST_CHECK(getRelation(Type::static_left_of, { x, y, z }, { x+1, y, z}));
                BOOST_CHECK(getRelation(Type::static_right_of, { x+1, y, z }, { x, y, z}));

                BOOST_CHECK(getRelation(Type::static_behind, { x, y, z }, { x, y+1, z }));
                BOOST_CHECK(getRelation(Type::static_in_front_of, { x, y+1, z }, { x, y, z }));

                BOOST_CHECK(getRelation(Type::static_below, { x, y, z }, { x, y, z+1 }));
                BOOST_CHECK(getRelation(Type::static_above, { x, y, z+1 }, { x, y, z }));

                // Check contact.
                const Eigen::Vector3i idx(x, y, z);
                for (int i = 0; i < idx.size(); ++i)
                {
                    if (idx(i) == 0)
                    {
                        Eigen::Vector3i contact = idx;
                        contact(i) += 1;
                        BOOST_CHECK(getRelation(Type::contact, idx, contact));
                    }
                }
            }
        }
    }


    // Check consistency: around
    for (auto edge : graph.edges())
    {
        SpatialRelations filter({ Type::static_left_of, Type::static_right_of,
                                  Type::static_behind, Type::static_in_front_of });
        if (edge.attrib().relations.filter(filter).any())
        {
            BOOST_CHECK(edge.attrib().relations.get(Type::static_around));
        }
    }

    // Check consistency: contact
    for (auto edge : graph.edges())
    {
        const auto& rels = edge.attrib().relations;
        if (rels.get(Type::contact))
        {
            BOOST_CHECK_EQUAL(rels.get(Type::static_top), rels.get(Type::static_above));
            BOOST_CHECK_EQUAL(rels.get(Type::static_bottom), rels.get(Type::static_bottom));
            BOOST_CHECK_EQUAL(rels.get(Type::static_contact_around), rels.get(Type::static_around));
        }
    }

    // Check Inside/Surround.
    for (size_t x = 0; x < 3; ++x)
    {
        for (size_t y = 0; y < 3; ++y)
        {
            for (size_t z = 0; z < 3; ++z)
            {
                auto obj = graph.vertex(makeID(x, y, z));
                auto sur = graph.vertex(surroundID);
                auto obj2sur = graph.edge(obj, sur);
                auto sur2obj = graph.edge(sur, obj);
                BOOST_TEST_CONTEXT("Comparing object " << x << y << z << " to surrounding."
                                   << "\n- object AABB:   " << obj.attrib().currentAABB
                                   << "\n- surround AABB: " << sur.attrib().currentAABB
                                   << "\n- object -> surrounding: " << obj2sur.attrib().relations
                                   << "\n- surrounding -> object: " << sur2obj.attrib().relations)
                {
                    BOOST_CHECK(obj2sur.attrib().relations.get(Type::static_inside));
                    BOOST_CHECK(sur2obj.attrib().relations.get(Type::static_surrounding));
                }
            }
        }
    }

    // Check no dynamic relations were computed.
    for (auto edge : graph.edges())
    {
        BOOST_CHECK(!edge.attrib().relations.filter(SpatialRelations::dynamicTypes).any());
    }
}



BOOST_AUTO_TEST_CASE(test_evaluate_relations_dynamic)
{
    const Eigen::Vector3f extents = extents.Constant(2);
    const Eigen::Quaternionf ori = ori.Identity();

    ShapeList currentObjects;
    ShapeList pastObjects;

    float x = 0;
    long id = 1;

    auto addObject = [&](ShapeID id, Eigen::Vector3f pastPos, Eigen::Vector3f currentPos)
    {
        currentObjects.emplace_back(new Box(currentPos, ori, extents, id));
        pastObjects.emplace_back(new Box(pastPos, ori, extents, id));
    };

    // Stable: No change between frames
    ShapeID stableA(id++), stableB(id++);
    addObject(stableA, {x, 0, 0}, {x, 0, 0});
    addObject(stableB, {x, 0, 5}, {x, 0, 5});

    x += 10;

    // Moving together: Both touch and move the same way.
    ShapeID movingTogetherA(id++), movingTogetherB(id++);
    addObject(movingTogetherA, {x, 0, 0}, {x, 2, 0});
    addObject(movingTogetherB, {x, 0, 2}, {x, 2, 2});

    x += 10;

    // Halting together: Both touch and do not move.
    ShapeID haltingTogetherA(id++), haltingTogetherB(id++);
    addObject(haltingTogetherA, {x, 0, 0}, {x, 0, 0});
    addObject(haltingTogetherB, {x, 0, 2}, {x, 0, 2});

    x += 10;

    // Fixed moving together: Both touch, one does not move and the other does.
    ShapeID fixedMovingTogetherA(id++), fixedMovingTogetherB(id++);
    addObject(fixedMovingTogetherA, {x, 0, 0}, {x, 0, 0});
    addObject(fixedMovingTogetherB, {x, 0, 2}, {x, 0.5, 2});

    x += 10;

    // Getting close: Do not touch but get closer.
    ShapeID gettingCloseA(id++), gettingCloseB(id++);
    addObject(gettingCloseA, {x, 0, -4}, {x, 0, -2});
    addObject(gettingCloseB, {x, 0,  2}, {x, 0,  2});

    x += 10;

    // Moving apart: Do not touch but get closer.
    ShapeID movingApartA(id++), movingApartB(id++);
    addObject(movingApartA, {x, 0, -2}, {x, 0, -4});
    addObject(movingApartB, {x, 0,  2}, {x, 0,  4});


    spatial::Parameters params;
    params.distanceEqualityThreshold = 0.01f;

    const SpatialGraph graph =
            spatial::evaluateRelations(toShapeMap(currentObjects), toShapeMap(pastObjects), params);

    BOOST_CHECK_EQUAL(graph.numVertices(), currentObjects.size());
    BOOST_CHECK_EQUAL(graph.numEdges(), currentObjects.size() * (currentObjects.size() - 1));

    using Type = SpatialRelations::Type;


    auto getEdge = [&](ShapeID idA, ShapeID idB)
    {
        return graph.edge(graph.vertex(idA), graph.vertex(idB));
    };

    auto checkSymmetricRelation = [&](ShapeID idA, ShapeID idB, Type t)
    {
        const auto& a2b = getEdge(idA, idB);
        const auto& b2a = getEdge(idB, idA);

        const auto& a2bRels = a2b.attrib().relations;
        const auto& b2aRels = b2a.attrib().relations;

        BOOST_TEST_CONTEXT("Checking symmetric relation '" << SpatialRelations::names.at(t) << "' between " << idA << " and " << idB
                           << "\n- a -> b: " << a2bRels
                           << "\n- b -> a: " << b2aRels
                           << "\n- AABB of a: " << a2b.source().attrib().pastAABB
                           << " then " << a2b.source().attrib().currentAABB
                           << "\n- AABB of b: " << a2b.target().attrib().pastAABB
                           << " then " << a2b.target().attrib().currentAABB
                           )
        {
            BOOST_CHECK(a2bRels.get(t));
            BOOST_CHECK(b2aRels.get(t));
        }
    };

    checkSymmetricRelation(stableA, stableB, Type::dynamic_stable);
    checkSymmetricRelation(movingTogetherA, movingTogetherB, Type::dynamic_moving_together);
    checkSymmetricRelation(haltingTogetherA, haltingTogetherB, Type::dynamic_halting_together);
    checkSymmetricRelation(fixedMovingTogetherA, fixedMovingTogetherB, Type::dynamic_fixed_moving_together);
    checkSymmetricRelation(gettingCloseA, gettingCloseB, Type::dynamic_getting_close);
    checkSymmetricRelation(movingApartA, movingApartB, Type::dynamic_moving_apart);
}


BOOST_AUTO_TEST_CASE(test_SpatialGraph_filtered)
{
    // Left and mid touch, but mid and right don't.
    Box left(AxisAlignedBoundingBox({0, 0, 0}, {1, 1, 1}), ShapeID(4));
    Box mid(AxisAlignedBoundingBox({1, 0, 0}, {2, 1, 1}), ShapeID(6));
    Box right(AxisAlignedBoundingBox({3, 0, 0}, {4, 1, 1}), ShapeID(8));

    const ShapeMap objects = toShapeMap({&left, &mid, &right});
    const SpatialGraph graph = spatial::evaluateStaticRelations(objects);

    BOOST_CHECK_EQUAL(graph.numVertices(), objects.size());
    BOOST_CHECK_EQUAL(graph.numEdges(), objects.size() * (objects.size() - 1));

    // Keep only edges with relations contact or right.
    const SpatialRelations filter({ SpatialRelations::Type::contact,
                                    SpatialRelations::Type::static_right_of});

    const SpatialGraph filtered = graph.filtered(filter);

    BOOST_TEST_CONTEXT("Filtered graph: " << filtered)
    {
        /* Expected number of edges:
         * (2) left <-> mid (contact)
         * (1) left -> right (right of)
         * (1) mid -> right (right of)
         * ---
         *  4
         */
        BOOST_CHECK_EQUAL(filtered.numVertices(), objects.size());
        BOOST_CHECK_EQUAL(filtered.numEdges(), 4);

        for (auto edge : filtered.edges())
        {
            BOOST_CHECK(edge.attrib().relations.filter(filter).any());
        }
    }

}


BOOST_AUTO_TEST_SUITE_END()
